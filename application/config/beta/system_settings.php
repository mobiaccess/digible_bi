<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['protocol_send_files'] = 'ftp';

$config['upload']['max_size'] = 102400;
$config['upload']['allowed_types_image'] = 'jpg|jpeg|png';
$config['upload']['allowed_types_audio'] = 'wav|mp3';
$config['upload']['allowed_types_video'] = 'mov|avi|mp4|ogg';

$config['app_name'] = 'Zodiack';

$config['url-site'] = str_replace("localhost",$_SERVER['SERVER_ADDR'],base_url()); //'http://lp.njrxp.com.br/';

# Config others
$config['path-others'] = '/var/www/html/others/';
# config digible-bi
$config['path-digible-bi'] = $config['path-others'] . 'digible_bi/';

$config['settings'] = $config;