<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// $config['protocol_send_files'] = 'ftp';

// $config['server']['ip'] = 'ftp-cdn.acotelbrasil.com.br';
// $config['server']['user'] = 'acoconteudos';
// $config['server']['password'] = '@c0C0nT&uD@s';
// $config['server']['host'] = 'ftp-cdn.acotelbrasil.com.br';
// $config['server']['port'] = '21';
// $config['server']['path'] = '/Zodiack/';

// $config['url'] = 'http://static-cdn.acotelbrasil.com.br/Zodiack/';

// $config['local_path'] = '/var/www/html/others/zodiack_publisher/upload/';

$config['upload']['max_size'] = 102400;
$config['upload']['allowed_types_image'] = 'jpg|jpeg|png';
$config['upload']['allowed_types_audio'] = 'wav|mp3';
$config['upload']['allowed_types_video'] = 'mov|avi|mp4|ogg';

$config['upload']['max_width'] = 960;
$config['upload']['max_height'] = 540;

# Config others
$config['path-others'] = '/var/www/html/others/';
# config digible-bi
$config['path-digible-bi'] = $config['path-others'] . 'digible_bi/';

$config['settings'] = $config;