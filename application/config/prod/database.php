<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'njrxp';
$query_builder = TRUE;

/*
 *   Config DB caixapostal
 */
$db['njrxp']['hostname'] = 'dbmobiaccessdigital.cdfcolpfgd4s.us-east-1.rds.amazonaws.com';
$db['njrxp']['port']     = '5432';
$db['njrxp']['username'] = 'postgres';
$db['njrxp']['password'] = '#m0b1.4c3ss!';
$db['njrxp']['database'] = 'njrxp';
$db['njrxp']['dbdriver'] = 'postgre';
$db['njrxp']['dbprefix'] = '';
$db['njrxp']['pconnect'] = FALSE;
$db['njrxp']['db_debug'] = FALSE;
$db['njrxp']['cache_on'] = FALSE;
$db['njrxp']['cachedir'] = '';
$db['njrxp']['char_set'] = 'utf8';
$db['njrxp']['dbcollat'] = 'utf8_general_ci';
$db['njrxp']['swap_pre'] = '';
$db['njrxp']['autoinit'] = TRUE;
$db['njrxp']['stricton'] = FALSE;

##JOGOS DE SEMPRE
$db['jogos_de_sempre']['hostname'] = 'dbmobiaccessdigital.cdfcolpfgd4s.us-east-1.rds.amazonaws.com';
$db['jogos_de_sempre']['port']     = '5432';
$db['jogos_de_sempre']['password'] = '#m0b1.4c3ss!';
$db['jogos_de_sempre']['username'] = 'postgres';
$db['jogos_de_sempre']['database'] = 'jogos_de_sempre';
$db['jogos_de_sempre']['dbdriver'] = 'postgre';
$db['jogos_de_sempre']['dbprefix'] = '';
$db['jogos_de_sempre']['pconnect'] = FALSE;
$db['jogos_de_sempre']['db_debug'] = FALSE;
$db['jogos_de_sempre']['cache_on'] = FALSE;
$db['jogos_de_sempre']['cachedir'] = '';
$db['jogos_de_sempre']['char_set'] = 'utf8';
$db['jogos_de_sempre']['dbcollat'] = 'utf8_general_ci';
$db['jogos_de_sempre']['swap_pre'] = '';
$db['jogos_de_sempre']['autoinit'] = TRUE;
$db['jogos_de_sempre']['stricton'] = FALSE;

##BASE UNICA
$db['base_unica']['hostname'] = 'mobiaccess-iops.cftktw4x2hbw.us-east-2.rds.amazonaws.com';
$db['base_unica']['password'] = '#m0b1.4c3ss!';
$db['base_unica']['username'] = 'postgres';
$db['base_unica']['database'] = 'base_unica';
$db['base_unica']['dbdriver'] = 'postgre';
$db['base_unica']['dbprefix'] = '';
$db['base_unica']['pconnect'] = FALSE;
$db['base_unica']['db_debug'] = FALSE;
$db['base_unica']['cache_on'] = FALSE;
$db['base_unica']['cachedir'] = '';
$db['base_unica']['char_set'] = 'utf8';
$db['base_unica']['dbcollat'] = 'utf8_general_ci';
$db['base_unica']['swap_pre'] = '';
$db['base_unica']['autoinit'] = TRUE;
$db['base_unica']['stricton'] = FALSE;

##BASE UNICA CLARO
$db['base_unica_claro']['hostname'] = 'dbmobiacess.cftktw4x2hbw.us-east-2.rds.amazonaws.com';
$db['base_unica_claro']['password'] = '#m0b1.4c3ss!';
$db['base_unica_claro']['username'] = 'postgres';
$db['base_unica_claro']['database'] = 'base_unica_claro';
$db['base_unica_claro']['dbdriver'] = 'postgre';
$db['base_unica_claro']['dbprefix'] = '';
$db['base_unica_claro']['pconnect'] = FALSE;
$db['base_unica_claro']['db_debug'] = FALSE;
$db['base_unica_claro']['cache_on'] = FALSE;
$db['base_unica_claro']['cachedir'] = '';
$db['base_unica_claro']['char_set'] = 'utf8';
$db['base_unica_claro']['dbcollat'] = 'utf8_general_ci';
$db['base_unica_claro']['swap_pre'] = '';
$db['base_unica_claro']['autoinit'] = TRUE;
$db['base_unica_claro']['stricton'] = FALSE;

/*
 *   Config DB SMS_Navigation
 */

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = '';
$db['default']['password'] = '';
$db['default']['database'] = '';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */
