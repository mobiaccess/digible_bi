<?php

$form_type = array(
    array(
        'field' => 'type',
        'label' => 'Tipo',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'description',
        'label' => 'Descrição',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )
);

$form_category = array(
    array(
        'field' => 'category',
        'label' => 'Categoria',
        'rules' => 'trim|required'
     ),
    array(
        'field' => 'description',
        'label' => 'Descrição',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )
);

$form_partner = array(
    array(
        'field' => 'partner',
        'label' => 'Parceiro',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )
);

$form_item = array(
     array(
        'field' => 'item',
        'label' => 'Item',
        'rules' => 'trim|required'
         ),
    array(
      'field' => 'id_type',
      'label' => 'Tipo',
      'rules' => 'trim|required'
       ),
    array(
      'field' => 'id_status',
      'label' => 'Status',
      'rules' => 'trim|required'
       ),
);

$form_tag = array(
    array(
        'field' => 'tag',
        'label' => 'tag',
        'rules' => 'trim|required'
         )
);

$form_versicle = array(
    array(
        'field' => 'versiculo',
        'label' => 'Versículo',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    )
);

$form_devotional = array(
    array(
        'field' => 'mensagem',
        'label' => 'Devocional',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    )
);

$form_versicle_category = array(
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'code',
        'label' => 'Code',
        'rules' => 'trim'
    )
);

$form_news = array(
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'titulo',
        'label' => 'Título',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'news',
        'label' => 'Notícia',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'peso',
        'label' => 'Peso',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    ),
    array(
        'field' => 'data_publicacao',
        'label' => 'Data Publicação',
        'rules' => 'trim|required'
    ),
);

$form_news_category = array(
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'religions[]',
        'label' => 'Religiões',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'descricao',
        'label' => 'Descrição',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    ),
);

$form_religion = array(
    array(
        'field' => 'religion',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'description',
        'label' => 'Descrição',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )
);


$form_movie = array(
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'titulo',
        'label' => 'Título',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    ),
    array(
        'field' => 'data_publicacao',
        'label' => 'Data Publicação',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'item_image',
        'label' => 'Imagem',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'item_movie',
        'label' => 'Vídeo',
        'rules' => 'trim|required'
    ),
);

$form_movie_edit = array(
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'titulo',
        'label' => 'Título',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    ),
    array(
        'field' => 'data_publicacao',
        'label' => 'Data Publicação',
        'rules' => 'trim|required'
    ),
);

$form_movie_category = array(
    array(
        'field' => 'categoria',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'descricao',
        'label' => 'Descrição',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    ),
);

$form_quiz_level = array(
    array(
        'field' => 'level',
        'label' => 'Nível',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'code_level',
        'label' => 'Código',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'min_score',
        'label' => 'Pontuação mínima',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    ),
);

$form_quiz_question = array(
    /*array(
        'field' => 'category',
        'label' => 'Categoria',
        'rules' => 'trim|required'
    ),*/
    array(
        'field' => 'question',
        'label' => 'Pergunta',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'arrayAnswers[]',
        'label' => 'Resposta',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )

);
/*
for ( $i=1; $i<=4; $i++ ){

    $form_quiz_question[] = array(
        'field' => 'answer_'.$i,
        'label' => 'Resposta '.$i,
        'rules' => 'trim|required'
    );

    $form_quiz_question[] = array(
        'field' => 'answer_json_'.$i,
        'label' => 'JSON',
        'rules' => 'trim'
    );

}
*/

$form_achievement = array(
    
    array(
        'field' => 'title',
        'label' => 'Título',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'description',
        'label' => 'Descrição',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'image',
        'label' => 'Imagem',
        'rules' => 'trim'
    ),
    array(
        'field' => 'image_bw',
        'label' => 'Imagem P&B',
        'rules' => 'trim'
    ),
    array(
        'field' => 'code',
        'label' => 'Código',
        'rules' => 'trim|required'
    )

);

$form_config_advertise = array(
    
    array(
        'field' => 'id_so_type',
        'label' => 'Sistema',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'advertise',
        'label' => 'Anúncio',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'code',
        'label' => 'Código',
        'rules' => 'trim'
    ),
    array(
        'field' => 'module',
        'label' => 'Módulo',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )

);

$form_config_product = array(
    
    array(
        'field' => 'id_so_type',
        'label' => 'Sistema',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'product',
        'label' => 'Produto',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'code',
        'label' => 'Código',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'price',
        'label' => 'Preço',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'economic',
        'label' => 'Econômico',
        'rules' => 'trim'
    ),
    array(
        'field' => 'status',
        'label' => 'Status',
        'rules' => 'trim'
    )

);

$form_config_facebook = array(
    
    array(
        'field' => 'app_name',
        'label' => 'Nome da aplicação',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'app_id',
        'label' => 'ID da aplicação',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'secret',
        'label' => 'Secret',
        'rules' => 'trim|required'
    ),
    
);

$form_info = array(
    
    array(
        'field' => 'info',
        'label' => 'Info',
        'rules' => 'trim|required'
    ),
    
);

$form_notification = array(
    
    array(
        'field' => 'id_notification_type',
        'label' => 'Tipo',
        'rules' => 'trim|required'
    ),
    
    array(
        'field' => 'notification',
        'label' => 'Notificação',
        'rules' => 'trim|required|max_length[140]'
    ),
    
    array(
        'field' => 'date_published',
        'label' => 'Data de publicação',
        'rules' => 'trim|required'
    ),
    
);

$form_content_sms = array(
    
    array(
        'field' => 'id_channel',
        'label' => 'Canal',
        'rules' => 'trim|required'
    ),
    
    array(
        'field' => 'id_status',
        'label' => 'Status',
        'rules' => 'trim|required'
    ),
    
    array(
        'field' => 'date_published',
        'label' => 'Data de publicação',
        'rules' => 'trim|required'
    ),
    
    array(
        'field' => 'content',
        'label' => 'Mensagem',
        'rules' => 'trim|required'
    )
    
);

$form_faq = array(
    
    array(
        'field' => 'html',
        'label' => 'FAQ',
        'rules' => 'trim|required'
    ),
    
);

$form_terms = array(
    
    array(
        'field' => 'html',
        'label' => 'Termos',
        'rules' => 'trim|required'
    ),
    
);
$form_salmo = array(
    
    array(
        'field' => 'title',
        'label' => 'titulo',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'status',
        'label' => 'status',
        'rules' => 'trim|required'
    ),
    
);

$config = array(
    'type_validacao' => $form_type,
    'category_validacao' => $form_category,
    'partner_validacao' => $form_partner,
    'item_validacao' => $form_item,
    'tag_validacao' => $form_tag,
    'versicle_validacao' => $form_versicle,
    'versicle_category_validacao' => $form_versicle_category,
    'devotional_validacao' => $form_devotional,
    'news_validacao' => $form_news,
    'news_category_validacao' => $form_news_category,
    'religion_validacao' => $form_religion,
    'movie_category_validacao' => $form_movie_category,
    'movie_validacao' => $form_movie,
    'movie_validacao' => $form_movie,
    'movie_edit_validacao' => $form_movie_edit,
    'quiz_level_validacao' => $form_quiz_level,
    'quiz_validacao' => $form_quiz_question,
    'achievement_validacao' => $form_achievement,
    'config_advertise_validacao' => $form_config_advertise,
    'config_product_validacao' => $form_config_product,
    'config_facebook_validacao' => $form_config_facebook,
    'info_validacao' => $form_info,
    'notification_validacao' => $form_notification,
    'content_sms_validacao' => $form_content_sms,
    'faq_validacao' => $form_faq,
    'terms_validacao' => $form_terms,
    'salmos_validacao' => $form_salmo,
);
