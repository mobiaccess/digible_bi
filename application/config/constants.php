<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


// Dblinks parameters
define('ACOTELVAS_DEV', 	'host=10.2.102.150 dbname=acotel_brazil_vas');
define('ACOTELVAS_PROD', 	'host=10.2.102.80 dbname=acotel_brazil_vas');
define('BROADCAST_DEV', 	'host=10.2.240.11 port=5434 dbname=broadcast2');
define('BROADCAST_PROD', 	'host=10.2.102.157 dbname=broadcast2');
define('TIMBRAZIL', 		'host=10.2.102.3 dbname=timbrazil');
define('CROSS_SELLING', 	'host=10.2.102.29 dbname=cross_selling');
define('INFOTAINMENT_PROD', 'host=10.2.102.113 dbname=infotainment');


define('BROADCAST_SOURCE', "BC2");

define('ERROR_MSG', "Comando invalido, por favor tente novamente");
define('FAIL_MSG', "Nao conseguimos processar sua requisicao, favor tente novamente mais tarde");


/*
 * Email Types Subject Constants
 */

define("CREATED_BROADCAST_SUBJECT", "Broadcast 2.0 - Notificação de novo broadcast");
define("UPDATED_BROADCAST_SUBJECT", "Broadcast 2.0 - Notificação de alteração broadcast");
define("PENDING_APPROVAL_BROADCAST_SUBJECT", "Broadcast 2.0 - Notificação de broadcast com aprovação pendente");

/*
 * Files locate
 */
define('PATH_DEFAULT', '/var/www/html/others/zodiack');

/* End of file constants.php */
/* Location: ./application/config/constants.php */