<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['protocol_send_files'] = 'ftp';

$config['server']['ip'] = '192.168.222.15';
$config['server']['user'] = 'zodiack';
$config['server']['password'] = 'zodiack';
$config['server']['host'] = 'image-zodiack.acotelbrasil.com.br';
$config['server']['port'] = '21';
$config['server']['path'] = '/';

$config['url'] = 'http://image-zodiack.acotelbrasil.com.br/';

$config['local_path'] = '/var/www/html/others/zodiack_publisher/upload/';

$config['upload']['max_size'] = 102400;
$config['upload']['allowed_types_image'] = 'jpg|jpeg|png';
$config['upload']['allowed_types_audio'] = 'wav|mp3';
$config['upload']['allowed_types_video'] = 'mov|avi|mp4|ogg';

$config['upload']['max_width'] = 960;
$config['upload']['max_height'] = 540;

$config['app_name'] = 'Zodiack';

$config['url-core'] = 'http://beta-mobivideos.acotelbrasil.com.br:8080/webapp/';
$config['url-site'] = str_replace("localhost",$_SERVER['SERVER_ADDR'],base_url());

$config['bu-send-password'] = 'https://beta-hub.acotelbrasil.com.br/send/password?';

$config['apk-folder'] = '/var/www/html/others/jogos_de_sempre/';

$config['url-landing-page'] = 'https://beta-hub.acotelbrasil.com.br/wap/subscribe?TOKEN=19b15c4a203910dc1814067db9caf414&CODE='; //STJS0101

$config['url-cancel-subscriber'] = 'https://beta-hub.acotelbrasil.com.br/web/unsubscribe?MSISDN=%s&CODE=%s'; //STJS0101

$config['url-landing-page-result'] = str_replace("localhost",$_SERVER['SERVER_ADDR'],base_url()) . 'landingpage_result';

// BU-Claro
$config['bu-claro'] = "https://bu-claro.digible.com.br/";

$config['settings'] = $config;