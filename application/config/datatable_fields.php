<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$config['NEWS'] = array(
    'ID',
    'Título',
    'Cat',
    'Peso',
    'Status',
    'Publicação',
);
        
$config['MOVIE'] = array(
    'ID',
    'Título',
    'Cat',
    'Status',
    'Publicação',
);

$config['VERSICLE'] = array(
    'ID',
    'Versículo',
    'Categoria'
);

$config['DEVOTIONAL'] = array(
    'ID',
    'Mensagem',
    'Categoria'
);

$config['QUIZ'] = array(
    'ID',
    'Pergunta',
    'Nível',
    'Status'
);

$config['datatable_fields'] = $config;