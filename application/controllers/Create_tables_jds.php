<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-09-12
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Create_tables_jds extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function index() {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        // $this->Logger->info('comentário');
        $this->tracking->createTables();
        // $this->Logger->info('Done. Total: ' . sizeof($rows));

        $this->Logger->info('END TRANSACTION');

    }

}