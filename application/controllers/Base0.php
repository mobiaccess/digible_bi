<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-07-30
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base0 extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function makeBaseSubscriptionNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );


        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {
            // $this->Logger->info('comentário');
            $this->tracking->makeBaseSubscription($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseSubscriptionJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {
            // $this->Logger->info('comentário');
            $this->tracking->makeBaseSubscription($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseCanceledNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            // $this->Logger->info('comentário');
            $this->tracking->makeBaseCanceled($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseCanceledJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            // $this->Logger->info('comentário');
            $this->tracking->makeBaseCanceled($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseBillingNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {
            
            // $this->Logger->info('comentário');
            $this->tracking->makeBaseBilling($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseBillingJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {
            
            // $this->Logger->info('comentário');
            $this->tracking->makeBaseBilling($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseAccessNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            // $this->Logger->info('comentário');
            $this->tracking->makeBaseAccess($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseAccessJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            // $this->Logger->info('comentário');
            $this->tracking->makeBaseAccess($dt->format("Y-m-d"));
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

}