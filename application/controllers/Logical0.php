<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-07-30
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logical0 extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function createLogicalSubscriptionFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeSubscriptionLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalSubscriptionFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeSubscriptionLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalCanceledFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeCanceledLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalCanceledFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeCanceledLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalBillingFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeBillingLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalBillingFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeBillingLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalAccessFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeLandingLogLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createLogicalAccessFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $begin = new DateTime('2018-06-01');
        $end = new DateTime('2018-08-10');
        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            // $this->Logger->info('comentário');
            $this->tracking->makeLandingLogLogicalFile($date);
            // $this->Logger->info('Done. Total: ' . sizeof($rows));
        }

        $this->Logger->info('END TRANSACTION');

    }

    
}