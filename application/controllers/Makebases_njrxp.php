<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-07-30
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Makebases_njrxp extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function makeBaseSubscription($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        }

        $date = $this->input->get_post('date', true);

        if (!$date || $date == '') {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        // $this->Logger->info('comentário');
        $this->tracking->makeBaseSubscription($date);
        // $this->Logger->info('Done. Total: ' . sizeof($rows));

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseCanceled($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        // $this->Logger->info('comentário');
        $this->tracking->makeBaseCanceled($date);
        // $this->Logger->info('Done. Total: ' . sizeof($rows));

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseBilling($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        // $this->Logger->info('comentário');
        $this->tracking->makeBaseBilling($date);
        // $this->Logger->info('Done. Total: ' . sizeof($rows));

        $this->Logger->info('END TRANSACTION');

    }

    public function makeBaseAccess($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        // $this->Logger->info('comentário');
        $this->tracking->makeBaseAccess($date);
        // $this->Logger->info('Done. Total: ' . sizeof($rows));

        $this->Logger->info('END TRANSACTION');

    }
    
}
?>