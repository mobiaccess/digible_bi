<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-06-22
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sendfiles_akamai_jds extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function sendSubscriptionFile($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $files = $this->tracking->getSubscriptionFilesToSend($date);
        $this->Logger->info('Enviando arquivos');
        foreach ($files as $file) {
            if ($this->uploadFile($file['id_file'], $file['filename'])) {
                $this->Logger->info('File sent: ' . $file['filename']);
                $this->tracking->setSentSubscriptionFile($file['id_file']);
            }
        }
        $this->Logger->info('END TRANSACTION');
    }

    private function uploadFile($idFile, $origFileName) {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        set_time_limit(0);

        $blobName = $origFileName . ".csv"; //"arc_subs.csv";
        $filename = $this->system_settings['path-digible-bi'] . $blobName;
        $this->Logger->info('FileName: ' . $filename);

        $this->Logger->info('[' . __METHOD__ . '] End');

    }

