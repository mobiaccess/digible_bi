<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-06-22
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sendfiles_njrxp extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function sendSubscriptionFile($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $files = $this->tracking->getSubscriptionFilesToSend($date);
        $this->Logger->info('Enviando arquivos');
        foreach ($files as $file) {
            if ($this->uploadFile($file['id_file'], $file['filename'])) {
                $this->Logger->info('File sent: ' . $file['filename']);
                $this->tracking->setSentSubscriptionFile($file['id_file']);
            }
        }
        $this->Logger->info('END TRANSACTION');
    }

    public function sendCanceledFile($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Enviando arquivos');
        $files = $this->tracking->getCanceledFilesToSend($date);

        foreach ($files as $file) {
            if ($this->uploadFile($file['id_file'], $file['filename'])) {
                $this->Logger->info('File sent: ' . $file['filename']);
                $this->tracking->setSentCanceledFile($file['id_file']);
            }
        }
        $this->Logger->info('END TRANSACTION');
    }

    public function sendBillingFile($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Enviando arquivos');
        $files = $this->tracking->getBillingFilesToSend($date);

        foreach ($files as $file) {
            if ($this->uploadFile($file['id_file'], $file['filename'])) {
                $this->Logger->info('File sent: ' . $file['filename']);
                $this->tracking->setSentBillingFile($file['id_file']);
            }
        }
        $this->Logger->info('END TRANSACTION');
    }

    public function sendAccessFile($date=false) {
        
        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Enviando arquivos');
        $files = $this->tracking->getAccessFilesToSend($date);

        foreach ($files as $file) {
            if ($this->uploadFile($file['id_file'], $file['filename'])) {
                $this->Logger->info('File sent: ' . $file['filename']);
                $this->tracking->setSentAccessFile($file['id_file']);
            }
        }
        $this->Logger->info('END TRANSACTION');

    }

    private function uploadFile($idFile, $origFileName) {

        set_time_limit(0);

        $blobName = $origFileName . ".csv"; //"arc_subs.csv";
        $filename = $this->system_settings['path-digible-bi'] . $blobName;
        $this->Logger->info('FileName: ' . $filename);

        $accesskey = "KrtImIQrOFu0mBiIVgJqHfVRI/RuUQZ8YwDHv/B2K5MQDp4ejSudVJhtWnR/D2e9XHcIYulb4eQLt/aV5fHckw==";
        $storageAccount = 'bidigible';
        $filetoUpload = realpath($filename);
        $containerName = 'mobiaccess';
        //$blobName = 'image.jpg';
        $version = "2017-07-29";

        $destinationURL = "https://$storageAccount.blob.core.windows.net/$containerName/$blobName";


        $currentDate = gmdate("D, d M Y H:i:s T", time());
        $handle = fopen($filetoUpload, "r");
        $fileLen = filesize($filetoUpload);

        $headerResource = "x-ms-blob-cache-control:max-age=3600\nx-ms-blob-type:BlockBlob\nx-ms-date:$currentDate\nx-ms-version:$version";
        $urlResource = "/$storageAccount/$containerName/$blobName";

        $arraysign = array();
        $arraysign[] = 'PUT';               /*HTTP Verb*/  
        $arraysign[] = '';                  /*Content-Encoding*/  
        $arraysign[] = '';                  /*Content-Language*/  
        $arraysign[] = $fileLen;            /*Content-Length (include value when zero)*/  
        $arraysign[] = '';                  /*Content-MD5*/  
        $arraysign[] = 'text/csv';         /*Content-Type*/  
        $arraysign[] = '';                  /*Date*/  
        $arraysign[] = '';                  /*If-Modified-Since */  
        $arraysign[] = '';                  /*If-Match*/  
        $arraysign[] = '';                  /*If-None-Match*/  
        $arraysign[] = '';                  /*If-Unmodified-Since*/  
        $arraysign[] = '';                  /*Range*/  
        $arraysign[] = $headerResource;     /*CanonicalizedHeaders*/
        $arraysign[] = $urlResource;        /*CanonicalizedResource*/

        $str2sign = implode("\n", $arraysign);

        $sig = base64_encode(hash_hmac('sha256', urldecode(utf8_encode($str2sign)), base64_decode($accesskey), true));  
        $authHeader = "SharedKey $storageAccount:$sig";

        $headers = [
            'Authorization: ' . $authHeader,
            'x-ms-blob-cache-control: max-age=3600',
            'x-ms-blob-type: BlockBlob',
            'x-ms-date: ' . $currentDate,
            'x-ms-version: ' . $version,
            'Content-Type: text/csv',
            'Content-Length: ' . $fileLen
        ];

        $ch = curl_init($destinationURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 600); //timeout in seconds
        curl_setopt($ch, CURLOPT_INFILE, $handle); 
        curl_setopt($ch, CURLOPT_INFILESIZE, $fileLen);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 900); 
        curl_setopt($ch, CURLOPT_UPLOAD, true); 
        $result = curl_exec($ch);
        $this->Logger->info('result: ' . $result);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->Logger->info('httpcode: ' . $httpcode);

        // echo ('Result<br/>');
        // print_r($result);

        // echo ('Error<br/>');
        // print_r(curl_error($ch));

        
        if (curl_errno($ch)){
            curl_close($ch);
            $this->Logger->info('Falha ao enviar arquivo: ' . $filename);
            return false;
        }

        $this->Logger->info('Arquivo [' . $filename .'] enviado!');
        return true;

    }

}