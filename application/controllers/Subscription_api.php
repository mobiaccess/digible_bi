<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// trava a exibição de erros
error_reporting(0);
ini_set(“display_errors”, 0 );

/**
 * Controla Something
 * http://localhost/esporte_interativo_vas/index.php/subscription_api?MSISDN=552183059028&ID_SYNTAX=4&ACTION=SUBSCRIBE
 * @author something
 */

class Subscription_api extends MY_Controller {

    private $msisdn;
    private $action;
    private $ident;
    private $dateGoodTo;
    private $externalUid;
    private $subscriptionDate;
    private $begin_time_execution;
    private $out;
    private $actionSubscribe = "SUBSCRIBE";
    private $actionUnsubscribe = "UNSUBSCRIBE";
    private $actionUnsubscribeAll = "UNSUBSCRIBE_ALL";
    private $base;
    private $source;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $system_settings = $this->config->item('settings', 'system_settings');
        $this->base = $system_settings['bu-send-password'];   
    }
    
    /*
     * $this->action = subscribe ou unsubscribe
     * para mais opções adicione as rotas
     */
    public function index() {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Subscription_model', 'subscription');
        $this->subscription->setLogger($this->Logger);

        $this->Logger->info("\$_SERVER array: " . print_r($_SERVER, true));
        $this->Logger->info("\$_REQUEST array: " . print_r($_REQUEST, true));
        $this->Logger->info("\$_POST array: " . print_r($_POST, true));
        $this->Logger->info("\$_GET array: " . print_r($_GET, true));

        if(!$this->action)
            $this->action = strtoupper($this->uri->segment(2));

        $this->msisdn = $this->input->get_post('MSISDN', true);
        $this->ident = $this->input->get_post('IDENT', true);
        $this->subscriptionTypeCode = $this->input->get_post('CODE', true);
        $this->externalUid = strtoupper($this->input->get_post('UID', true));
        $this->dateGoodTo = strtoupper($this->input->get_post('GOOD_TO', true));
        $this->subscriptionDate = strtoupper($this->input->get_post('SUBS_DATE', true));
        $this->syntax = $this->input->get_post('SYNTAX', true);
        $this->source = $this->input->get_post('SOURCE', true);

        if (strlen($this->msisdn) < 13 AND !is_numeric($this->msisdn)) {
            
            $this->out = array('error' => "ERRO: Não possui número msisdn.", 'response' => false);
            $this->Logger->info("ERRO: Não possui número msisdn.");
            
        } elseif (strlen($this->action) == 0 || $this->action === false ) {
            
            $this->out = array('error' => "ERRO: Não possui action de comando.", 'response' => false);
            $this->Logger->info("ERRO: Não possui action de comando.");
            
        }  elseif (strlen($this->ident) == 0 || $this->ident === false ) {
            
            $this->out = array('error' => "ERRO: Não possui ident.", 'response' => false);
            $this->Logger->info("ERRO: Não possui ident.");
            
        } elseif (strlen($this->externalUid) == 0 || $this->externalUid === false ) {
            
            $this->out = array('error' => "ERRO: Não possui external UID.", 'response' => false);
            $this->Logger->info("ERRO: Não possui external uid.");
            
        } elseif ( (strlen($this->dateGoodTo) == 0 || $this->dateGoodTo === false) && in_array($this->action, array($this->actionSubscribe)) ) {
            
            $this->out = array('error' => "ERRO: Não possui date_good_to.", 'response' => false);
            $this->Logger->info("ERRO: Não possui date_good_to.");
            
        } elseif ( (strlen($this->subscriptionDate) == 0 || $this->subscriptionDate === false) && in_array($this->action, array($this->actionSubscribe)) ) {
            
            $this->out = array('error' => "ERRO: Não possui date_inserted.", 'response' => false);
            $this->Logger->info("ERRO: Não possui date_inserted.");
            
        } elseif ( (strlen($this->subscriptionTypeCode) == 0 || $this->subscriptionTypeCode === false) && in_array($this->action, array($this->actionSubscribe, $this->actionUnsubscribe)) ) {
            
            $this->out = array('error' => "ERRO: Não possui subscription_type_code.", 'response' => false);
            $this->Logger->info("ERRO: Não possui subscription_type_code.");
            
        } else {

            $this->Logger->info("Parametros recebidos com sucesso.");
            $this->Logger->info("msisdn: [{$this->msisdn}]");
            $this->Logger->info("action: [{$this->action}]");
            $this->Logger->info("ident: [{$this->ident}]");
            $this->Logger->info("subscription_type_code: [{$this->subscriptionTypeCode}]");
            $this->Logger->info("external_uid: [{$this->externalUid}]");
            $this->Logger->info("date_good_to: [{$this->dateGoodTo}]");
            $this->Logger->info("date_inserted: [{$this->subscriptionDate}]");
            $this->Logger->info("syntax: [{$this->syntax}]");
            $this->Logger->info("source: [{$this->source}]");

            switch ($this->action) {
                
                case $this->actionSubscribe:
                    
                    $this->Logger->info("ACTION -> SUBSCRIBE");    
                    
                    try {
                        
                        $subs = $this->subscription->subscribe($this->msisdn, $this->ident, $this->subscriptionTypeCode, $this->externalUid, $this->dateGoodTo, $this->subscriptionDate, $this->syntax, $this->source);
                        $this->Logger->info('$subs: ' . print_r($subs, true));
                        //$this->sendPassword($this->msisdn, $this->subscriptionTypeCode, $subs['password']);
                        // registrar assinatura para notifacação
                        $result_inst_notify = $this->subscription->insertSubsNotify($subs['id_subscription'], $this->msisdn, $this->ident, $this->subscriptionTypeCode, $this->externalUid, $this->dateGoodTo, $this->subscriptionDate, $this->syntax, $this->source);

                        $this->out['response'] = true;
                        $this->out['error'] = "none";
                        
                    } catch (SubscriptionException $e) {
                        $this->Logger->info(" Subcription exception message: " . $e->getMessage());
                        $this->Logger->info(" Subcription exception code: " . $e->getCode());
                        switch ($e->getCode()) {
                            case -32:
                                $this->out['response'] = true;
                                $this->out['error'] = "none";
                                break;
                            default:
                                # Ocorreu um erro inesperado no sistema;
                                $this->out['response'] = false;
                                $this->out['error'] = "ERRO INESPERADO";
                                $this->Logger->info("ERRO INESPERADO");
                                break;
                        }
                    }
                    break;
                case $this->actionUnsubscribe:
                                     
                    $this->Logger->info("ACTION -> UNSUBSCRIBE");

                    try {
                        
                        if ($this->subscription->unsubscribe($this->msisdn, $this->ident, $this->subscriptionTypeCode, $this->externalUid, $this->source) ) {
                            
                            $this->Logger->info('ASSINATURA CANCELADA COM SUCESSO');
                            // registrar cancelamento para notificar parceiro!
                            $result_inst_notify = $this->subscription->insertCancNotify($this->msisdn, $this->ident, $this->subscriptionTypeCode, $this->externalUid, $this->source);

                            $this->out['response'] = true;
                            $this->out['error'] = "none";
                            
                        }
                        
                    } catch (SubscriptionException $e) {
                        $this->Logger->info(" Subcription exception message: " . $e->getMessage());
                        $this->Logger->info(" Subcription exception code: " . $e->getCode());
                        switch ($e->getCode()) {
                            default:
                                # Ocorreu um erro inesperado no sistema;
                                $this->out['response'] = false;
                                $this->out['error'] = "ERRO INESPERADO";
                                $this->Logger->info("ERRO INESPERADO");
                                break;
                        }
                    }
                    break;
            }
        }
        
        $this->load->view('view_json', array('out' => array("result" => $this->out)));
        $this->Logger->info("\$this->out: [" . print_r($this->out,true) . "]");
        $this->Logger->info("return_json: [" . json_encode($this->out) . "]");
        $this->Logger->info("Tempo total de executação da index: " . (microtime(true) - $this->begin_time_execution));        
        $this->Logger->info('[' . __METHOD__ . ']End');
        
    }

    private function sendPassword($msisdn, $code, $password) {
        $this->Logger->info("[START][".__METHOD__."]");

        //fluxo de vinculação de msisdn
        $url = $this->base . 'MSISDN=' . $msisdn . '&CODE=' . $code . '&PASSWORD=' . $password; // 5521981527320 STJS0101 1234
        $this->Logger->info('url: ' . $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('token: 634896ba3647c125d9a533047a20ab3b'));
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 'msisdn=' . $msisdn);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $response = curl_exec($ch);
        $this->Logger->info('return: ' . $response);
        // Closing
        curl_close($ch);
        $this->Logger->info("[END][".__METHOD__."]");
    }

    public function sendUnsubscribe() {
        $this->Logger->info("[START][".__METHOD__."]");

        if (!isset($this->session->user)) {
            return;
        }

        $msisdn = $this->session->user['msisdn'];
        $this->load->model('home_model');
        $this->home_model->setLogger($this->Logger);
        $code = $this->home_model->getCode();

        //fluxo de vinculação de msisdn
        $url = sprintf($this->base, $msisdn, $code);
        $this->Logger->info('url: ' . $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('token: 634896ba3647c125d9a533047a20ab3b'));
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 'msisdn=' . $msisdn);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $response = json_decode(curl_exec($ch));
        $this->Logger->info('return: ' . $response);
        // Closing
        curl_close($ch);

        $this->Logger->info(print_r($response, true));

        if ($response->result->response = true) {
            $this->load->library('session');
            $this->session->unset_userdata('user');
            $data = array(
                'success' => true,
                'msg' => $response->result->description,
            );
        } else {
            $data = array(
                'success' => false,
                'msg' => $response->result->error,
            );
        }
        

        $this->Logger->info("[END][".__METHOD__."]");
        exit(json_encode($data));
    }

}
?>