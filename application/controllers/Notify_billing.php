<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notify_billing extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $system_settings = $this->config->item('settings', 'system_settings');
    }

    public function index() {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        if(!$this->input->is_cli_request()) {
            echo "Script can only be accessed from the command line";
            $this->Logger->info("Script can only be accessed from the command line");
            return;
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Subscription_model', 'subscription');
        $this->subscription->setLogger($this->Logger);

        $this->Logger->info('Start notify billing ...');
        // get rows to notify
        $rows = $this->subscription->getBillToNotify();
        $this->Logger->info('Done. Total: ' . sizeof($rows));

        if(sizeof($rows) > 0) {
            $this->Logger->info('Start sending billing to partner ...');

            foreach($rows as $row) {
                $this->sendNotify($row['msisdn'], $row['ident'], $row['code'], $row['good_to'], $row['last_billing_success'], $row['price'], $row['uid'], $row['id_billing_success']);
            }

            $this->Logger->info('Done.');
        }

        $this->Logger->info('END TRANSACTION');
    }

    private function sendNotify($msisdn, $ident, $code, $good_to, $last_billing_success, $price, $uid, $id_billing_success) {
        $this->Logger;
        $url = 'https://api.hiedu.co/v1/mobiaccess/notifications/billing-success/?MSISDN=' . $msisdn . '&IDENT=' . $ident . '&CODE=' . $code . '&GOOD_TO=' . $good_to . '&LAST_BILLING_SUCCESS=' . $last_billing_success . '&PRICE=' . $price . '&UID=' . $uid;
        // $url = 'https://api.hiedu.co/v1/mobiaccess/notifications/billing-success/?MSISDN=' . $msisdn . '&IDENT=' . $ident . '&CODE=' . $code . '&UID=' . $uid . '&SOURCE=' . $source;        

        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_NOBODY => false,
            CURLOPT_USERAGENT => 'MobiAccess Sample cURL Request',
        );
        
        $curl = curl_init($url);
        curl_setopt_array($curl, $options);

        $content = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $this->Logger->info('RESPONSE: ' . $content);
        $this->Logger->info('CODE: ' . $httpcode);

        $this->Logger->info($url . ' (Sent!)');
        
        if(curl_error($curl) || $httpcode != 200){
            $this->Logger->info('ERRO NA REQUISIÇÃO');
            $this->load->model('Subscription_model', 'subscription');
            $this->subscription->setLogger($this->Logger);
            $this->subscription->resetBillNotify($id_billing_success);
            curl_close($curl);
            return;
        }

        curl_close($curl);

        $this->Logger->info('SUCESSO NA REQUISIÇÃO');
        $this->load->model('Subscription_model', 'subscription');
        $this->subscription->setLogger($this->Logger);
        $this->subscription->confirmBillNotify($id_billing_success);
    }

}

?>
