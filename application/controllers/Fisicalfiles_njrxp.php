<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-06-22
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fisicalfiles_njrxp extends MY_Controller {

    private $system_settings;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
    }

    public function createSubscriptionFile($date=false) {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Start get data to create subscription file');
        $logicalFiles = $this->tracking->getSubscriptionLogicalFiles($date);

        foreach($logicalFiles as $file) {

            $rows = $this->tracking->getSubscriptionContentFile($file['id_file'], $date, $file['carrier_code']);
            $this->Logger->info('Done. Total: ' . sizeof($rows));

            if(sizeof($rows) > 0) {

                $this->Logger->info('Dados encontrado, escrevendo arquivo');
                $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                //$this->load->library('filecdr');
                
                if (!$this->writeFile($filename, $rows, $this->Logger)) {
                    $this->Logger->info("Error: ao criar arquivo ($filename)");
                } else {
                    $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                    // set file is created
                    $this->tracking->setCreateSubscriptionFile($file['id_file']);
                }

            }

        }

        $this->Logger->info('END TRANSACTION');
    }
    
    public function createCanceledFile($date=false) {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Start get data to create canceled file');
        $logicalFiles = $this->tracking->getCanceledLogicalFiles($date);

        foreach($logicalFiles as $file) {

            $rows = $this->tracking->getCanceledCountentFile($file['id_file'], $date, $file['carrier_code']);
            $this->Logger->info('Done. Total: ' . sizeof($rows));

            if(sizeof($rows) > 0) {

                $this->Logger->info('Dados encontrado, escrevendo arquivo');
                $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                //$this->load->library('filecdr');
                
                if (!$this->writeFile($filename, $rows, $this->Logger)) {
                    $this->Logger->info("Error: ao criar arquivo ($filename)");
                } else {
                    $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                    // set file is created
                    $this->tracking->setCreateCanceledFile($file['id_file']);
                }

            }
        }

        $this->Logger->info('END TRANSACTION');
    }

    public function createBillingFile($date=false) {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Start get data to create subscription file');
        $logicalFiles = $this->tracking->getBillingLogicalFiles($date);

        foreach($logicalFiles as $file) {

            $rows = $this->tracking->getBillingContentFile($file['id_file'], $date, $file['carrier_code']);
            $this->Logger->info('Done. Total: ' . sizeof($rows));

            if(sizeof($rows) > 0) {

                $this->Logger->info('Dados encontrado, escrevendo arquivo');
                $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                //$this->load->library('filecdr');
                
                if (!$this->writeFile($filename, $rows, $this->Logger)) {
                    $this->Logger->info("Error: ao criar arquivo ($filename)");
                } else {
                    $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                    // set file is created
                    $this->tracking->setCreateBillingFile($file['id_file']);
                }

            }

        }

        $this->Logger->info('END TRANSACTION');
    }

    public function createAccessFile($date=false) {

        $this->Logger->info('[' . __METHOD__ . '] Start');

        // if(!$this->input->is_cli_request()) {
        //     echo "Script can only be accessed from the command line";
        //     $this->Logger->info("Script can only be accessed from the command line");
        //     return;
        // }

        if (!$date) {
            $date = $today = date("Y-m-d", strtotime("-1 day"));
        }

        $this->begin_time_execution = microtime(true);
        
        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        $this->Logger->info('Start get data to create subscription file');
        $logicalFiles = $this->tracking->getAccessLogicalFiles($date);

        foreach($logicalFiles as $file) {

            $rows = $this->tracking->getAccessCountentFile($file['id_file'], $date);
            $this->Logger->info('Done. Total: ' . sizeof($rows));

            if(sizeof($rows) > 0) {

                $this->Logger->info('Dados encontrado, escrevendo arquivo');
                $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                //$this->load->library('filecdr');
                
                if (!$this->writeFile($filename, $rows, $this->Logger)) {
                    $this->Logger->info("Error: ao criar arquivo ($filename)");
                } else {
                    $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                    // set file is created
                    $this->tracking->setCreateAccessFile($file['id_file']);
                }

            }

        }

        $this->Logger->info('END TRANSACTION');
    }

    /**
     * $filename is a full path
     * $arrContentFile is rows to CDR
     */
    private function writeFile($filename, $arrContentFile, $Logger) {

        // Check if file exists
        if (file_exists($filename)) {
            $Logger->info("Arquivo ($filename) já criado!");
            return true;
        }

        if (!$handle = fopen($filename, 'a')) {
            $Logger->error("Não foi possível abrir o arquivo ($filename)");
            return false;
        }

        foreach($arrContentFile as $row) {
            if (isset($row['syntax'])) {
                $row['syntax'] = str_replace("\n", "", $row['syntax']);
            }
            $conteudo = implode(";", $row);
            $conteudo = $conteudo . "\n";
            //print_r($conteudo);
            if (fwrite($handle, $conteudo) === FALSE) {
                $Logger->error("Não foi possível escrever no arquivo ($filename)");
                return false;
            }
        }

        fclose($handle);
        $Logger->info('Done.');
        return true;
    }

}

?>
