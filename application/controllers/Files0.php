<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-07-30
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Files0 extends MY_Controller {

    private $system_settings;
    // 2019-11-13 até 2020-04-27
    private $begin;
    private $end;

    public function __construct() {
        parent::__construct();
        $this->config->load('system_settings', TRUE);
        $this->system_settings = $this->config->item('settings', 'system_settings');
        $this->begin = new DateTime('2018-06-01');
        $this->end = new DateTime('2018-08-10');
    }

    public function createSubscriptionFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_njrxp', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking->getSubscriptionLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking->getSubscriptionContentFile($file['id_file'], $date, $file['carrier_code']);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking->setCreateSubscriptionFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createSubscriptionFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_jds', 'tracking');
        $this->tracking->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking->getSubscriptionLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking->getSubscriptionContentFile($file['id_file'], $date, $file['carrier_code']);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking->setCreateSubscriptionFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createCanceledFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_njrxp', 'tracking_njrxp');
        $this->tracking_njrxp->setLogger($this->Logger);
            
        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking_njrxp->getCanceledLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking_njrxp->getCanceledCountentFile($file['id_file'], $date, $file['carrier_code']);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking_njrxp->setCreateCanceledFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createCanceledFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_jds', 'tracking_jds');
        $this->tracking_jds->setLogger($this->Logger);
    
        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking_jds->getCanceledLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking_jds->getCanceledCountentFile($file['id_file'], $date, $file['carrier_code']);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking_jds->setCreateCanceledFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createBillingFileNJRXP() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_njrxp', 'tracking_njrxp');
        $this->tracking_njrxp->setLogger($this->Logger);

        foreach ($period as $dt) {
            
            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking_njrxp->getBillingLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking_njrxp->getBillingContentFile($file['id_file'], $date, $file['carrier_code']);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking_njrxp->setCreateBillingFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createBillingFileJDS() {

        ini_set('max_execution_time', 0);

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_jds', 'tracking_jds');
        $this->tracking_jds->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking_jds->getBillingLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking_jds->getBillingContentFile($file['id_file'], $date, $file['carrier_code']);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking_jds->setCreateBillingFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createAccessFileNJRXP() {

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '1024M');

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_njrxp', 'tracking_njrxp');
        $this->tracking_njrxp->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");

            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking_njrxp->getAccessLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking_njrxp->getAccessCountentFile($file['id_file'], $date);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking_njrxp->setCreateAccessFile($file['id_file']);
                    }

                }

            }
            
        }

        $this->Logger->info('END TRANSACTION');

    }

    public function createAccessFileJDS() {

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '1024M');

        $this->Logger->info('[' . __METHOD__ . '] Start');

        #$end = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($this->begin, $interval, $this->end);

        $this->load->model('Tracking_model_jds', 'tracking_jds');
        $this->tracking_jds->setLogger($this->Logger);

        foreach ($period as $dt) {

            $date = $dt->format("Y-m-d");
            
            $this->Logger->info('Start get data to create subscription file');
            $logicalFiles = $this->tracking_jds->getAccessLogicalFiles($date);

            foreach($logicalFiles as $file) {

                $rows = $this->tracking_jds->getAccessCountentFile($file['id_file'], $date);
                $this->Logger->info('Done. Total: ' . sizeof($rows));

                if(sizeof($rows) > 0) {

                    $this->Logger->info('Dados encontrado, escrevendo arquivo');
                    $filename = $this->system_settings['path-digible-bi'] . $file['filename'] . '.csv';

                    //$this->load->library('filecdr');
                    
                    if (!$this->writeFile($filename, $rows, $this->Logger)) {
                        $this->Logger->info("Error: ao criar arquivo ($filename)");
                    } else {
                        $this->Logger->info("Sucesso: ao criar arquivo arquivo ($filename)");
                        // set file is created
                        $this->tracking_jds->setCreateAccessFile($file['id_file']);
                    }

                }

            }

        }

        $this->Logger->info('END TRANSACTION');

    }

    /**
     * $filename is a full path
     * $arrContentFile is rows to CDR
     */
    private function writeFile($filename, $arrContentFile, $Logger) {

        // Check if file exists
        if (file_exists($filename)) {
            $Logger->info("Arquivo ($filename) já criado!");
            return true;
        }

        if (!$handle = fopen($filename, 'a')) {
            $Logger->error("Não foi possível abrir o arquivo ($filename)");
            return false;
        }

        foreach($arrContentFile as $row) {
            $conteudo = implode(";", $row);
            $conteudo = $conteudo . "\n";
            //print_r($conteudo);
            if (fwrite($handle, $conteudo) === FALSE) {
                $Logger->error("Não foi possível escrever no arquivo ($filename)");
                return false;
            }
        }

        fclose($handle);
        $Logger->info('Done.');
        return true;
    }

}