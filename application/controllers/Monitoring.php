<?php
/**
 * Controller para fornecer estilo para Landing Page TIM
 * 
 * @author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 * @since 2019-01-14
 * 
 **/
class Monitoring extends MY_Controller {
        
    private $base;

    public function __construct() {
        parent::__construct();
    }

    public function check_status() {
        $this->Logger->info('[' . __METHOD__ . '] Start');
        $this->load->model('Home_model', 'home');
        $this->home->setLogger($this->Logger);
        echo $this->home->zabbixCheckStatus();
        $this->Logger->info('[' . __METHOD__ . '] End');
    }

}
?>