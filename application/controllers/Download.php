<?php
/**
 * Library para manter menu de categorias atualizado;
 * 
 * @author Fernando Siggelkow <fernando.siggelkow@acotel.com>
 * @since 2017-05-10
 * 
 **/
class Download extends MY_Controller {
        
    public function __construct() {

        parent::__construct();
        
    }

    public function index() {

        $this->Logger->info("Start - " . __METHOD__);
        $this->load->library('session');

        $this->load->model('home_model');
        $this->home_model->setLogger($this->Logger);

        $id_game = $this->input->get('id_game');

        // register download and get filename
        $filename = $this->home_model->download($this->session->user['id_subscription'], $id_game);

        $this->config->load('system_settings', TRUE);
        $system_settings = $this->config->item('settings', 'system_settings');
        $base_file = $system_settings['apk-folder'];
        $local_file = $base_file . $filename;
        
        // $local_file = $base_file . 'jogo_da_vida.apk';
        $this->Logger->info(__METHOD__ . ' absolute path: ' . $local_file);

        if (file_exists($local_file)) {

            $this->Logger->info(__METHOD__ . ' Preparando Header para download');
            header("Content-Disposition: attachment; filename=\"" . basename('/var/www/html/others/jogos_de_sempre/' . $filename) . "\"");
            header("Content-Type: application/force-download");
            header("Content-Length: " . filesize($local_file));
            header("Connection: close");
            readfile($local_file);

        }

        $this->Logger->info("End - " . __METHOD__);
    }

    public function checkDownload() {

        $this->Logger->info("Start - " . __METHOD__);
        $this->load->library('session');

        $id_game = $this->input->get('id_game');

        $this->load->model('home_model');
        $this->home_model->setLogger($this->Logger);

        $this->Logger->info(__METHOD__ . ' User: ' . print_r($this->session->user, true));

        $result = $this->home_model->checkDownload($this->session->user['id_subscription'], $id_game);
        if ($result == 1) {
            $data = array(
                'success' => true,
                'msg' => 'O download começará em breve.',
            );
        } elseif ($result == -5) { 
            $data = array(
                'success' => false,
                'msg' => 'Limite máximo de downloads do jogo já foi atingido.',
            );
        } elseif ($result == -1) {
            $data = array(
                'success' => false,
                'msg' => 'Insira créditos no seu celular para baixar seu jogo.',
            );
        } elseif ($result == -2) {
            $data = array(
                'success' => false,
                'subscribe' => true,
                'msg' => 'Você ainda não é assinante, para assinar clique em "Quero Assinar" e tenha acesso a todos os jogos.',
            );
            $this->load->library('session');
            $this->session->unset_userdata('user');
        } else {
            $data = array(
                'success' => false,
                'msg' => 'O limite de downloads da semana já foi atingido.',
            );
        }
        $this->Logger->info(__METHOD__ . ' $data: ' . print_r($data, true));
        $this->Logger->info("End - " . __METHOD__);
        exit(json_encode($data));
    }
    
}