jQuery(function($){
	$.timepicker.regional['pt-BR'] = {
            
            timeText: 'Horário',
            hourText: 'Horas',
            minuteText: 'Minutos',
            currentText: 'Agora',
            closeText: 'Fechar'
    };

	$.timepicker.setDefaults($.timepicker.regional['pt-BR']);
	
});

