$(function(){
    
    var url_site = $('.navbar-brand').attr('href');
    
    if ( $('#list').length ){
    
        var oTable = $('#list').dataTable({

            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": url_site+"index.php/item/data_table",
            "aaSorting": [[ 0, "desc" ]],
            "aoColumns": [
                {"sWidth":"8%"},
                {"sWidth":"8%"},
                {"sWidth":"8%"},
                {"sWidth":"18%"},
                {"sWidth":"12%"},
                {"sWidth":"10%"},
                {"sWidth":"12%"},
                { "bSortable": false },
            ],
           "oLanguage": {
                "sUrl": url_site+"application/themes/default/js/dataTables/dataTables.portuguese.txt"
           }

        });
        
    }

    if ( $('#upload-item').length ){

        $('#upload-item').uploadifive({
            'uploadScript'          : url_site+'index.php/upload/do_upload/',
            'auto'              : true,
            'multi'             : false,
            'formData'          : {},
            'buttonText'        : 'adicionar mídia',
            'removeCompleted'   : true,
            'onError'     : function(file, errorType) {
                alert('['+errorType+'] Ocorreu um erro com o arquivo ' + file.name + '. Verifique se você enviou o tipo correto de arquivo');
            },
            'onUploadComplete' : function(file, data) {

                var IS_JSON = true;

                try{
                    var data_json = $.parseJSON(data);
                }catch(err){
                    IS_JSON = false;
                    alert(data);
                    return false;
                }

                if ( IS_JSON ){

                    data = data_json;	

                    var image = data.file_name;

                    // elemento principal, contendo a estrutura da imagem
                    var el = $('#block-image-model');

                    // pagando o nome do arquivo se existir
                    var file_name = el.find('input[name="file_item"]').val();

                    if ( el.find('img').eq(0).hasClass("tem-imagem") ){

                        $.post( url_site+"index.php/upload/upload_remove", { "file_name": file_name }, function(data){ if ( data == 0 ) alert('Não foi possível excluir a imagem!') });

                    }

                    var url = $('#url-servidor').val();

                    // adicionando a imagem do UPLOAD
                    el.find('img').eq(0).attr("src",url+image).addClass("tem-imagem");
                    el.find('input[name="item_ids"]').removeAttr('disabled');
                    el.find('input[name="file_item"]').val(image).removeAttr('disabled');
                    el.show();

                    $('input[name="local"]').val(url+image);

                    //var file = data.file_name;
                    /*
                    $.ajax({

                        url: url_site+'index.php/item/processar_item/',
                        dataType: "html",
                        type: "POST",
                        data: data,
                        success: function(txt) {

                            if (!data){

                                alert("Ocorreu um erro ao adicionar.");

                            }

                            // limpando a busca, se houver
                            $('#list-modal .dataTables_filter :input').val(''); //CLEAR CURRENT VALUE IN THE SEARCH BAR
                            oModalTable.fnFilter('');

                        },complete: function(data) {

                            // recarregando o DataTable
                            oModalTable.fnClearTable();

                        }

                    });
                    */
                }

            }

        });
        
    }

    $(document).on('click','.remove-image',function(){
        
        if ( confirm('Deseja realmente excluir a imagem?') ){
            $(this).parent().remove();
        }
        
    });
    
    $('#form-item').submit(function(){

        var validation_error = false;
        $('#alert-erro .content-alert').html('');
        $('#alert-erro').hide();

        if ( jQuery.trim($('#url-local').val()) == '' ){
            $('<p>Por favor, você precisa enviar o arquivo</p>').appendTo('#alert-erro .content-alert');
            $('#alert-erro').show();
            return false;
        }

        return true;
        
    });

})
