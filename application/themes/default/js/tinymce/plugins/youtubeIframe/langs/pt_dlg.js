tinyMCE.addI18n('pt.youtube_dlg',{
    title: 'Inserir/editar v&iacute;deos do Youtube',
    url_field: 'Url ou c&oacute;digo do v&iacute;deo no YouTube:',
    url_example1: 'Exemplo de URL',
    url_example2: 'Exemplo de C&oacute;digo',
    choose_size: 'Escolha o tamanho',
    custom: 'Personalizar',
    Width: 'Largura',
    Height: 'Altura',
    iframe: 'Novo estilo com iFrame',
    embed: 'Estilo antigo com Embeded'
});
