        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          
            <ul class="nav navbar-nav side-nav">
            
                <li<?php if ( $this->router->fetch_class() == 'home' ){ ?> class="active"<?php } ?>>
                    <a href="<?php echo base_url('index.php/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>

                <li<?php if ( $this->router->fetch_class() == 'item' ){ ?> class="active"<?php } ?>>
                    <a href="<?php echo base_url('index.php/item') ?>"  class="active"><i class="fa fa-film"></i> Itens</a>
                </li>

                <li<?php if ( $this->router->fetch_class() == 'horoscope' ){ ?> class="active"<?php } ?>>
                    <a href="<?php echo base_url('index.php/horoscope') ?>"  class="active"><i class="fa fa-film"></i> Horoscopo</a>
                </li>

                <!--
                <li< ?php if ( $this->router->fetch_class() == 'versicle' || $this->router->fetch_class() == 'salmos' || $this->router->fetch_class() == 'versicle_category' ){ ?> class="active"< ?php } ?>>
                    <a href="javascript:;" data-target="#menu-versicle" data-toggle="collapse"><i class="fa fa-book"></i> Biblia <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="menu-versicle" class="collapse< ?php if ( $this->router->fetch_class() == 'versicle' || $this->router->fetch_class() == 'salmos' || $this->router->fetch_class() == 'versicle_category' ){ ?> in< ?php } ?>">
                        <li< ?php if ( $this->router->fetch_class() == 'versicle' ){ ?> class="active"< ?php } ?>>
                            <a href="< ?php echo base_url('index.php/versicle/') ?>"><i class="fa fa-book"></i> Versiculos</a>
                        </li>
                        <li< ?php if ( $this->router->fetch_class() == 'salmos' ){ ?> class="active"< ?php } ?>>
                            <a href="< ?php echo base_url('index.php/salmos') ?>"><i class="fa fa-child"></i> Salmos</a>
                        </li>
                        <li< ?php if ( $this->router->fetch_class() == 'versicle_category' ){ ?> class="active"< ?php } ?>>
                            <a href="< ?php echo base_url('index.php/versicle_category/') ?>"><i class="fa fa-bars"></i> Categorias</a>
                        </li>
                    </ul>
                </li>
                -->
                
                <li<?php if ( $this->router->fetch_class() == 'content_sms' ){ ?> class="active"<?php } ?>>
                    <a href="<?php echo base_url('index.php/content_sms/') ?>"><i class="fa fa-envelope-o"></i> Conteúdo SMS </a>
                </li>

                <li<?php if ( $this->router->fetch_class() == 'faq' ){ ?> class="active"<?php } ?>>
                    <a href="<?php echo base_url('index.php/faq/') ?>"><i class="fa fa-question-circle"></i> FAQ</a>
                </li>
                
                <li<?php if ( $this->router->fetch_class() == 'terms' ){ ?> class="active"<?php } ?>>
                    <a href="<?php echo base_url('index.php/terms/') ?>"><i class="fa fa-question-circle"></i> Termos de Uso</a>
                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right navbar-user">
                <li><a href="javascript:void(0);"><i class="fa fa-user"></i> <span id="login"><?php echo $this->session->userdata('login'); ?></span> </a></li>
                <li><a href="<?php echo base_url('index.php/logout'); ?>"><i class="fa fa-power-off"></i> logout</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
