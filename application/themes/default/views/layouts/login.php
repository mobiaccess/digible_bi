<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $template['title'] ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>font-awesome/css/font-awesome.min.css">
    
    <!-- JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/bootstrap.js"></script>

  </head>

  <body>

    <div id="wrapper" style="padding-left: 0">

        <?php echo $template['body']; ?>

    </div><!-- /#wrapper -->

  </body>
</html>