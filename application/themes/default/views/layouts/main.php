<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $template['title'] ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/sb-admin.css" rel="stylesheet">
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/sb-admin-others.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>font-awesome/css/font-awesome.min.css">
    
    <!-- JavaScript -->
    <!--<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery-1.10.2.js"></script>-->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery-1.11.0.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/bootstrap.js"></script>

    <?php echo $template['metadata'] ?>

    <?php /*
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/dataTables/DT_bootstrap.css" rel="stylesheet" type="text/css"/> 
    <script type="text/javascript" src="<?php echo base_url() . $this->template->get_theme_path() ?>js/dataTables/DT_bootstrap.js"></script> 
     */ ?>

    
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url() ?>">Zodiack</a>
        </div>
      
        <?php echo $this->template->load_view('partials/partial_menu'); ?>
        
      </nav>

      <div id="page-wrapper">
          
          <div class="container-fluid">
          
            <?php echo $template['body']; ?>
              
          </div>    

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

  </body>
</html>