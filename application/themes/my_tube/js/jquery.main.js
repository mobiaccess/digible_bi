$().ready(function(){

	$('.send-form').validate({
		submitHandler: function(){
			var curForm = $('.send-form');
			$("<div />").addClass("formOverlay").appendTo(curForm);  
			 	
				 $.ajax({
					url: mail_url + 'js/mail.php',
					type: 'POST',
					data: curForm.serialize(),
					success: function(data) {
					var res=data.split("::");
					curForm.find("div.formOverlay").remove();
					curForm.prev('.expMessage').html(res[1]);
					if(res[0]=='Success')
					{
					   curForm.remove(); 
					   curForm.prev('.expMessage').html('');
					}              
				  }
				 });
			return false;
		}	
	});

	$('.login-form').ajaxForm({
		success: function(responseText, statusText, xhr, $form){
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				// alert(resp.login);
				$('#error_message').html('');
				location.reload();
			} else {
				// alert(resp.login);
				$('#error_message').html(resp.reason);
			}
		}
	});

	// $('.signUp-form').validate({
	// 	submitHandler: function(form){

	// 		var pass = $('#signUp-password').val();
	// 		var confirmPass = $('#confirm-signUp-password').val()

	// 		if(pass == confirmPass && pass.length > 0) {
				
	// 			$(form).ajaxForm({
	// 				success: function(responseText, statusText, xhr, $form){
	// 					var resp = jQuery.parseJSON(responseText);
	// 					if (resp.login) {
	// 						// alert(resp.login);
	// 						$('#error_message').html('');
	// 						location.reload();
	// 					} else {
	// 						// alert(resp.login);
	// 						$('#error_message').html(resp.er_msg);
	// 					}	
	// 				}
	// 			});

	// 		} else if(pass != confirmPass) {
	// 			$('#cad_error_message').html('as senhas precisam ser idênticas');
	// 		}
	// 	}
	// });

	$('.signUp-form').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				// alert(resp.login);
				$('#error_message').html('');
				// location.reload();
				alert(resp.reason);
			} else {
				// alert(resp.login);
				$('#error_message').html(resp.reason);
				alert(resp.reason);
			}	
		}
	});

	$('.signUp-msisdn-form-end').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				location.reload();
			}
		}
	});

	$('.signUp-msisdn-form').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				alert("Parabéns, você receberá sua senha por SMS");
			} else {
				alert(responseText);
			}
		}
	});

	$('.login-msisdn-form').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				location.reload();
			}
		}
	});

	$('#subscriber-frm-button').on('click', function(){
		$('#text_product').html('Informe seu número para aproveitar o MobiVídeos por completo!');
		$('#product_detail').hide();
		$('#bt-subs-um').html('Enviar');
		$('.subscriber-frm-button').hide();
		$('.subscription-frm-button').show();
	});

	$('#subscription-frm-button').on('click', function(){
		$('#text_product').html('Tenha Acesso ILIMITADO a webseries inéditas! Humor, beleza, curiosidades, documentários, culinária e muito mais!');
		$('#product_detail').show();
		$('#bt-subs-um').html('Assinar');
		$('.subscriber-frm-button').show();
		$('.subscription-frm-button').hide();
	});

	/*$('.associate-msisdn-form').on('click',function(){
		// success: function(responseText, statusText, xhr, $form) {
		// 	var resp = jQuery.parseJSON(responseText);
		// 	if (resp.success) {
		// 		location.reload();
		// 	}
		// }
		console.log('Usuário precisa informar o msisdn');
    	$(".login-form").removeClass("form-active");
    	$(".signUp-form").removeClass("form-active");
    	$(".login-msisdn-form").removeClass("form-active");
    	$(".associate-msisdn-form").removeClass("form-active");
    	$(".associate-msisdn-form-end").addClass("form-active");
	});*/

	$('.associate-msisdn-form').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			console.log('return: ' + responseText);
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				$('#is_subscriber').attr('value', resp.is_subscriber);
				console.log('Usuário iniciou o associete-msisdn');
				// $("#wrapper").toggleClass("toggled");
		    	// $(".user-form").toggleClass("form-open");
		    	$(".login-form").removeClass("form-active");
		    	$(".signUp-form").removeClass("form-active");
		    	$(".login-msisdn-form").removeClass("form-active");
		    	$(".associate-msisdn-form").removeClass("form-active");
		    	$(".subscription-msisdn-form-end").addClass("form-active");	
			} else {
				console.log("msisdn não suportado");
				$('#cad_error_message_start_subs').html(resp.reason);
			}
		}
	});

	$('.associate-msisdn-form-end').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			console.log(responseText);
			var resp = jQuery.parseJSON(responseText);
			if (resp.success && resp.is_subscriber == null) {

				$(".login-form").removeClass("form-active");
		    	$(".signUp-form").removeClass("form-active");
		    	$(".login-msisdn-form").removeClass("form-active");
		    	$(".associate-msisdn-form").removeClass("form-active");
		    	$(".associate-msisdn-form-end").removeClass("form-active");
		    	$(".subscription-msisdn-form-end").removeClass("form-active");
		    	$(".subscription-msisdn-form").addClass("form-active");

				if (resp.data) {
					console.log('Exibir produtos');
					$.each(resp.data, function(k, v){
						console.log('k: ' + resp.data[k].subscription_type_code);
						// console.log('v: ' + v);
						$('#products_select').append($("<option></option>")
							.attr('value', resp.data[k].subscription_type_code)
							.text(resp.data[k].periodicity + ' / ' + resp.data[k].price ));
						
					});
				}

			} else if (resp.is_subscriber) {

				$(".login-form").removeClass("form-active");
		    	$(".signUp-form").removeClass("form-active");
		    	$(".login-msisdn-form").removeClass("form-active");
		    	$(".associate-msisdn-form").removeClass("form-active");
		    	$(".associate-msisdn-form-end").removeClass("form-active");
		    	$(".subscription-msisdn-form-end").removeClass("form-active");
		    	$(".subscription-msisdn-form").removeClass("form-active");
		    	$(".signUp-msisdn-form-end").addClass("form-active");

			} else {
				console.log("PIN CODE inválido");
				$('#cad_error_message').html(resp.reason);
			}
		}
	});	

	$('.subscription-msisdn-form').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			console.log(responseText);
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				$(".login-form").removeClass("form-active");
		    	$(".signUp-form").removeClass("form-active");
		    	$(".login-msisdn-form").removeClass("form-active");
		    	$(".associate-msisdn-form").removeClass("form-active");
		    	$(".subscription-msisdn-form").removeClass("form-active");
		    	$(".subscription-msisdn-form-end").addClass("form-active");
			}
		}
	});

	$('.subscription-msisdn-form-end').ajaxForm({
		success: function(responseText, statusText, xhr, $form) {
			console.log(responseText);
			var resp = jQuery.parseJSON(responseText);
			if (resp.success) {
				location.reload();
			} else {
				console.log("PIN CODE inválido");
				$('#cad_error_message_end_subs').html(resp.reason);
			}
		}
	});	
	
	$('#confirm-signUp-password').keyup(checkPasswordMatch);

	if(typeof(jProducts) != "undefined" && jProducts !== null) {
		var prodcts = jQuery.parseJSON(jProducts);
		$('#product_detail').html("R$ " + prodcts.price + "/" + prodcts.periodicity);
		$('#input_subscription_type_code').val(prodcts.subscription_type_code);

	}

});

function checkLogin(player, l, user, is_locked) {
	var local_user = jQuery.parseJSON(user);
	if (!l || local_user == null) {
		$("#wrapper").toggleClass("toggled");
    	$(".user-form").toggleClass("form-open");
		player.pause();
		return false;
	}
	if (!local_user.is_subscriber && !local_user.msisdn && is_locked) {
		 // vincular msisdn e depois fazer compra
		console.log('Usuário logado mas não assinante');
		$("#wrapper").toggleClass("toggled");
    	$(".user-form").toggleClass("form-open");
    	$(".login-form").removeClass("form-active");
    	$(".signUp-form").removeClass("form-active");
    	$(".login-msisdn-form").removeClass("form-active");
    	$(".associate-msisdn-form-end").removeClass("form-active");
    	if (!$('.subscription-msisdn-form-end').hasClass('form-active')) {
    		$(".associate-msisdn-form").addClass("form-active");
    	}
		player.pause();
		return false;

	} else if(!local_user.is_subscriber && local_user.msisdn && is_locked) {
		// console.log("Ususário tem msisdn mas não é assinante");
		// console.log("url: " + url_producs);
		
		$('#msisdn_subs_start').attr('type', 'hidden');
		$('#msisdn_subs_start').attr('value', local_user.msisdn);

		$('#fav_icon').remove();
		
		$("#wrapper").toggleClass("toggled");
    	$(".user-form").toggleClass("form-open");
    	$(".login-form").removeClass("form-active");
    	$(".signUp-form").removeClass("form-active");
    	$(".login-msisdn-form").removeClass("form-active");
    	$(".associate-msisdn-form-end").removeClass("form-active");
    	if (!$('.subscription-msisdn-form-end').hasClass('form-active')) {
    		$(".associate-msisdn-form").addClass("form-active");
    	}

    	player.pause();
		return false;

		/*$.get(url_producs, function(producs){

			$("#wrapper").toggleClass("toggled");
    		$(".user-form").toggleClass("form-open");
			$(".login-form").removeClass("form-active");
	    	$(".signUp-form").removeClass("form-active");
	    	$(".login-msisdn-form").removeClass("form-active");
	    	$(".associate-msisdn-form").removeClass("form-active");
	    	$(".associate-msisdn-form-end").removeClass("form-active");
	    	$(".subscription-msisdn-form-end").removeClass("form-active");
	    	$(".subscription-msisdn-form").addClass("form-active");

	    	var resp = jQuery.parseJSON(producs);

			if (resp.data) {
				console.log('Exibir produtos');
				$.each(resp.data, function(k, v){
					console.log('k: ' + resp.data[k].subscription_type_code);
					// console.log('v: ' + v);
					$('#products_select').append($("<option></option>")
						.attr('value', resp.data[k].subscription_type_code)
						.text(resp.data[k].periodicity + ' / ' + resp.data[k].price ));
					
				});
			}

		});*/
		
	} 
	else {
		if (local_user.is_subscriber && !local_user.has_credit && is_locked) {
			alert('Conteúdo exclusivo para assinantes. Recarregue e curta o MobiVídeos sem restrições.');
			player.pause();
			return false;
		}
	}
}

function checkPasswordMatch(){
	var pass = $('#signUp-password').val();
	var confirmPass = $('#confirm-signUp-password').val()
	if(pass != confirmPass) {
		$('#cad_error_message').html('as senhas não conferem');
	} else {
		$('#cad_error_message').html('');
	}
}