<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MobiVídeo</title>
	<meta name="viewport" content="initial-scale=1.0, width=device-width" />

	<!-- Google login -->
	<meta name="google-signin-client_id" content="940711316888-aprlnhheq2so5g783d6d7giofi52vvib.apps.googleusercontent.com">
	
	<!-- Style Sheets -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/bootstrap.min.css">
	<!-- Font Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/socicon-styles.css">	
	<!-- Font Icons -->
	<link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/hover-min.css" />
	<link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/animate.css" />
	<link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/css-menu.css" />
	<link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/owl.carousel.css" />
	<link rel="stylesheet" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/loader.css" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/styles.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . $this->template->get_theme_path() ?>css/responsive.css">

	<link rel="icon" type="image/png" href="/favicon.png" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Loader -->
<div id="loader-container">
	<div id="loader">
	  <ul>
	    <li></li>
	    <li></li>
	    <li></li>
	    <li></li>
	    <li></li>
	    <li></li>
	  </ul>
	</div>
</div>
<!-- Loader -->

<div id="wrapper">
	<div id="main-content">
	<!-- Main Bar-->
	<div id="main-bar">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-5 col-xs-6 border-right">
					<div class="logo">
						<a href="<?php echo site_url()  ?>">
							<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/mobivideos_logo.png" alt="Logo">	
						</a>
					</div>					
				</div>
				<div class="col-sm-7 col-xs-6 hidden-md hidden-lg text-right">
					<!-- <button type="button" class="btn btn-default btn-create-album">
						Create Album
					</button> -->
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="clearfix visible-xs"></div>
				<div class="col-md-6 col-sm-4 border-right sm-border-top">
					<div class="search-box">
						<!-- <form method="post"> -->
						<?= form_open('index.php/search', 'method="get"'); ?>
							<input type="text" name="search" id="search" class="form-control" placeholder="Buscar" required>
							<button type="submit" class="search-icon">
								<i class="fa fa-search"></i>
							</button>
						</form>						
					</div>
				</div>
				<div class="col-md-2 col-sm-4 sm-border-top">
					<div class="social-icon">
						<ul class="list-inline list-unstyled">
							<li><a href="https://www.facebook.com/MobiVideos/" target="_blanck"><i class="fa fa-facebook" style="font-size: 1.73em;"></i></a></li>
							<li><a href="https://twitter.com/Mobi_Videos" target="_blanck"><i class="fa fa-twitter" style="font-size: 1.73em;"></i></a></li>
							<!-- <li><a href="#"><i class="fa fa-instagram"></i></a></li> -->
							<!-- <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube"></i></a></li> -->
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-sm-4 border-left sm-border-top">
					<div class="login">
						<div class="media">
							<div class="media-left">
								<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/user.svg" alt="play" onerror="this.src='images/user.png'">
							</div>
							<div class="media-body">
								<p>
									<?php if (isset($user)) : ?>
									<?php $locate = array('index.php', 'login', 'logout'); ?>
									<!-- <?php print_r($user); ?> -->
										<p class="limit_ponto"><?= ($user->email) ? $user->email : $user->msisdn; ?></p> <a class="" href="<?php echo site_url($locate);  ?>" onclick="fbLogout();">Sair</a>
									<?php else : ?>
										Olá Visitante <a class="login-toggle" href="#">Entrar</a>
									<?php endif; ?>
								</p>
							</div>
						</div>									
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Main Bar -->

	<!-- Main Navigation -->
	<div id="main-navigation">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-sm-12">
					<div id='cssmenu'>
						<ul>
						    <li class="active"><a href="<?php echo site_url()  ?>"><i class="fi ion-ios-home"></i>Início</a>
							  <!--ul>
							  	<li><a href="index.html">Home V1</a>
									<ul>
										<li><a href="index.html">Home V1</a></li>
										<li><a href="index-left-sidebar.html">With Left Sidebar</a></li>
										<li><a href="index-without-sidebar.html">Without Sidebar</a></li>
									</ul>
							  	</li>
							  	<li><a href="../video-v2/index-v2.html">Home V2</a>
                                <ul>
											<li><a href="../video-v2/index-v2.html">Home V2</a></li>
											<li><a href="../video-v2/index-left-sidebar.html">With Left Sidebar</a></li>
											<li><a href="../video-v2/index-without-sidebar.html">Without Sidebar</a></li>
										</ul>
                                </li>
							  </ul-->
						   </li>
						   <!--li><a href='#'><i class="fi ion-ios-film-outline"></i>Video Styles</a>
						   	  <ul>
							  	<li><a href="video-2-column.html">Video 2 Column</a></li>
							  	<li><a href="video-3-column.html">Video 3 Column</a></li>
							  	<li><a href="video-4-column.html">Video 4 Column</a></li>
							  </ul>						      
						   </li-->
						   <li><a href="#"><i class="fi ion-android-apps"></i>Categorias</a>
								<ul>
									<?php foreach ($categories as $cat) { ?>
									<?php $locate = array('index.php', 'category', 'index', $cat->id_movie_category); ?>
									<li><a href="<?php echo site_url($locate)  ?>"><?= $cat->movie_category; ?></a></li>
									<?php } ?>
								</ul>
						   </li>
						   <!--li><a href="#"><i class="fi ion-compose"></i>Blog</a>
								<ul>
									<li><a href="blog-grid.html">Blog Grid Style</a></li>
									<li><a href="blog-listing.html">Blog Listing Style</a></li>
									<li><a href="blog-detail.html">Blog Detail</a></li>
								</ul>
						   </li-->
						   <!--li><a href="tv.html"><i class="fi ion-android-desktop"></i>Tv</a></li-->
						   <!--li><a href="about.html"><i class="fi ion-person"></i>About</a></li-->
						</ul>
					</div>
				</div>
				<!--div class="col-md-2 text-right hidden-sm hidden-xs">
					<button type="button" class="btn btn-default btn-create-album">
						Create Album
					</button>
				</div-->
			</div>
		</div>
	</div>
	<!-- Main Navigation -->

	<?php echo $template['body']; ?>

	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-about wow fadeIn" data-wow-duration="0.5s">
						<div class="vid-heading">
							<span>Sobre</span>
							<div class="hding-bottm-line"></div>
						</div>
						<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/mobivideos_logo.png" alt="Logo">
						<p>
						O MobiVídeos é um produto de streaming de vídeos com Webséries inéditas, exclusivas para você! Aqui você encontra muito Humor, Dicas de Beleza, Documentários, Receitas, Curiosidades e muito mais. Compartilhe vídeos com os amigos nas Mídias Sociais. faça tudo isso quando e onde quiser: do seu celular, tablet ou computador. Aproveite!
						</p>
						<!-- <p>
							Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequatauctor eu in elit.  Class aptent tac Duis sed odio s psum velit.
						</p> -->
						<?php $locate = array('index.php', 'home', 'infolegais'); ?>
						<a href="<?php echo site_url($locate)  ?>" style="color: #b53b75;">Termos & Condições</a>
						<br />
						<?php $locate = array('index.php', 'home', 'infolegais', 'privacy'); ?>
						<a href="<?php echo site_url($locate)  ?>" style="color: #b53b75;">Política de privacidade</a>
						<br />
						<?php $locate = array('index.php', 'home', 'infolegais', 'faq'); ?>
						<a href="<?php echo site_url($locate)  ?>" style="color: #b53b75;">FAQ</a>
						<br/ >
						<br />
						<ul class="bottom-social list-inline list-unstyled">
							<li><a href="https://www.facebook.com/MobiVideos/" target="_blanck"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/Mobi_Videos" target="_blanck"><i class="fa fa-twitter"></i></a></li>
							<!-- <li><a href="#"><i class="fa fa-pinterest"></i></a></li> -->
							<!-- <li><a href="#"><i class="fa fa-instagram"></i></a></li> -->
							<!-- <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="bottom-post wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
						<div class="vid-heading">
							<span>Últimas Postagens</span>
							<div class="hding-bottm-line"></div>
						</div>
						<div class="latest-post">
							<?php foreach ($home->movies_footer_sugest as $movi) { ?>
							<?php $locate = array('index.php', 'player', 'index', $movi->id_movie); ?>
							<div class="media">
								<div class="media-left">
									<img src="<?= $movi->thumbnail ?>" alt="video" width="90" height="60">
								</div>
								<div class="media-body">
									<h1><a href="<?php echo site_url($locate)  ?>"><?= $movi->title ?></a></h1>
									<!-- <p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p> -->
								</div>
							</div>
							<?php } ?>
							<!-- <div class="media">
								<div class="media-left">
									<img src="<?php echo base_url() . $this->template->get_theme_path() ?>images/most-like-img-2.jpg" alt="video">
								</div>
								<div class="media-body">
									<h1><a href="blog-detail.html">Martial Art</a></h1>
									<p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p>
								</div>
							</div>
							<div class="media">
								<div class="media-left">
									<img src="<?php echo base_url() . $this->template->get_theme_path() ?>images/most-like-img-3.jpg" alt="video">
								</div>
								<div class="media-body">
									<h1><a href="blog-detail.html">Streat Crime</a></h1>
									<p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="bottom-categories wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.4s">
						<div class="vid-heading">
							<span>Categorias</span>
							<div class="hding-bottm-line"></div>
						</div>
						<ul class="category-list list-unstyled">
							<?php foreach ($categories as $cat) { ?>
							<?php $locate = array('index.php', 'category', 'index', $cat->id_movie_category); ?>
							<li><a href="<?php echo site_url($locate)  ?>"><?= $cat->movie_category; ?></a></li>
							<?php } ?>
							<!-- <li><a href="category-listing.html">Action</a></li>
							<li><a href="category-listing.html">Adventure</a></li>
							<li><a href="category-listing.html">War</a></li>
							<li><a href="category-listing.html">Romance</a></li>
							<li><a href="category-listing.html">Comedy</a></li>
							<li><a href="category-listing.html">Horror</a></li> -->
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="sendus wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.6s">
						<div class="vid-heading">
							<span>Nos envie uma mensagem</span>
							<div class="hding-bottm-line"></div>
						</div>
						<div id="contact-form">
							<div class="expMessage"></div>
							<form class="send-form">
								<input type="text" name="formInput[name]" id="name" class="form-control half-wdth-field" placeholder="Nome" required>
								<input type="email" name="formInput[email]" id="email" class="form-control half-wdth-field pull-right" placeholder="Email" required>

								<textarea name="formInput[message]" id="message" class="form-control" rows="4" placeholder="Mensagem" required></textarea>
								<input type="hidden" name="action" value="submitform">
								<div>
									<button type="submit" value="submit" class="btn btn-send">
										Enviar
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- copyright -->	
	<div id="copyright">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<p>
						<img src="<?php echo base_url() . $this->template->get_theme_path() ?>images/logo_acotel.png" alt="logo acotel" height="45" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- copyright -->

	</div>

	<div id="login-box">
		<i class="fa fa-remove login-toggle"></i>
		<!-- Login from -->
		<?= form_open('index.php/login/authenticate', 'class="user-form login-form form-active text-center"'); ?>
		<!-- <form class="user-form login-form form-active text-center"> -->
					<h1 class="form-heading">Entre com os dados da sua conta</h1>
					<h1 class="form-heading" id="error_message" style="color: #d82727"></h1>
					<!-- <a href="#" class="btn btn-login w-facebook"><i class="fa fa-facebook"></i>Facebook</a> -->
					<!-- <fb:login-button class="btn" data-size="large" scope="public_profile,email" onlogin="checkLoginState();">
					</fb:login-button>
					<div class="g-signin2 fa" data-onsuccess="onSignIn"></div> -->
					<!-- <a href="#" class="btn btn-login w-twitter"><i class="fa fa-twitter"></i>Twitter</a> -->
					<!-- <p class="or-login">ou</p> -->
					<div class="form-group">
						<!-- <input type="email" name="email" class="form-control" id="login-email" required> -->
						<?= form_input('email', '', 'class="form-control" id="login-email" required placeholder="Email"') ?>
						<i class="fa fa-envelope input-icon"></i>
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" id="login-password" required>
						<i class="fa fa-key input-icon"></i>
					</div>
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Entrar
					</button>
					<!-- <p class="forgot-password">
						Esqueceu sua senha? <a href="#">Click aqui</a>
					</p> -->
					<!-- <br />
					<p class="no-account" id="login-msisdn-click">
						Login por Telefone <a href="#" id="login-msisdn-frm">Click aqui</a>
					</p> -->
					<br />
					<p class="no-account">
						Ainda não tem cadastro? <a id="signUp-frm-button" href="#">Cadastrar</a>
					</p>
					<?= form_hidden('url_locate', $url) ?>
		</form>

		<!-- Login from -->
		<?= form_open('index.php/login/authenticate_msisdn', 'class="user-form login-msisdn-form text-center"'); ?>
		<!-- <form class="user-form login-form form-active text-center"> -->
					<h1 class="form-heading">Entre com os dados da sua conta</h1>
					<h1 class="form-heading" id="error_message" style="color: #d82727"></h1>
					<div class="form-group">
						<input type="tel" name="msisdn" class="form-control" id="login-msisdn" placeholder="DDD+Telefone" required>
						<i class="fa fa-comment input-icon"></i>
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" id="login-password" required>
						<i class="fa fa-key input-icon"></i>
					</div>
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Login
					</button>
					<!-- <p class="forgot-password">
						Esqueceu sua senha? <a href="#">Click aqui</a>
					</p> -->
					<br />
					<p class="no-account">
						Já tem cadastro com email? <a id="login-frm-button-2" href="#">Login</a>
					</p>
					<?= form_hidden('url_locate', $url) ?>
		</form>
		
		<!-- SignUp form -->
		<?= form_open('index.php/login/create', 'class="user-form signUp-form text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading">Cadastrar</h1>
					<h1 class="form-heading" id="cad_error_message" style="color: #d82727"></h1>
					<div class="form-group">
						<input placeholder="e-mail" type="email" name="email" class="form-control" id="signUp-email" required>
						<i class="fa fa-envelope input-icon"></i>
					</div>
					<div class="form-group">
						<input placeholder="senha" type="password" name="password" class="form-control" id="signUp-password" required>
						<i class="fa fa-key input-icon"></i>
					</div>
					<div class="form-group">
						<input placeholder="confirme a senha" type="password" name="confim-password" class="form-control" id="confirm-signUp-password" required>
						<i class="fa fa-key input-icon"></i>
					</div>
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Enviar
					</button>
					<br />
					<p class="no-account">
						Já tem cadastro? <a id="login-frm-button" href="#">Login</a>
					</p>
		</form>

		<?= form_open('index.php/login/pkg_crl_susbcription_start', 'class="user-form signUp-msisdn-form text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading">Tenha Acesso ILIMITADO a webseries inéditas! Humor, beleza, curiosidades, documentários, culinária e muito mais!</h1>
					<h1 class="form-heading" id="cad_error_message" style="color: #d82727"></h1>
					<h1 class="form-heading">Informe seu número de celular</h1>
					<div class="form-group">
						<input type="tel" name="msisdn" class="form-control" id="login-msisdn" placeholder="DDD+Telefone" required>
						<i class="fa fa-comment input-icon"></i>
					</div>			
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Sign Up
					</button>
					<!-- <p class="forgot-password">
						Forgot your password? <a href="#">Click here</a>
					</p> -->
					<br />
					<p class="no-account">
						Já tem cadastro? <a id="login-frm-button-3" href="#">Login</a>
					</p>
		</form>

		<?= form_open('index.php/login/associate_msisdn_end', 'class="user-form signUp-msisdn-form-end text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading">Informe o código recebido.</h1>
					<h1 class="form-heading" id="cad_error_message" style="color: #d82727"></h1>
					<div class="form-group">
						<input type="tel" name="pincode" class="form-control" id="login-msisdn" placeholder="Pin code" required>
						<i class="fa fa-comment input-icon"></i>
					</div>			
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Sign Up
					</button>
					<!-- <p class="forgot-password">
						Forgot your password? <a href="#">Click here</a>
					</p> -->
					<br />
					<p class="no-account">
						Já tem cadastro? <a id="login-frm-button-3" href="#">Login</a>
					</p>
		</form>

		<?= form_open('index.php/login/subscription_start', 'class="user-form associate-msisdn-form text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading" id="text_product">Tenha Acesso ILIMITADO a webseries inéditas! Humor, beleza, curiosidades, documentários, culinária e muito mais!</h1>
					<h1 class="form-heading" id="product_detail"></h1>
					<h1 class="form-heading" id="cad_error_message_start_subs" style="color: #d82727"></h1>
					<input type="hidden" name="subscription_type_code" id="input_subscription_type_code">
					<div class="form-group">
						<input type="tel" name="msisdn" id="msisdn_subs_start" class="form-control" placeholder="DDD+Telefone" required>
						<i class="fa fa-comment input-icon" id="fav_icon"></i>
					</div>			
					<button type="submit" class="btn btn-block btn-login text-uppercase" id="bt-subs-um">
						Assinar
					</button>
					<br />
					<p class="no-account subscriber-frm-button">
						Já sou assinante? <a id="subscriber-frm-button" href="#">Clique aqui.</a>
					</p>
					<p class="no-account-subs subscription-frm-button" hidden="true">
						Quero assinar? <a id="subscription-frm-button" href="#">Clique aqui.</a>
					</p>
					<!-- <p class="forgot-password">
						Forgot your password? <a href="#">Click here</a>
					</p> -->
		</form>

		<?= form_open('index.php/login/associate_msisdn_start', 'class="user-form associate-msisdn-form-end text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading">Informe seu número de celular</h1>
					<h1 class="form-heading" id="cad_error_message" style="color: #d82727"></h1>
					<div class="form-group">
						<input type="tel" name="msisdn" class="form-control" id="pincode-msisdn" placeholder="DDD+Telefone" required>
						<i class="fa fa-comment input-icon"></i>
					</div>
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Enviar
					</button>
					<!-- <p class="forgot-password">
						Forgot your password? <a href="#">Click here</a>
					</p> -->
		</form>

		<?= form_open('index.php/login/subscription_start', 'class="user-form subscription-msisdn-form text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading">Selecione um produto</h1>
					<div class="form-group">
						<select id="products_select" name="subscription_type_code"></select>
					</div>
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Assinar
					</button>
					<!-- <p class="forgot-password">
						Forgot your password? <a href="#">Click here</a>
					</p> -->
		</form>

		<?= form_open('index.php/login/subscription_end', 'class="user-form subscription-msisdn-form-end text-center"'); ?>
		<!-- <form class="user-form signUp-form text-center"> -->
					<h1 class="form-heading">Informe o código recebido.</h1>
					<h1 class="form-heading" id="cad_error_message_end_subs" style="color: #d82727"></h1>
					<div class="form-group">
						<input type="hidden" name="is_subscriber" id="is_subscriber">
						<input type="tel" name="pincode" class="form-control" id="login-msisdn" placeholder="Pin code" required>
						<i class="fa fa-comment input-icon"></i>
					</div>			
					<button type="submit" value="login" class="btn btn-block btn-login text-uppercase">
						Confirmar
					</button>
		</form>

	</div>
</div>		

	<!-- Scripts -->
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/wow.min.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery-1.12.3.min.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/css-menu.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.validate.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/custom.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.form.min.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.main.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.facebook.js"></script>
	<script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.google.js"></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script type="text/javascript">
		var url_producs = "<?php echo site_url(array('index.php', 'login', 'list_products', '1'))  ?>";
		var mail_url = "<?php echo base_url() . $this->template->get_theme_path() ?>";
	</script>
	
</body>
</html>