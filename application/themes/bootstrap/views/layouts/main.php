<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <mete name="keywords" content="" >
    <meta name="author" content="">

    <title>Jogos de Sempre</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/freelancer.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>

</head>

<body class="index" id="page-top">

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-light" id="mainNav">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            Menu <i class="fa fa-bars"></i>
        </button>
        <div class="container">
            <div class="imgwrapper">
                <a class="navbar-brand" href="#page-top"><img style="width: 70%;" class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>/img/portfolio/logo_jogos_de_sempre.png" alt="Logo Jogos de Jempre"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <?php if (isset($user)) : ?>
                            <?php $locate = array('index.php', 'login', 'logout'); ?>
                            <span class="nav-link text-white"><?= substr($user['msisdn'], -11); ?> (<a href="<?php echo site_url($locate);  ?>" id="linkLogout">Sair</a>)</span>    
                        <?php else : ?>
                            <a class="nav-link linkLogin" href="#">Entrar</a>
                        <?php endif; ?>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#portfolio">Jogos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">Sobre</a>
                    </li>
                    <?php if (isset($user)) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="#" id="myDownloadsLink">Meus Downloads</a>
                        </li>
                    <?php endif; ?>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="faqLink">Dúvidas frequentes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="termosLink">Termos de uso</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contato</a>
                    </li>
                    <?php if (isset($user)) : ?>
                        <li class="nav-item">
                            <!--?= form_open('index.php/subscription_api/sendUnsubscribe', 'id="form-unsubscribe"'); ?>
                                 <input type="submit" value="Cancelar assinatura" class="btn btn-primary"--> 
                                 <a class="nav-link" href="#" id="cancelLink">Cancelar assinatura</a>
                            <!--/form-->
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <img class="img-fluid" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/portfolio/jds-bk.png" alt="">
            <div class="intro-text">
                <span class="name">Jogos de Sempre</span>
                <hr class="star-light">
                <!-- <span class="skills">Web Developer - Graphic Artist - User Experience Designer</span> -->
                <?php if (!isset($user)) : ?>
                    <!--?= form_open('index.php/login', 'class="form-inline justify-content-center" id="form-login-exp"'); ?>
                        <div class="form-group" style="padding: 0 5px; 0 5px">
                            <label for="inputDDD" class="sr-only">Telefone</label>
                            <input type="number" name="msisdn" class="form-control" placeholder="Celular" maxlength="11" required>
                        </div>
                        <div class="form-group" style="padding: 0 5px; 0 5p">
                            <label for="inputMsisdn" class="sr-only">Senha</label>
                            <input type="password" name="password" class="form-control" placeholder="Senha" required>
                        </div>
                        <div style="padding: 0 5px; 0 5p">
                              <input type="submit" class="btn btn-primary" value="Continuar"> <i class="fa fa-chevron-right"></i></button>  
                        </div-->
                        <div>
                            <a href="#" style="margin-bottom: 10px;" class="btn btn-primary btn-lg btnConfirmSubscription">Quero Assinar</a>
                            <a href="#" class="btn btn-primary linkLogin">Já sou Assinante</a>
                        </div>
                        <p style="font-size: 10pt;">Produto exclusivo para usuário com dispositivo Android.</p>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </header>

    <?php echo $template['body']; ?>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <h2 class="text-center">Sobre</h2>
            <hr class="star-light">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <!-- <p>Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional LESS stylesheets for easy customization.</p> -->
                    <p>Jogos de Sempre é um clube de games com a versão digital de jogos clássicos e famosos de marcas consagradas como a Estrela, incluindo Banco Imobiliário, Jogo da Vida, Genius, QI, Lig 4, Batalha Naval, War Strategy e muitos outros.</p>
                    <p>Faça parte do clube, sempre com novidades e lançamentos. Assinatura semanal por R$5,19 e a cada semana você pode baixar um jogo novo, que é seu pra sempre.</p>
                </div>
                <!-- <div class="col-lg-4">
                    <p>Whether you're a student looking to showcase your work, a professional looking to attract clients, or a graphic artist looking to share your projects, this template is the perfect starting point!</p>
                </div>
                <div class="col-lg-8 offset-lg-2 text-center">
                    <a href="#" class="btn btn-lg btn-outline">
                        <i class="fa fa-download"></i> Download Theme
                    </a>
                </div> -->
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <h2 class="text-center">Contato</h2>
            <hr class="star-primary">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <!-- <form name="sentMessage" id="contactForm" novalidate> -->
                    <?= form_open('index.php/home/send_email', 'id="form-send-email"'); ?>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Nome</label>
                                <input class="form-control" name="name" id="name" type="text" placeholder="Nome" required data-validation-required-message="Por favor informe seu nome.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Email</label>
                                <input class="form-control" name="email" id="email" type="email" placeholder="Email" required data-validation-required-message="Por favor informe seu email.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Telefone</label>
                                <input class="form-control" name="phone" id="phone" type="tel" placeholder="Telefone" required data-validation-required-message="Por favor informe seu telefone.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls">
                                <label>Mensagem</label>
                                <textarea class="form-control" name="message" id="message" rows="5" placeholder="Mensagem" required data-validation-required-message="Por favor informe sua mensagem."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success btn-lg" value="Enviar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <!-- <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>3481 Melrose Place
                            <br>Beverly Hills, CA 90210</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a class="btn-social btn-outline" href="#"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-social btn-outline" href="#"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-social btn-outline" href="#"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-social btn-outline" href="#"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-social btn-outline" href="#"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>About Freelancer</h3>
                        <p>Freelance is a free to use, open source Bootstrap theme created by <a href="http://startbootstrap.com">Start Bootstrap</a>.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Your Website 2017
                    </div>
                </div>
            </div>
        </div>
    </footer> -->

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top hidden-lg-up">
        <a class="btn btn-primary page-scroll" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- Login Modal -->
    <div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Já sou assinante</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <?= form_open('index.php/login', 'id="form-login"'); ?>  <!-- class="form-inline justify-content-center"  -->
                <div class="form-group">
                    <input type="number" name="msisdn" placeholder="Celular" class="form-control" maxlength="11" required>
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Senha" class="form-control" required>
                </div>
                <div class="form-group forgot_list_lines">
                    <ul>
                        <li><input type="submit" value="Enviar" class="btn btn-primary"></li>
                        <li style="margin-left:5px"><a href="#" id="aForgotPassword"><p class="subscription">Esqueci minha senha</p></a></li>
                    </ul>
                </div>
            </form>
            <hr>
            <p class="subscription">Faça parte do clube, sempre com novidades e lançamentos. A cada semana você pode baixar um novo jogo, que é seu para sempre. Assinatura semanal por R$5,19.</p> 
            <a href="#" class="btn btn-primary btnConfirmSubscription">Quero Assinar</a>
            <p style="font-size: 10pt; margin-top: 10px;">Produto exclusivo para usuário com dispositivo Android.</p>
        <!-- <p>Some text in the modal.</p> -->
        </div>
        <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
        </div>

    </div>
    </div>

    <!-- Login Modal -->
    <div id="forgotPasswordModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Entre com seus dados</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <?= form_open('index.php/login/forgot_password', 'id="form-forgot-password"'); ?>  <!-- class="form-inline justify-content-center"  -->
                <div class="form-group">
                    <input type="number" name="msisdn" placeholder="Celular" class="form-control" maxlength="11" required>
                </div>
                <div class="form-group">
                    <input type="submit" value="Enviar" class="btn btn-primary">
                </div>
            </form>
        <!-- <p>Some text in the modal.</p> -->
        </div>
        <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
        </div>

    </div>
    </div>

    <!-- FAQ Modal -->
    <div id="faqModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Dúvidas Frequentes</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <p><strong>O que é o serviço Jogos de Sempre?</strong><br/>
                Jogos de Sempre é um clube de jogos clássicos e famosos de cartas e tabuleiro das marcas mais
                reconhecidas, como por exemplo, Brinquedos Estrela. Todos os jogos são disponibilizados em versões
                completas e sem anúncios, para download em aparelhos Android. </p>
            <p>
                <strong>Quais jogos estão disponíveis?</strong><br/>
                Estrela: Jogo da Vida, QI, Banco Imobiliário, Genius, Cilada e Lig4.
                Clássicos: War Strategy, Memo Quest, Batalha Naval, Carta Futebol Clube, Blackjack 21, ChinChon,
                Rouba Monte, Mau Mau e Escopa.
                Novos jogos serão incluídos periodicamente, fique ligado! </p>
            <p>
                <strong>Como funciona o serviço?</strong><br/>
                Jogos de Sempre é um clube de assinatura no valor de R$5,19 por semana com renovação automática. A
                cada semana tarifada, você pode fazer o download de 1 (um) jogo do catálogo.
                Uma vez baixado, o jogo é seu, desde que você não mude seu número de telefone. Obs: no máximo 5
                downloads do mesmo jogo. </p>
            <p>
                <strong>Onde posso baixar e jogar os jogos?</strong><br/>
                Você pode acessar o site tim.jogosdesempre.com.br do seu celular Android e seguir o processo de
                assinatura. A outra alternativa é enviar SMS para 77085, confirmar enviando SIM e clicar no link recebido
                para acessar o site “Jogos de Sempre”. Depois, basta fazer o download do jogo escolhido pelo site
                tim.jogosdesempre.com.br e ele ficará instalado em seu Smartphone ou Tablet. </p>
            <p>
                <strong>Existe um limite de conteúdos que eu posso consumir?</strong><br/>
                A assinatura no valor de R$5,19 do serviço Jogos de Sempre oferece o direito ao download de 1 (um)
                jogo por semana. Ou seja, a cada semana tarifada você poderá baixar um novo jogo de sua escolha.
                Caso decida cancelar e assinar novamente, você consegue baixar um novo jogo, porém será tarifado
                novamente. </p>
            <p>
                <strong>Quais os requisitos para acessar o conteúdo do Jogos de Sempre?</strong><br/>
                Você precisa possuir um Smartphone ou Tablet com sistema operacional Android, configurado para
                permitir download de apps de fontes desconhecidas. Este serviço não é compatível com celulares que
                possuem os sistemas operacionais: JAVA, Firefox, Windows Phone ou iOS. </P>
            <p>
                <strong>Como faço para configurar a permissão de apps de fontes desconhecidas?</strong><br/>
                Os Jogos do clube são externos ao Google Play, portanto é necessário configurar a permissão em seu
                aparelho. Acesse a configuração de seu Android em: Segurança -&gt; Fontes desconhecidas e marque o
                campo “Permitir a instalação de aplicativos de fontes desconhecidas”.
                Ao instalar qualquer aplicativo externo, é exibido um alerta padrão, porém a ação é totalmente segura
                para todos os jogos do clube.
                Dúvidas sobre aparelhos específicos, consulte o suporte do fabricante. </P>
            <p>
                <strong>Como faço para baixar um jogo?</strong><br/>
                Após configurar a permissão para apps de fontes desconhecidas, escolha o jogo no menu, leia os
                detalhes e clique no botão &quot;quero baixar&quot;. Siga as instruções, preencha com seu número de telefone e a
                senha que enviaremos por SMS e pronto. O download é iniciado automaticamente. Acompanhe o
                andamento do download na janela superior do seu celular (deslizando com o dedo na tela de cima para
                baixo). Ao finalizar, toque sobre o arquivo e siga o passo a passo.
                Os jogos são aplicativos e ficarão disponíveis livremente em seu aparelho. Boa diversão! </p>
            <p>
                <strong>Mudei o número do meu celular. Preciso assinar o serviço Jogos de Sempre novamente?</strong><br/>
                Sim. A sua assinatura está vinculada ao número do celular cadastrado. Se o seu número foi alterado,
                você deve fazer uma nova assinatura. Se o aparelho for o mesmo, os jogos já baixados são mantidos. </p>
            <p>
                <strong>Se eu trocar de aparelho celular terei que assinar o serviço de novo?</strong><br/>
                Não. Sua assinatura está associada ao seu número de telefone celular e não ao seu aparelho, o que
                significa que você não precisa assinar o serviço novamente, caso já seja assinante. Porém, para ter os
                jogos desejados no novo aparelho é necessário fazer o download por meio do serviço Jogos de Sempre.</p>
            <p>
                <strong>Se eu formatar meu celular / retornar para configuração de fábrica, mantenho os jogos adquiridos?</strong><br/>
                Não. Caso um jogo seja apagado por qualquer motivo, você precisa fazer o download novamente.</p>
        </div>
        <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
        </div>

    </div>
    </div>

    <!-- Termos Modal -->
    <div id="termosModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Termos de Uso</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         <p>Os usuários deverão ler atentamente estes Termos e Condições de
            Uso, antes de efetuarem a contratação de nossos serviços.</p><br/>
        <p>
            CONDIÇÕES DE USO - Jogos de Sempre</p><br/>
        <p>
            As Condições de Uso, aqui consignadas, regerão o relacionamento
            entre o Jogos de Sempre e os seus Clientes, no que se refere a
            utilização do serviço. O serviço contempla o uso de website, no qual
            os clientes poderão realizar o download dos conteúdos que tiverem
            interesse.</p> <br/>
        <p>
            1. DESCRIÇÃO DO SERVIÇO</p><br/>
        <p>
            O serviço Jogos de Sempre é um serviço voltado ao entretenimento e
            tem como objetivo fornecer aos seus clientes conteúdos de Jogos.</P><br/>
        <p> 
            Este serviço poderá ser utilizado por pessoas de qualquer idade.</P><br/>
        <p>
            2. CONTEÚDOS</P><br/>
        <p>
            Jogos de Sempre é o clube dos jogos clássicos de cartas e tabuleiro,
            com apoio de Brinquedos Estrela.</P><br/>
        <p>
            Jogos de tabuleiro Estrela disponíveis: Jogo da Vida, QI, Banco
            imobiliário, Cilada e Genius</P><br/>
        <p>
            Jogos clássicos: War Strategy, Memo Quest, Batalha Naval, Carta
            Futebol Clube, 21, ChinChon, Rouba Monte, Mau Mau e Escopa.</P><br/>
        <p>
            War Strategy e Carta Futebol Clube são jogos multiplayer e permitem
            jogar online.</P><br/>
        <p>
            Todas os jogos serão disponibilizados em suas versões completas e
            sem anúncios. Novos jogos serão adicionados periodicamente.</P><br/>
        <p>
            3. ADESÃO E ACESSO AOS SERVIÇOS</P><br/>
        <p>
            Os Clientes, titulares das linhas telefônicas pré, controle ou pós-
            pagas, para as quais tenham solicitado os serviços, reconhecem
            desde já disporem dos recursos financeiros necessários para a
            aquisição dos conteúdos e se responsabilizam por suas escolhas.</P><br/>
        <p>
            A responsabilidade por todo e qualquer acesso indevido será
            exclusiva do Cliente.</P><br/>
        <p>
            Na contratação, os Clientes autorizam o envio gratuito de informações
            e ofertas relacionadas aos Serviços.</P><br/>
        <p>
            4. VALORES</P><br/>
        <p>
            O valor da assinatura é de R$5,19 por semana.</P><br/>
        <p>
            O usuário poderá realizar o download de 01 (um) jogo a cada semana
            tarifada, ou seja, a cada renovação de assinatura o cliente pode fazer
            o download de mais 01(um) jogo do portfólio.</P><br/>
        <p>
            Tal valor será debitado automaticamente dos saldos de créditos dos
            clientes com celulares pré-pagos, ou cobrados na conta telefônica
            quando forem Clientes com celulares controle ou pós-pagos. <span style="color:Red">Os
            clientes pré-pagos que eventualmente não tenham saldo no valor total
            de R$5,19 disponíveis em sua conta, poderão ser debitados de
            valores inferiores, tendo o serviço renovado.</span></P><br/>
        <p>
            O valor referente a assinatura será efetivamente devido e debitado, na
            forma acima descrita.</P><br/>
        <p>
            5. COMO CONTRATAR</P><br/>
        <p>
            Para contratar o serviço Jogos de Sempre é necessário efetuar uma
            assinatura via SMS enviando a palavra <span style="color:Red">JS</span> para ativação do serviço. O
            cliente também pode receber uma oferta que indicará a palavra que
            deve ser enviada para o serviço ser ativado. Além disso, através do
            Website será possível realizar a assinatura direto na página &quot;<span style="color:Red">Minha
            Assinatura</span>&quot; ou no botão &quot;<span style="color:Red">Quero experimentar</span>&quot; na página inicial
            (home). No momento da contratação o usuário será tarifado no valor
            R$ 5,19 por semana.</P><br/>
        <p>
            6. COMO CANCELAR</P><br/>
        <p>
            O usuário poderá desativar o serviço enviando a palavra <span style="color:Red">SAIR</span> via
            SMS para o número 77085, no momento em que desejar.</P><br/>
        <p>
            O cancelamento pode ser feito também pelo website
            tim.jogosdesempre.com.br, acessando <span style="color:Red">Cancelar assinatura.</span></P><br/>
        <p>
            O cancelamento será realizado instantaneamente.</P><br/>
        <p>
            7. DIREITOS AUTORAIS</P><br/>
        <p>
            Todos os conteúdos oferecidos no serviço Jogos de Sempre são
            protegidos por direitos autorais e não podem ser utilizados para fins
            comerciais.</P><br/>
        <p>
            Os Conteúdos são protegidos conforme a lei vigente e não podem ser
            utilizados sem autorização prévia.</P><br/>
        <p>
            O uso ilegal desses conteúdos resultará em infração a direitos
            autorais, de marca registrada e/ou outros direitos de propriedade
            intelectual, previstos na Lei nº 9.279/93, no Código Civil e na
            Constituição da República Federativa do Brasil.</P><br/>
        <p>
            8. POLÍTICA DE PRIVACIDADE</P><br/>
        <p>
            Os Clientes são plenamente responsáveis por todas as informações
            prestadas durante o preenchimento do seu cadastro. O serviço Jogos
            de Sempre se disponibiliza a corrigir prontamente qualquer alteração
            nos dados pessoais dos Clientes, desde que os mesmos solicitem
            esta alteração.</P><br/>
        <p>
            O serviço Jogos de Sempre buscará a proteção do direito de
            privacidade dos clientes.</P><br/>
        <p>
            Contratando os Serviços, os clientes reconhecem e aceitam a coleta,
            utilização e divulgação de seus dados pessoais exclusivamente para
            os fins previstos nestas Condições de Uso, que seja:</P><br/>
        <p>
            Resposta a eventuais exigências de autoridades públicas, desde que
            devidamente fundamentadas.</P><br/>
        <p>
            Os usuários autorizam, ainda, que seus Dados Pessoais sejam,
            eventualmente, utilizados para:</P><br/>
        <p>
            I. Enviar mensagens gratuitas sobre informações e ofertas
            relacionadas ao serviço, para o número cadastrado no site;</P><br/>
        <p>
            II. Gestão, administração, prestação, ampliação e aprimoramento dos
            Serviços;</P><br/>
        <p>
            III. Envio, por meios tradicionais e/ou eletrônicos, de atualizações dos
            Conteúdos, de informações técnicas, operacionais e comerciais
            relativas aos Serviços, já existentes ou que venham a ser
            desenvolvidos; e</P><br/>
        <p>
            IV. Envio de formulários de pesquisas, aos quais os usuários não
            ficam obrigados a responder.</P><br/>
        <p>
            Salvo os campos destinados a Dados Pessoais, considerados
            obrigatórios, como o nome completo do usuário, número e modelo do
            telefone celular, a inserção de informações sobre outros Dados
            Pessoais como, por exemplo, endereço, sexo, idade, entre outros, é
            facultativa aos clientes.</P><br/>
        <p>
            Além dos Dados Pessoais fornecidos pelos clientes, o serviço Jogos
            de Sempre poderá coletar automaticamente determinadas
            informações técnicas, as quais incluem, sem se limitar, o sistema
            operacional do computador ou smartphone e o tipo de browser
            utilizado pelos assinantes.</P><br/>
        <p>
            9. SEGURANÇA</P><br/>
        <p>
            O serviço Jogos de Sempre possui níveis de segurança legalmente
            requeridos para a proteção de dados dos clientes e, sempre que
            exigido, aprimorará tais sistemas de modo a evitar a perda, mau uso,
            alteração, acesso não autorizado ou subtração indevida dos dados
            pessoais recolhidos. Entretanto, os clientes declaram-se cientes de
            que as medidas de segurança relativas à internet não são totalmente
            infalíveis.</P><br/>
        <p>
            10. DISPOSIÇÕES GERAIS</P><br/>
        <p>
            Os Clientes serão responsáveis pelo acesso ou uso indevido dos
            Serviços.</P><br/>
        <p>
            O serviço Jogos de Sempre publicará em seu site e nas demais
            formas utilizadas para dar publicidade a estas Condições de Uso as
            eventuais mudanças introduzidas.</P><br/>
        <p>
            Estas Condições de Uso estão reguladas e serão interpretadas
            segundo as leis da República Federativa do Brasil.</P><br/>
        <p>
            Reservamo-nos o direito de modificar, a qualquer momento, visando
            uma melhoria contínua, de forma unilateral, o presente termo,
            condições de uso e valores, bem como suspender ou cancelar o
            serviço mediante comunicação prévia.</P><br/>
        <p>
            Ao navegar por este portal, você aceita guiar-se pelos termos e
            condições que se encontram vigentes na data e, portanto, deve
            verifica-los previamente cada vez que visitá-lo.</p> 
        </div>
        <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
        </div>

    </div>
    </div>

    <!-- Login Modal -->
    <div id="myDownloadsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Meus Downloads</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <ul style="list-style-type: none;margin: 0; padding: 0;">
                <?php foreach($my_games as $game) { ?>
                    <li style="margin: 5px;">
                        <img class="my-downloads-img" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/portfolio/<?=$game['icon']?>" alt="game icon">
                        <span style="margin-left: 10px;"><?=$game['title'] ?></span>
                        <button style="vertical-align: middle; float:right;" class="btn btn-success btnDonload btn-sm" id="<?=$game['id_game'];?>" type="button" style="margin-bottom: 10px; float:right"><i class="fa fa-download"></i></button>
                    </li>
                <?php } ?>
            </ul>
        <!-- <p>Some text in the modal.</p> -->
        </div>
        <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
        </div>

    </div>
    </div>

    <!-- iframe to make download .apk -->
    <iframe id="secretIFrame" src="" style="display:none; visibility:hidden;"></iframe>

    <!-- Set variaveis  -->
    <script type="text/javascript">

        var url_download = "<?php echo site_url(array('index.php', 'download')); ?>";
        var url_check_download = "<?php echo site_url(array('index.php', 'download', 'checkDownload')); ?>";
        var url_cancel = "<?php echo site_url(array('index.php', 'subscription_api', 'sendUnsubscribe')); ?>";
        var landing_page = "<?php echo $url_lp  ?>";
        // console.log(url_download);

        var user;
        <?php if (isset($user)) : ?>
            user = <?php echo json_encode($user); ?>;
        <?php endif; ?>
	</script>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/freelancer.js"></script>

    <!-- Acotel core JavaScript  -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.main.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>js/jquery.form.min.js"></script>

</body>

</html>