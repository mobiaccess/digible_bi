<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <!-- , initial-scale=1, shrink-to-fit=yes"> -->
    <meta name="description" content="">
    <meta name="keywords" content="" >
    <meta name="author" content="">

    <title>Jogos de Sempre</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/freelancer.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>

        .navbar-toggler {
            z-index: 1;
        }
        
        @media (max-width: 576px) {
            nav > .container {
                width: 100%;
            }
        }

        @media screen and (max-width: 480px) {
            h2 {
                font-size: 1.75em !important;
            }
        }

        .btn-outline {
            border: solid 2px #2C3E50;
            background: #2C3E50;
        }

        .btn-outline:hover{
            border: solid 2px #fff;
            background: #fff;
        }

        a:hover {
            color: #014c8c !important;
        }

        p {
            font-size: 105%;
        }
    </style>

</head>
<!-- background: #18bc9c; <?php echo base_url() . $this->template->get_theme_path() ?> -->
<!-- <body class="index" id="page-top"> -->
<body>
    <div class="container" style="
                width: 395px;
                height: 698px;
                padding: 15px 0 !important;
                background-image: url('<?php echo base_url() . $this->template->get_theme_path() ?>img/lp/bg.png');
                background-repeat: no-repeat;
                background-size:100% 100%;
                background-position: center top;">

        <div class="col-xs-12 text-center">
            <img class="img-fluid" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/lp/teste_itens.png" alt="">
        </div>

        <div class="col-xs-12 text-center" style="margin-top: 12px;">
            <a href="<?=$url_lp;?>" class="">
                <img class="rounded" style="max-width: 395px; height: auto; width: 100%; object-fit:cover;" src ="<?php echo base_url() . $this->template->get_theme_path() ?>img/lp/red-button.png"/>
            </a>
        </div>

        <div class="text-center"style="padding: 0 5px 0 5px">
            <div>
                <p style="height: auto;width:auto; text-align: center; object-fit:cover; font-size: small;margin-top: 15px !important;">
                    <small>
                        <strong>Jogos de Sempre</strong> é um clube de games com a versão digital de jogos clássicos e famosos de marcas consagradas como a Estrela, incluindo Banco Imobiliário, Jogo da Vida, Genius, QI, Lig 4, Batalha Naval, War Strategy e muitos outros.
                        <br>Faça parte do clube, sempre com novidades e lançamentos. Assinatura semanal por R$5,19 e a cada semana você pode baixar um jogo novo, que é seu pra sempre.
                    </small>
                </p>
            </div>    
        </div>            
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>