<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <meta name="description" content="">
    <meta name="keywords" content="" >
    <meta name="author" content="">

    <title>Jogos de Sempre</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/freelancer.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
        html{
            max-width:100%;
            height: auto;
        }
        body{
            background: white url('<?php echo base_url() . $this->template->get_theme_path() ?>img/lp/bg.png') no-repeat center top;
            /* background-size: cover; */
        }
        .navbar-toggler {
            z-index: 1;
        }
        
        @media (max-width: 576px) {
            nav > .container {
                width: 100%;
            }
        }

        @media screen and (max-width: 480px) {
            h2 {
                font-size: 1.75em !important;
            }
        }

        .btn-outline {
            border: solid 2px #2C3E50;
            background: #2C3E50;
        }

        .btn-outline:hover{
            border: solid 2px #fff;
            background: #fff;
        }

        a:hover {
            color: #014c8c !important;
        }

        p {
            font-size: 105%;
            font-weight: bold;
        }
    </style>

</head>
<!-- background: #18bc9c; <?php echo base_url() . $this->template->get_theme_path() ?> -->
<!-- <body class="index" id="page-top"> -->
<body>
    <div class="container" style="
                max-width: 395px;
                max-height: 698px;
                ">

        <div class="col-xs-12 text-center">
            <img class="img-fluid" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/lp/teste_itens.png" alt="">
        </div>

        <div class="row text-center" style="alignmargin-top: 15px !important; height:20% !important;">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 text-center" style="">
                <?php echo $template['body']; ?>
            </div>
            <div class="col-lg-3"></div>
        </div>

        <div class="text-center"style="padding: 0 5px 0 5px">
            <div>
                <p style="height: auto;width:auto; text-align: center; object-fit:cover; font-size: small;margin-top: 15px !important;">
                    <small>
                        <br>Faça parte do clube, sempre com novidades e lançamentos. Assinatura semanal por R$5,19 e a cada semana você pode baixar um jogo novo, que é seu pra sempre.
                    </small>
                </p>
            </div>    
        </div>            
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/tether/tether.min.js"></script>
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>