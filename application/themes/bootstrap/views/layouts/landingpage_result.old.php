<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="" >
    <meta name="author" content="">

    <title>Jogos de Sempre</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/freelancer.min.css" rel="stylesheet">
    <link href="<?php echo base_url() . $this->template->get_theme_path() ?>css/landing_page_result.css" rel="stylesheet">

    <!-- Temporary navbar container fix -->
    <style>
    body{
        background: white url("<?php echo base_url() . $this->template->get_theme_path() ?>img/lp/bg.png") no-repeat center top;
    }
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }

    @media screen and (max-width: 480px) {
        h2 {
            font-size: 1.75em !important;
        }
    }
    </style>

</head>

<body class="index" id="page-top" style="height:100vh !important;">
    <section class="success" style="padding: 15px 0 !important; height:100%">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <a href="<?php echo base_url() ?>"><img class="img-fluid" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/portfolio/jds-bk.png" alt=""></a>
                </div>
                <div class="col-lg-3"></div>
            </div>

            <h2 class="text-center" style="padding: 5px 0 !important;">Jogos de Sempre</h2>
            <hr class="star-light">

            <div class="row" style="margin-top: 15px !important; height:100% !important;">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 text-center">
                    <?php echo $template['body']; ?>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </section>
</body>

</html>