 $().ready(function(){

    $('#form-login-exp').ajaxForm({
        success: function(responseText, statusText, xhr, $form){
            var resp = jQuery.parseJSON(responseText);
            if (resp.success) {
                console.log(resp.msg);
                location.reload();
            } else {
                console.log(resp.msg);
                $.alert(resp.msg);
            }
        }
    });

    $('#form-login').ajaxForm({
        success: function(responseText, statusText, xhr, $form){
            $('#loginModal').modal('hide');
            var resp = jQuery.parseJSON(responseText);
            if (resp.success) {
                console.log(resp.msg);
                location.reload();
            } else {
                $.alert(resp.msg);
            }
        }
    });

    $('#form-unsubscribe').ajaxForm({
        success: function(responseText, statusText, xhr, $form){
            // $('#loginModal').modal('hide');
            var resp = jQuery.parseJSON(responseText);
            if (resp.success) {
                console.log(resp.msg);
                location.reload();
            } else {
                $.alert(resp.msg);
            }
        }
    });

    $('#form-send-email').ajaxForm({
        success: function(responseText, statusText, xhr, $form){
            var resp = jQuery.parseJSON(responseText);
            console.log(resp.msg);
            $.alert({
                title: "Contato",
                content: resp.msg,
                buttons: {
                    ok: function() {
                        $('#form-send-email')[0].reset();
                    }
                }
            });
        }
    });

    $('.btnDonload').on('click', function() {

        var ua = navigator.userAgent.toLocaleLowerCase();
        var isAndroid = ua.indexOf("android") > -1;
        if (!isAndroid) {
            alert("Download exclusivo para Android.");
            return;
        }

        var id_game = $(this).attr('id');
        // alert('id: ' + $(this).attr('id'));

        if (user == null) {
            $('#loginModal').modal();
        } else {
            console.log('Download vai começar em alguns instantes');
            $.get(url_check_download + '?id_game=' + id_game, function(data, status){
                // alert("Data: " + data + "\nStatus: " + status);
                var resp = jQuery.parseJSON(data);
                if (resp.success) {
                    // window.location.replace(url_download + '?id_game=' + id_game);
                    $("#secretIFrame").attr("src",url_download + '?id_game=' + id_game);
                    // myGamesReload = true;
                    setTimeout(function(){
                        location.reload();
                    }, 15000);
                } else {
                    if (resp.subscribe) {
                        $.alert({
                            title: "Atenção",
                            content: resp.msg,
                            buttons: {
                                fechar: function() {
                                    location.reload();
                                },
                                assinar: {
                                    text: "Quero Assinar",
                                    action: function() {
                                        window.location.replace(landing_page);
                                    }
                                }
                            }
                        });
                    } else {
                        $.alert(resp.msg);
                    }
                }
            });
        }
    });
    
    $('.linkLogin').on('click', function() {
        $('#loginModal').modal();
    });

    $('#faqLink').on('click', function() {
        $('#faqModal').modal();
    });

    $('#termosLink').on('click', function() {
        $('#termosModal').modal();
    });

    $('#myDownloadsLink').on('click', function() {
        // if(myGamesReload){
        //     location.reload();
        //     myGamesReload = false;
        // }
        $('#myDownloadsModal').modal();
    });

    $('#aForgotPassword').on('click', function() {
        $('#loginModal').modal('hide');
        $('#forgotPasswordModal').modal();
    });

    $('#form-forgot-password').ajaxForm({
        success: function(responseText, statusText, xhr, $form){
            var resp = jQuery.parseJSON(responseText);
            // if (resp.success) {
            //     console.log(resp.msg);
            // }
            $.alert(resp.msg);
            $('#forgotPasswordModal').modal('hide');
        }
    });

    $('#cancelLink').click(function(){
        $.confirm({
            title: 'Atenção',
            content: 'Você está prestes a cancelar sua assinatura!',
            buttons: {
                fechar: function() {},
                cancelar: {
                    text: 'Cancelar',
                    action: function() {
                        $.confirm({
                            title: 'Cancelar',
                            content: 'Confirma o cancelamento da sua assinatura?',
                            buttons: {
                                confirmo: function () {
                                    // $.alert('Fazer o ajax de cancelamento!');
                                    $.get(url_cancel, function (data, status){
                                        var resp = jQuery.parseJSON(data);
                                        if (resp.success) {
                                                $.confirm({
                                                    title: 'Aviso',
                                                    content: 'Sua solicitação de cancelamento foi enviada.',
                                                    buttons: {
                                                        fechar: function () {
                                                            location.reload();
                                                        }
                                                    }
                                                });
                                        }
                                    });
                                },
                                cancelar: function () {}
                            }
                        });
                    }
                }
            }
        });
    });

    $('.btnConfirmSubscription').click(function(){
        var ua = navigator.userAgent.toLocaleLowerCase();
        var isAndroid = ua.indexOf("android") > -1;
        if (!isAndroid) {
            $.confirm({
                title: "Atenção",
                content: "Produto exclusivo para usuário com dispositivo Android.",
                buttons: {
                    fechar: function() {},
                    continuar: function() {
                        window.location.replace(landing_page);
                    }
                }
            });
        } else {
            window.location.replace(landing_page);
        }
    });

    $(window).click(function() {
        $('.navbar-toggler-right').collapse('hide');
        $('.navbar-collapse').collapse('hide');
    });

});