<?php

/*
 * Method to load css files into your project.
 *
 * Accepts a string or array as parameter.
 *
 * Usage:
 *
 * echo load_css('screen.css');
 * echo load_css(array('screen.css','otherstyle.css'));
 * echo load_css(array(
 *			 array('screen.css', 'screen, projection'),
 *			 array('print.css', 'print'),
 *		 ));
 *
 * @author William Rufino <williamhrs@gmail.com>
 * @author Lucas Vasconcelos <lucas.vasconcelos@gmail.com>
 * @version 1.6
 * @param mixed $css
 */
if (!function_exists('load_css')) {

	function load_css($data,$modulo_name="") {
	    
		$CI =& get_instance();
            
		$CI->load->helper('html');
		$CI->config->load('assets');
		
		// verifica se o nome do módulo foi passado por parâmetro
		// se não o nome do modulo corrente será colocado
		$modulo_name = ( ! empty($modulo_name) ) ? $modulo_name : $CI->router->fetch_module();
		
		$csspath = key($CI->config->item('modules_locations')).$modulo_name.'/public/css/';

		if ( ! is_array($data) ){
			return link_tag($csspath . $data, 'stylesheet', 'text/css');
		}else{
			$return = '';
			foreach ($data as $item) {
				if (!is_array($item)) {
					$return .= link_tag($csspath . $item, 'stylesheet', 'text/css') . "\n";
				} else {
					$return .= link_tag($csspath . $item[0], 'stylesheet', 'text/css', '', $item[1]) . "\n";
				}
			}
		}
		return $return;
		
	}

}

if (!function_exists('load_public_css')) {

	function load_theme_css($css) {
	    
		$CI =& get_instance();
            
		$CI->load->helper('html');
		
		$csspath = base_url() . $CI->template->get_theme_path() . 'css/';
		
		if ( ! is_array($css) ) {
			$css = (array)$css;
		}	
		
		$return = '';

		foreach ($css as $style) {
			$return .= link_tag($csspath . $style, 'stylesheet', 'text/css') . "\n";
		}
		
		return $return;
		
	}

}

/*
 * Method to load javascript files into your project.
 * @author William Rufino
 * @version 1.4
 * @param array $js
 */
if (!function_exists('load_js')) {

	function load_js($js,$modulo_name="") {
		
		$CI =& get_instance();
		
		$CI->load->helper('url');
		
		if (! is_array($js)) {
			$js = (array)$js;
		}
		
		$CI->config->load('assets');
		// verifica se o nome do módulo foi passado por parâmetro
		// se não o nome do modulo corrente será colocado
		$modulo_name = ( $modulo_name != "" ) ? $modulo_name : $CI->router->fetch_module();
		
		$jspath = base_url().key($CI->config->item('modules_locations')).$modulo_name.'/public/js/';
		
		$return = '';
		foreach ($js as $j) {
			$return .= '<script type="text/javascript" src="' . $jspath . $j . '"></script>' . "\n";
		}
		return $return;
            
	}

}

if (!function_exists('load_public_js')) {

	function load_theme_js($js) {
		
		$CI =& get_instance();
		
		$CI->load->helper('url');

		if (!is_array($js)) {
			$js = (array) $js;
		}

		$jspath =  base_url() . $CI->template->get_theme_path() . 'js/';

		$return = '';
		foreach ($js as $j) {
			$return .= '<script type="text/javascript" src="' . $jspath . $j . '"></script>' . "\n";
		}
		
		return $return;
            
	}

}

/*
 * Method to insert images into your project.
 * @author William Rufino
 * @version 1.2
 * @param string $image - path to image
 */
if (!function_exists('load_img')) {

	function load_img($img,$admin=false){
		
		$CI =& get_instance();
		
		$CI->load->helper('url');
		$CI->config->load('assets');
		
		if ( $admin ){
			if(!$CI->config->item('image_path_admin')){
				$CI->config->set_item('image_path','public/image/admin');
			}
			$imagepath =  base_url() . $CI->config->item('image_path_admin');
		}else{
			if(!$CI->config->item('image_path')){
				$CI->config->set_item('image_path','public/image/');
			}
			$imagepath =  base_url() . $CI->config->item('image_path');
		}
		
		return base_url(). $imagepath . $img;
		
	}

}

