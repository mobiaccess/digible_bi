<?php
function formatTimestamp($date,$time){
	$exp_date = explode("/", $date);
	return $exp_date[2] ."/". $exp_date[1] ."/". $exp_date[0] ." ". $time .":00";
}

function formatDate($date){
	if(strlen($date)>0){
		$exp_date = explode("/", $date);
		return $exp_date[2] ."-". $exp_date[1] ."-". $exp_date[0];
	} else {
		return "";
	}
}

function formatDateToBr($date){
	if(strlen($date)>0){
		$exp_date = explode("-", $date);
		return $exp_date[2] ."/". $exp_date[1] ."/". $exp_date[0];
	} else {
		return "";
	}
}

function arrayToString($array){
	$str = "";
	if(is_array($array)){
		foreach($array as $key){
			if(is_array($key)){
				foreach($key as $subKey){
					if(strlen($subKey) > 0){
						$str .= $subKey . ",";
					}
				}
			} else {
				if(strlen($key) > 0){
					$str .= $key . ",";
				}
			}
		}
		if(strlen($str)>0){
			return substr($str, 0, -1);
		} else {
			return $str;
		}
	} else {
		return "";
	}
}

function arrayToStringWithPlicks($array){
	$str = "";
	if(is_array($array)){
		foreach($array as $key){
			if(is_array($key)){
				foreach($key as $subKey){
					if(strlen($subKey) > 0){
						$str .= "'" . $subKey . "',";
					}
				}
			} else {
				if(strlen($key) > 0){
					$str .= "'" . $key . "',";
				}
			}
		}
		if(strlen($str)>0){
			return substr($str, 0, -1);
		} else {
			return $str;
		}
	} else {
		return "";
	}
}

# This function removes duplicated values inside a multi-dimensional array
# Here it is used to remove duplicated msisdns that might come in the base file.
function array_flat( $a, $s = array( ), $l = 0 ){

	if( !is_array( $a ) ) return $s;
	foreach( $a as $k => $v ){
		if( !is_array( $v ) ){
			$s[ ] = $v;
			continue;
		}
		$l++;
		$s = array_flat( $v, $s, $l );
		$l--;
	}
	if( $l == 0 ) $s = array_values( array_unique( $s ) );
	return $s;

}

function simpleText($Text){
	/**
	 * Caracteres especiais permitidos
	 *
	 * .,-?!'@:;/()<>
	 */

	$result = $Text;

	$especiais_bloqueados = '+"#$%¨&*_¹²³£¢¬§{}°ºª´~^[]|\\`“”';

	// Troco caracteres especiais que nao sao permitidos por algum que se aproxime ao seu real significado
	$result = str_replace('"', "'", $result);
	$result = str_replace('“', "'", $result);
	$result = str_replace('”', "'", $result);
	$result = str_replace('#', '', $result);
	$result = str_replace('$', 'S', $result);
	$result = str_replace('%', '', $result);
	$result = str_replace('¨', '', $result);
	$result = str_replace('&', 'E', $result);
	$result = str_replace('*', '-', $result);
	//$result = str_replace('_', '-', $result);
	$result = str_replace('¹', '1', $result);
	$result = str_replace('²', '2', $result);
	$result = str_replace('³', '3', $result);
	$result = str_replace('£', 'L', $result);
	$result = str_replace('¢', 'c', $result);
	$result = str_replace('¬', '', $result);
	$result = str_replace('§', '', $result);
	$result = str_replace('{', '(', $result);
	$result = str_replace('}', ')', $result);
	$result = str_replace('°', '', $result);
	$result = str_replace('º', 'o', $result);
	$result = str_replace('ª', 'a', $result);
	$result = str_replace('´', "'", $result);
	$result = str_replace('~', '', $result);
	$result = str_replace('^', '', $result);
	$result = str_replace('[', '(', $result);
	$result = str_replace(']', ')', $result);
	$result = str_replace('|', '/', $result);
	$result = str_replace('\\', '/', $result);
	$result = str_replace('`', "'", $result);

	// Troco cedilha minusculo/maiusculo por c minusculo/maiusculo
	$result = str_replace('ç', "c", $result);
	$result = str_replace('Ç', "C", $result);

	// Troco a com acento por a sem acento
	$result = str_replace('á', "a", $result);
	$result = str_replace('à', "a", $result);
	$result = str_replace('ã', "a", $result);
	$result = str_replace('â', "a", $result);
	$result = str_replace('ä', "a", $result);

	// Troco A com acento por A sem acento
	$result = str_replace('Á', "A", $result);
	$result = str_replace('À', "A", $result);
	$result = str_replace('Ã', "A", $result);
	$result = str_replace('Â', "A", $result);
	$result = str_replace('Ä', "A", $result);

	// Troco e com acento por e sem acento
	$result = str_replace('é', "e", $result);
	$result = str_replace('è', "e", $result);
	$result = str_replace('ê', "e", $result);
	$result = str_replace('ë', "e", $result);

	// Troco E com acento por E sem acento
	$result = str_replace('É', "E", $result);
	$result = str_replace('È', "E", $result);
	$result = str_replace('Ê', "E", $result);
	$result = str_replace('Ë', "E", $result);

	// Troco i com acento por i sem acento
	$result = str_replace('í', "i", $result);
	$result = str_replace('ì', "i", $result);
	$result = str_replace('î', "i", $result);
	$result = str_replace('ï', "i", $result);

	// Troco I com acento por I sem acento
	$result = str_replace('Í', "I", $result);
	$result = str_replace('Ì', "I", $result);
	$result = str_replace('Î', "I", $result);
	$result = str_replace('Ï', "I", $result);

	// Troco o com acento por o sem acento
	$result = str_replace('ó', "o", $result);
	$result = str_replace('ò', "o", $result);
	$result = str_replace('õ', "o", $result);
	$result = str_replace('ô', "o", $result);
	$result = str_replace('ö', "o", $result);

	// Troco O com acento por O sem acento
	$result = str_replace('Ó', "O", $result);
	$result = str_replace('Ò', "O", $result);
	$result = str_replace('Õ', "O", $result);
	$result = str_replace('Ô', "O", $result);
	$result = str_replace('Ö', "O", $result);

	// Troco u com acento por u sem acento
	$result = str_replace('ú', "u", $result);
	$result = str_replace('ù', "u", $result);
	$result = str_replace('û', "u", $result);
	$result = str_replace('ü', "u", $result);

	// Troco U com acento por U sem acento
	$result = str_replace('Ú', "U", $result);
	$result = str_replace('Ù', "U", $result);
	$result = str_replace('Û', "U", $result);
	$result = str_replace('Ü', "U", $result);

	return $result;
}

function simpleOnlyLettersAndUnderline($Text){
	$result = $Text;

	$result = str_replace(' ', '', $result);
	$result = str_replace('"', '', $result);
	$result = str_replace('“', '', $result);
	$result = str_replace('”', '', $result);
	$result = str_replace('#', '', $result);
	$result = str_replace('$', 'S', $result);
	$result = str_replace('%', '', $result);
	$result = str_replace('¨', '', $result);
	$result = str_replace('&', 'E', $result);
	$result = str_replace('*', '', $result);
	//$result = str_replace('_', '', $result);
	$result = str_replace('¹', '', $result);
	$result = str_replace('²', '', $result);
	$result = str_replace('³', '', $result);
	$result = str_replace('£', 'L', $result);
	$result = str_replace('¢', 'c', $result);
	$result = str_replace('¬', '', $result);
	$result = str_replace('§', '', $result);
	$result = str_replace('{', '', $result);
	$result = str_replace('}', '', $result);
	$result = str_replace('°', '', $result);
	$result = str_replace('º', 'o', $result);
	$result = str_replace('ª', 'a', $result);
	$result = str_replace('´', '', $result);
	$result = str_replace('~', '', $result);
	$result = str_replace('^', '', $result);
	$result = str_replace('[', '', $result);
	$result = str_replace(']', '', $result);
	$result = str_replace('|', '', $result);
	$result = str_replace('\\', '', $result);
	$result = str_replace('`', '', $result);
	$result = str_replace('(', '', $result);
	$result = str_replace(')', '', $result);
	$result = str_replace('-', '', $result);
	$result = str_replace('=', '', $result);
	$result = str_replace('!', '', $result);
	$result = str_replace('.', '', $result);
	$result = str_replace('?', '', $result);
	$result = str_replace(';', '', $result);
	$result = str_replace(':', '', $result);
	$result = str_replace(',', '', $result);
	$result = str_replace('@', '', $result);

// 	$result = str_replace('0', '', $result);
// 	$result = str_replace('1', '', $result);
// 	$result = str_replace('2', '', $result);
// 	$result = str_replace('3', '', $result);
// 	$result = str_replace('4', '', $result);
// 	$result = str_replace('5', '', $result);
// 	$result = str_replace('6', '', $result);
// 	$result = str_replace('7', '', $result);
// 	$result = str_replace('8', '', $result);
// 	$result = str_replace('9', '', $result);

	// Troco cedilha minusculo/maiusculo por c minusculo/maiusculo
	$result = str_replace('ç', "c", $result);
	$result = str_replace('Ç', "C", $result);

	// Troco a com acento por a sem acento
	$result = str_replace('á', "a", $result);
	$result = str_replace('à', "a", $result);
	$result = str_replace('ã', "a", $result);
	$result = str_replace('â', "a", $result);
	$result = str_replace('ä', "a", $result);

	// Troco A com acento por A sem acento
	$result = str_replace('Á', "A", $result);
	$result = str_replace('À', "A", $result);
	$result = str_replace('Ã', "A", $result);
	$result = str_replace('Â', "A", $result);
	$result = str_replace('Ä', "A", $result);

	// Troco e com acento por e sem acento
	$result = str_replace('é', "e", $result);
	$result = str_replace('è', "e", $result);
	$result = str_replace('ê', "e", $result);
	$result = str_replace('ë', "e", $result);

	// Troco E com acento por E sem acento
	$result = str_replace('É', "E", $result);
	$result = str_replace('È', "E", $result);
	$result = str_replace('Ê', "E", $result);
	$result = str_replace('Ë', "E", $result);

	// Troco i com acento por i sem acento
	$result = str_replace('í', "i", $result);
	$result = str_replace('ì', "i", $result);
	$result = str_replace('î', "i", $result);
	$result = str_replace('ï', "i", $result);

	// Troco I com acento por I sem acento
	$result = str_replace('Í', "I", $result);
	$result = str_replace('Ì', "I", $result);
	$result = str_replace('Î', "I", $result);
	$result = str_replace('Ï', "I", $result);

	// Troco o com acento por o sem acento
	$result = str_replace('ó', "o", $result);
	$result = str_replace('ò', "o", $result);
	$result = str_replace('õ', "o", $result);
	$result = str_replace('ô', "o", $result);
	$result = str_replace('ö', "o", $result);

	// Troco O com acento por O sem acento
	$result = str_replace('Ó', "O", $result);
	$result = str_replace('Ò', "O", $result);
	$result = str_replace('Õ', "O", $result);
	$result = str_replace('Ô', "O", $result);
	$result = str_replace('Ö', "O", $result);

	// Troco u com acento por u sem acento
	$result = str_replace('ú', "u", $result);
	$result = str_replace('ù', "u", $result);
	$result = str_replace('û', "u", $result);
	$result = str_replace('ü', "u", $result);

	// Troco U com acento por U sem acento
	$result = str_replace('Ú', "U", $result);
	$result = str_replace('Ù', "U", $result);
	$result = str_replace('Û', "U", $result);
	$result = str_replace('Ü', "U", $result);

	return $result;
}

function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
	/*
	 $interval can be:
	yyyy - Number of full years
	q - Number of full quarters
	m - Number of full months
	y - Difference between day numbers
	(eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
	d - Number of full days
	w - Number of full weekdays
	ww - Number of full weeks
	h - Number of full hours
	n - Number of full minutes
	s - Number of full seconds (default)
	*/

	if (!$using_timestamps) {
		$datefrom = strtotime($datefrom, 0);
		$dateto = strtotime($dateto, 0);
	}
	$difference = $dateto - $datefrom; // Difference in seconds

	switch($interval) {

		case 'yyyy': // Number of full years
			$years_difference = floor($difference / 31536000);
			if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
				$years_difference--;
			}
			if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
				$years_difference++;
			}
			$datediff = $years_difference;
			break;
		case "q": // Number of full quarters
			$quarters_difference = floor($difference / 8035200);
			while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
				$months_difference++;
			}
			$quarters_difference--;
			$datediff = $quarters_difference;
			break;
		case "m": // Number of full months
			$months_difference = floor($difference / 2678400);
			while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
				$months_difference++;
			}
			$months_difference--;
			$datediff = $months_difference;
			break;
		case 'y': // Difference between day numbers
			$datediff = date("z", $dateto) - date("z", $datefrom);
			break;
		case "d": // Number of full days
			$datediff = floor($difference / 86400);
			break;
		case "w": // Number of full weekdays
			$days_difference = floor($difference / 86400);
			$weeks_difference = floor($days_difference / 7); // Complete weeks
			$first_day = date("w", $datefrom);
			$days_remainder = floor($days_difference % 7);
			$odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
			if ($odd_days > 7) { // Sunday
				$days_remainder--;
			}
			if ($odd_days > 6) { // Saturday
				$days_remainder--;
			}
			$datediff = ($weeks_difference * 5) + $days_remainder;
			break;
		case "ww": // Number of full weeks
			$datediff = floor($difference / 604800);
			break;
		case "h": // Number of full hours
			$datediff = floor($difference / 3600);
			break;
		case "n": // Number of full minutes
			$datediff = floor($difference / 60);
			break;
		default: // Number of full seconds (default)
			$datediff = $difference;
			break;
	}
	return $datediff;
}

?>