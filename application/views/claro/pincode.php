<div style="">
    <img src="<?php echo base_url() . $this->template->get_theme_path() ?>/img/treine-com-o-fera.png" alt="Backgroud" style="margin-top: 20px; width:90%; display: block; margin-left: auto; margin-right: auto;">
    <img src="<?php echo base_url() . $this->template->get_theme_path() ?>/img/label-center.png" alt="Backgroud" style="margin-top: 120px; width:90%; display: block; margin-left: auto; margin-right: auto;">
    <img src="<?php echo base_url() . $this->template->get_theme_path() ?>/img/logo-neymar-png.png" alt="Backgroud" style="margin-top: 30px; width:25%; display: block; margin-left: auto; margin-right: auto;">
    <p style="color:#fff; margin-top:20px; margin-left: auto; margin-right: auto; width: 18em; text-align: center">
        Cliente: (<?php echo substr(@$msisdn, 0, 2);?>) (<?php echo substr(@$msisdn, 2, 9);?>) <br/>
        Digite o código recebido por SMS em seu celular para confirmar a assinatura do serviço Neymar Jr XP por R$4,99 por semana. Para cancelar, envie SAIR para 2111.
    </p>
    <?php if (@$error) { ?>
        <p style="margin-top:20px; margin-left: auto; margin-right: auto; width: 18em; text-align: justify; text-justify: inter-word; color:red; font-weight: bold;">
            <?php echo $error; ?>
        </p>
    <?php } ?>
    <!--form action="subscribe" style="margin-left: auto; margin-right: auto; width: 18em; text-align: center"-->
    <?php echo form_open('assineclaro/subscribe', 'method="get" style="margin-left: auto; margin-right: auto; width: 18em; text-align: center"'); ?>
        <input type="hidden" name="msisdn" value="<?php echo @$msisdn; ?>" />
        <input type="hidden" name="cid" value="<?php echo @$cid?>">
        <input type="text" name="pincode" placeholder="Código" size="4" maxlength="4" style="width:100%; text-align:center;" required />
        <br />
        <button type="submit" style="margin-top:20px; background-color:#9a1ed4; border-color:#9a1ed4;" class="btn btn-danger">Confirmar Assinatura</button>
    </form>
    <p style="margin-top:20px; margin-left: auto; margin-right: auto; width: 18em; font-size: 14px; text-align: center; text-justify: inter-word;">
        <a style="color:#fff;" href="https://njrxp.com/pt/terms.html">Termos e condições</a>
    </p>
</div>