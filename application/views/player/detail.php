<!-- Inner page Bar -->
	<!-- <div id="page-bar">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-8 col-xs-12">
					<div class="page-title">
						<h1 class="text-uppercase">Video Detail</h1>
					</div>
				</div>
				<div class="col-md-3 col-sm-4 col-xs-12 text-right">
					<ul class="breadcrumb">
					    <li>
					    	<a href="index.html"><i class="fi ion-ios-home"></i>Home</a>
					    </li>
					    <li class="active">Video Detail</li>
					</ul>
				</div>
			</div>
		</div>
	</div> -->
	<!-- Inner page Bar -->

	<!-- Secondary Section -->

	<div id="video-detail">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-8">
					<div class="vid-detail-container">
						<div class="row">
							<div class="col-sm-12">
								<div class="vid-player">
									<div class="embed-responsive embed-responsive-16by9">
			                            <!-- <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/88627743"></iframe> -->
			                            <div id="player"></div>
			                        </div>
								</div>
								<div class="vid-text">
									<!-- <p><span>By</span> <a href="#">Admin</a></p> -->
									<h1><?= $movie->movie->title ?></h1>
								</div>
								<div class="video-info-bar">
									<ul class="list-inline list-unstyled info-ul">
										<!-- <li><i class="fa fa-calendar"></i><?= $movie->movie->published ?></li> -->
										<li><i class="fa fa-eye"></i><?= $movie->movie->views ?></li>
										<!-- <li><i class="fa fa-thumbs-up"></i>457689</li> -->
										<!-- <li class="pull-right sharing-drop"><button class="btn"></button></li> -->
									</ul>
									<ul class="list-unstyled list-inline pull-right text-right sharing-bar">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-instagram"></i></a></li>
										<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
									</ul>
								</div>
								<!-- <div class="video-detail-text">
									<p>
										Donec rutrum varius quam non euismod. Nullam arcu urna, dignissim accumsan bibendum ut, fermentum a quam. Suspendisse fermentum augue vel nulla iaculis id tempus massa consequat. Donec venenatis leo eu felis condimentum in feugiat dui vulputate. Morbi vel quam lorem, sed tempor nibh. Sed elementum nunc tellus. Vivamus in orci vel risus dapibus imperdiet eu et nisl.
									</p>
									<p>
										Aliquam urna sapien, rhoncus eu eleifend ut, consectetur non purus. Proin mollis tortor vitae nisi egestas auctor. Nunc lacinia, diam sed vehicula convallis, lacus sem mattis risus, ac tincidunt quam dolor ut neque. Ut ut euismod sapien. Morbi volutpat, libero vel volutpat tristique, lectus orci auctor leo, eget adipiscing odio massa et mauris. Sed varius viverra urna sed venenatis. Nunc sit amet velit nec urna aliquam sollicitudin. Vestibulum condimentum, quam et aliquet ultrices, odio enim tempus ipsum, in facilisis dui nisi ut enim. Ut quis risus quis ante dictum auctor a sed turpis. Nulla vitae risus ut odio accumsan accumsan sollicitudin sit amet metus. Morbi ut urna at est posuere porta ac eu leo. Vivamus mollis sollicitudin varius. Pellentesque viverra, ligula id tincidunt ultrices, nunc nulla porttitor justo, sed varius risus urna et augue. Etiam et lectus lectus. Quisque vitae erat lorem, a mattis lectus.
									</p>
								</div> -->
								<!-- <div id="comment-frm-container">
									<div class="vid-heading overflow-hidden">
										<span class="wow fadeInUp" data-wow-duration="0.8s">Leave a comment</span>
										<div class="hding-bottm-line wow zoomIn" data-wow-duration="0.8s"></div>
									</div>
									<form class="comment-form">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<input type="text" name="comment-user-name" class="form-control" id="cmnt-user-name" placeholder="NAME" required>
													<div class="input-top-line"></div>
													<div class="input-bottom-line"></div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<input type="email" name="comment-user-email" class="form-control" id="cmnt-user-email" placeholder="EMAIL" required>
													<div class="input-top-line"></div>
													<div class="input-bottom-line"></div>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group">
													<textarea name="comment-user-message" id="cmnt-user-msg" rows="4" class="form-control" placeholder="MESSAGE" required></textarea>
													<div class="input-top-line"></div>
													<div class="input-bottom-line"></div>
												</div>
											</div>
											<div class="col-sm-12">
												<button type="submit" class="btn btn-snd">
													Post Comment
												</button>	
											</div>
											
										</div>
									</form>
								</div> -->
								<?php if ( isset($movie->epsodios)) { ?>
								<div class="related-item">
									<div class="vid-heading overflow-hidden">
										<span class="wow fadeInUp" data-wow-duration="0.8s">
											Episódios
										</span>
										<div class="hding-bottm-line wow zoomIn" data-wow-duration="0.8s">
										</div>
									</div>
									<div class="row">
										<div class="vid-container">
										<?php foreach ($movie->epsodios as $mov) { ?>
										<?php $locate = array('index.php', 'player', 'index', $mov->id_movie); ?>
											<div class="col-md-4 col-sm-6">
												<div class="latest-vid-img-container">		
													<div class="vid-img">
														<img class="img-responsive" src="<?= $mov->thumbnail; ?>" alt="video image">
														<a href="<?php echo site_url($locate)  ?>" class="play-icon play-small-icon">
										                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
										                </a>
														<div class="overlay-div"></div>
													</div>
													<div class="vid-text">
														<!-- <p><span>By</span> <a href="#">Jhon Doe</a></p> -->
														<h1><a href="<?php echo site_url($locate)  ?>" class="limit_ponto_search"><?= $mov->title ?></a></h1>
														<!-- <p class="vid-info-text">
															<span>4 month ago</span>
															<span>&#47;</span>
															<span>
																From <a href="#"><i class="fa fa-youtube-play"></i></a>
															</span>
															<span>&#47;</span>
															<span>410 views</span>
														</p> -->
													</div>
												</div>
											</div>
											<?php } ?>
											<!-- <div class="col-md-4 col-sm-6">
												<div class="latest-vid-img-container">		
													<div class="vid-img">
														<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/2-column-vid-img_3.jpg" alt="video image">
														<a href="video-detail.html" class="play-icon play-small-icon">
										                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
										                </a>
														<div class="overlay-div"></div>
													</div>
													<div class="vid-text">
														<p><span>By</span> <a href="#">Jhon Doe</a></p>
														<h1><a href="video-detail.html">Tennis Night</a></h1>
														<p class="vid-info-text">
															<span>4 month ago</span>
															<span>&#47;</span>
															<span>
																From <a href="#"><i class="fa fa-youtube-play"></i></a>
															</span>
															<span>&#47;</span>
															<span>410 views</span>
														</p>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-6">
												<div class="latest-vid-img-container">		
													<div class="vid-img">
														<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/2-column-vid-img_4.jpg" alt="video image">
														<a href="video-detail.html" class="play-icon play-small-icon">
										                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
										                </a>
														<div class="overlay-div"></div>
													</div>
													<div class="vid-text">
														<p><span>By</span> <a href="#">Jhon Doe</a></p>
														<h1><a href="video-detail.html">Wrestle Mania viii</a></h1>
														<p class="vid-info-text">
															<span>4 month ago</span>
															<span>&#47;</span>
															<span>
																From <a href="#"><i class="fa fa-youtube-play"></i></a>
															</span>
															<span>&#47;</span>
															<span>410 views</span>
														</p>
													</div>
												</div>
											</div> -->
										</div>
									</div>
								</div>
								<?php } ?>
								<div class="related-item">
									<div class="vid-heading overflow-hidden">
										<span class="wow fadeInUp" data-wow-duration="0.8s">
											Videos Sugeridos
										</span>
										<div class="hding-bottm-line wow zoomIn" data-wow-duration="0.8s">
										</div>
									</div>
									<div class="row">
										<div class="vid-container">
											<?php foreach ($movie->movies_suggest as $mov) { ?>
											<?php 
												$locate = array('index.php', 'player', 'index', $mov->id_movie); 
								            	if ($mov->type == 'serie') {
								            		$locate = array('index.php', 'serie', 'index', $mov->id_movie);
								            	}
											?>
											<div class="col-md-4 col-sm-6">
												<div class="latest-vid-img-container">		
													<div class="vid-img">
														<img class="img-responsive" src="<?= $mov->thumbnail; ?>" alt="video image">
														<a href="<?php echo site_url($locate)  ?>" class="play-icon play-small-icon">
										                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
										                </a>
														<div class="overlay-div"></div>
													</div>
													<div class="vid-text">
														<!-- <p><span>By</span> <a href="#">Jhon Doe</a></p> -->
														<h1><a href="<?php echo site_url($locate)  ?>"><?= $mov->title ?></a></h1>
														<!-- <p class="vid-info-text">
															<span>4 month ago</span>
															<span>&#47;</span>
															<span>
																From <a href="#"><i class="fa fa-youtube-play"></i></a>
															</span>
															<span>&#47;</span>
															<span>410 views</span>
														</p> -->
													</div>
												</div>
											</div>
											<?php } ?>
											<!-- <div class="col-md-4 col-sm-6">
												<div class="latest-vid-img-container">		
													<div class="vid-img">
														<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/2-column-vid-img_3.jpg" alt="video image">
														<a href="video-detail.html" class="play-icon play-small-icon">
										                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
										                </a>
														<div class="overlay-div"></div>
													</div>
													<div class="vid-text">
														<p><span>By</span> <a href="#">Jhon Doe</a></p>
														<h1><a href="video-detail.html">Tennis Night</a></h1>
														<p class="vid-info-text">
															<span>4 month ago</span>
															<span>&#47;</span>
															<span>
																From <a href="#"><i class="fa fa-youtube-play"></i></a>
															</span>
															<span>&#47;</span>
															<span>410 views</span>
														</p>
													</div>
												</div>
											</div>
											<div class="col-md-4 col-sm-6">
												<div class="latest-vid-img-container">		
													<div class="vid-img">
														<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/2-column-vid-img_4.jpg" alt="video image">
														<a href="video-detail.html" class="play-icon play-small-icon">
										                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
										                </a>
														<div class="overlay-div"></div>
													</div>
													<div class="vid-text">
														<p><span>By</span> <a href="#">Jhon Doe</a></p>
														<h1><a href="video-detail.html">Wrestle Mania viii</a></h1>
														<p class="vid-info-text">
															<span>4 month ago</span>
															<span>&#47;</span>
															<span>
																From <a href="#"><i class="fa fa-youtube-play"></i></a>
															</span>
															<span>&#47;</span>
															<span>410 views</span>
														</p>
													</div>
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>							
						</div>
					</div>					
				</div>
				<div class="col-md-3 col-sm-4">
					<div class="sidebar">
						<div class="sidebar-vid most-liked">
							<h1 class="sidebar-heading">mais curtidos</h1>
							<?php foreach ($movie->most_liked as $mov) { ?>
							<?php 
								$locate = array('index.php', 'player', 'index', $mov->id_movie); 
			            	if ($mov->type == 'serie') {
			            		$locate = array('index.php', 'serie', 'index', $mov->id_movie);
			            	}
							?>
							<div class="media">
								<div class="media-left">
									<img src="<?= $mov->thumbnail; ?>" alt="video" height="60" width="90">
								</div>
								<div class="media-body">
									<h1><a href="<?php echo site_url($locate); ?>"><?= $mov->title; ?></a></h1>
									<p>
										<span><i class="fa fa-eye"></i> <?= $mov->views; ?></span>
									</p>
								</div>
							</div>
							<?php } ?>
							<!-- <div class="media">
								<div class="media-left">
									<img src="<?php echo base_url() . $this->template->get_theme_path() ?>images/most-liked-img-s2.jpg" alt="video">
								</div>
								<div class="media-body">
									<h1><a href="video-detail.html">Magic</a></h1>
									<p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p>
								</div>
							</div>
							<div class="media">
								<div class="media-left">
									<img src="<?php echo base_url() . $this->template->get_theme_path() ?>images/most-liked-img-s3.jpg" alt="video">
								</div>
								<div class="media-body">
									<h1><a href="video-detail.html">Runner</a></h1>
									<p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p>
								</div>
							</div>
							<div class="media">
								<div class="media-left">
									<img src="<?php echo base_url() . $this->template->get_theme_path() ?>images/most-liked-img-s4.jpg" alt="video">
								</div>
								<div class="media-body">
									<h1><a href="video-detail.html">Fantasy</a></h1>
									<p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p>
								</div>
							</div> -->
						</div>
						<div class="sidebar-vid most-viewd">
							<h1 class="sidebar-heading">mais vistos</h1>
							<?php foreach ($movie->most_viewed as $mov) { ?>
							<?php 
								$locate = array('index.php', 'player', 'index', $mov->id_movie); 
				            	if ($mov->type == 'serie') {
				            		$locate = array('index.php', 'serie', 'index', $mov->id_movie);
				            	}
							?>
							<div class="most-viewd-container">
								<div class="most-viewd-img">
									<img class="img-responsive" src="<?= $mov->thumbnail; ?>" alt="video">
								</div>
								<div class="most-viewd-text">
									<h1><a href="<?php echo site_url($locate); ?>"><?= $mov->title; ?></a></h1>
									<p>
										<span><i class="fa fa-eye"></i> <?= $mov->views; ?></span>
									</p>
								</div>
							</div>
							<?php } ?>
							<!-- <div class="most-viewd-container">
								<div class="most-viewd-img">
									<img class="img-responsive" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/most-viewd-2.jpg" alt="video">
								</div>
								<div class="most-viewd-text">
									<h1><a href="video-detail.html">War Video Compilation</a></h1>
									<p>
										<span><i class="fa fa-comment"></i> 10</span>
										<span><i class="fa fa-eye"></i> 534</span>
									</p>
								</div>
							</div> -->							
						</div>
						<!-- <div class="tags">
							<h1 class="sidebar-heading">Tags</h1>
							<ul class="list-inline list-unstyled">
								<li><a href="#">3D</a></li>
								<li><a href="#">Animals &amp; Birds</a></li>
								<li><a href="#">HD</a></li>
								<li><a href="#">Horror</a></li>
								<li><a href="#">Art</a></li>
								<li><a href="#">Self</a></li>
								<li><a href="#">HD Songs</a></li>
								<li><a href="#">Comedy</a></li>
							</ul>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Secondary Section -->
<script type="text/javascript" src="https://bitmovin-a.akamaihd.net/bitmovin-player/stable/7.1/bitmovinplayer.js"></script>
<script type="text/javascript">

	var user = '<?= json_encode(@$user); ?>';
	console.log('user: ' + user);

	var jProducts = '<?= @$products; ?>';
	console.log('products: ' + jProducts);

	var is_locked = "<?= $movie->movie->locked; ?>";

	var movie = "<?= $movie->movie->movie; ?>"; //"//bitmovin-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8";
	var l = "<?= $login ?>";
	var conf = {
	    key:       "0ec09982-abeb-450a-9419-101524ebe708",
	    cast: {
            enable: true,
            application_id: '21E2FD72'
        },
	    source: {
	      dash: movie.replace('.m3u8', '.mpd'),
	      hls: movie,
	    }
	};
	var player = bitmovin.player("player");
	player.setup(conf).then(function(value) {
	    // Success
	    console.log("Successfully created bitmovin player instance");
	}, function(reason) {
	    // Error!
	    console.log("Error while creating bitmovin player instance");
	});
	player.addEventHandler(bitmovin.player.EVENT.ON_PLAY, function(playEvent){
		checkLogin(player, l, user, is_locked);
	});
</script>