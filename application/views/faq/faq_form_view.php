<div class="row">
          <div class="col-lg-12">
            
            <h2><?php echo ( $this->router->fetch_method() == 'adicionar' ) ? 'Adicionar' : 'Editar' ?> <small>FAQ</small></h2>
            <ol class="breadcrumb">
              <li class="active"> <?php echo ( $this->router->fetch_method() == 'adicionar' ) ? '<i class="fa fa-question-circle"></i> Adicionar' : '<i class="fa fa-pencil-square-o"></i> Editar' ?></li>
            </ol>
            
            <?php if ( validation_errors() ){ ?>
            <div class="alert bg-danger"><!-- alert-info  -->
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo validation_errors() ?>
            </div>
            <?php }elseif ( isset($msg_erro) ){ ?>
            <div class="alert bg-danger"><!-- alert-info  -->
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <?php echo $msg_erro ?>
            </div>
            <?php } ?>
            
            <?php if ( $this->session->flashdata('msg_sucess') ){ ?>
            <div class="col-lg-12 alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('msg_sucess') ?>
            </div>
            <?php } ?>
                
            <form role="form" id="form-faq" action="" method="post">

                <div class="form-group">
                    <label>FAQ</label>
                    <textarea class="form-control textarea-simples" name="html" rows="20"><?php echo set_value("html",@$arr['html']); ?></textarea>
                </div>

                <div class="form-group" style="margin-top: 40px; clear: both">
                    <button type="submit" class="btn btn-default">Enviar</button>
                </div>        

            </form>

          </div>
        </div><!-- /.row -->