<!-- 4 Column Video -->

<div id="secondary-section" class="vids-2-column">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="st-4-column secondary-vid">
					<div class="vid-heading overflow-hidden">
						<span class="wow fadeInUp" data-wow-duration="0.8s"><?= $movies->category->category ?></span>
						<div class="hding-bottm-line wow zoomIn" data-wow-duration="0.8s"></div>
					</div>
					<div class="row">
						<div class="vid-container">
						<?php foreach ($movies->content_category as $mov) { ?>
						<?php 
							$locate = array('index.php', 'player', 'index', $mov->id_movie); 
			            	if ($mov->type == 'serie') {
			            		$locate = array('index.php', 'serie', 'index', $mov->id_movie);
			            	}
						?>
							<div class="col-md-3 col-sm-6">
								<div class="latest-vid-img-container">		
									<div class="vid-img">
										<img class="img-responsive" src="<?= $mov->thumbnail ?>" alt="video image">
										<a href="<?php echo site_url($locate)  ?>" class="play-icon play-small-icon">
						                	<img class="img-responsive play-svg svg" src="<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.svg" alt="play" onerror="this.src='<?php echo base_url() . $this->template->get_theme_path() ?>images/play-button.png'">	                	
						                </a>
										<div class="overlay-div"></div>
									</div>
									<div class="vid-text">
										<!-- <p><span>By</span> <a href="#">Jhon Doe</a></p> -->
										<h1><a href="<?php echo site_url($locate)  ?>" class="limit_ponto_search"><?= $mov->title; ?></a></h1>
										<!-- <p class="vid-info-text">
											<span>4 month ago</span>
											<span>&#47;</span>
											<span>
												From <a href="#"><i class="fa fa-vimeo"></i></a>
											</span>
											<span>&#47;</span>
											<span>410 views</span>
										</p> -->
									</div>
								</div>
							</div>
						<?php } ?>
						</div>
					</div>
				</div>					
			</div>				
		</div>
	</div>
</div>

<!-- Secondary Section -->