        <div class="row" style="clear: both">
          <div class="col-lg-12">
            <h1>Dashboard</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
            </ol>
            
          </div>
        </div><!-- /.row -->

        <div class="row">
          
           <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <a href="<?php echo base_url('index.php/news')?>" class="link-white">
                    <div class="col-xs-3">
                      <i class="fa fa-file-text-o fa-5x"></i>
                    </div>  
                    <div class="col-xs-9 text-right">
                      <div class="huge"><?=$news_total?></div>
                      <div class="announcement-text">Notícias</div>
                    </div>
                  </a>
                    
                </div>
              </div>
              <a href="<?php echo base_url('index.php/news/adicionar')?>">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Adicionar nova
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-plus-circle"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
            
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <a href="<?php echo base_url('index.php/quiz')?>" class="link-white">
                    <div class="col-xs-3">
                      <i class="fa fa-gamepad fa-5x"></i>
                    </div>  
                    <div class="col-xs-9 text-right">
                      <div class="huge"><?=$quiz_total?></div>
                      <div class="announcement-text">Quiz</div>
                    </div>
                  </a>
                    
                </div>
              </div>
              <a href="<?php echo base_url('index.php/quiz/adicionar')?>">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Adicionar novo
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-plus-circle"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <a href="<?php echo base_url('index.php/content_sms')?>" class="link-white">
                    <div class="col-xs-3">
                      <i class="fa fa-envelope-o fa-5x"></i>
                    </div>  
                    <div class="col-xs-9 text-right">
                      <div class="huge"></div>
                      <div class="announcement-text">Conteúdo SMS</div>
                    </div>
                  </a>
                    
                </div>
              </div>
              <a href="<?php echo base_url('index.php/content_sms/add')?>">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Adicionar nova
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-plus-circle"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>  
            
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                    <a href="<?php echo base_url('index.php/versicle')?>" class="link-white">
                        <div class="col-xs-3">
                          <i class="fa fa-book fa-5x"></i>
                        </div>  
                        <div class="col-xs-9 text-right">
                          <div class="huge"><?=$versicle_total?></div>
                          <div>Versículos</div>
                        </div>
                    </a>    
                </div>
              </div>
              <a href="<?php echo base_url('index.php/versicle/adicionar')?>">
                <div class="panel-footer">
                  <div class="row">
                    <div class="col-xs-6">
                      Adicionar novo
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-plus-circle"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>  
         
          <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <a href="<?php echo base_url('index.php/tags')?>" class="link-white">
                    <div class="col-xs-3">
                      <i class="fa fa-tags fa-5x"></i>
                    </div>  
                    <div class="col-xs-9 text-right">
                      <div class="huge"><?=$tags_total?></div>
                      <div class="announcement-text">Tags</div>
                    </div>
                  </a>
                    
                </div>
              </div>
              <a href="<?php echo base_url('index.php/tags/adicionar')?>">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Adicionar nova
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-plus-circle"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
            
        </div><!-- /.row -->


      </div><!-- /#page-wrapper -->
