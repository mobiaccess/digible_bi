<!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <h2 class="text-center">Jogos</h2>
            <hr class="star-primary">
            <div class="row">
				<?php foreach ($home as $game) { ?>
					<div class="col-sm-3 portfolio-item">
						<div class="portfolio-link" href="#mod<?php echo $game['id_game'] ?>" data-toggle="modal">
							<div class="caption">
								<div class="caption-content">
									<i class="fa fa-search-plus fa-3x"></i>
								</div>
							</div>
							<img class="img-fluid" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/portfolio/<?php echo $game['icon'] ?>" alt="">
						</div>
					</div>
				<?php } ?>
            </div>
        </div>
	</section>

<?php foreach ($home as $game) { ?>
	<div class="portfolio-modal modal fade" id="mod<?php echo $game['id_game'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <div class="modal-body">
                                <h2><?php echo $game['title'] ?></h2>
                                <hr class="star-primary">
                                <img class="img-fluid img-centered" src="<?php echo base_url() . $this->template->get_theme_path() ?>img/portfolio/<?php echo $game['icon'] ?>" alt="">
								<!-- <p>Use this area of the page to describe your project. The icon above is part of a free icon set by <a href="https://sellfy.com/p/8Q9P/jV3VZ/">Flat Icons</a>. On their website, you can download their free set with 16 icons, or you can purchase the entire set with 146 icons for only $12!</p> -->
								<p><?php echo $game['description'] ?></p>
                                <ul class="list-inline item-details">
                                    <!-- <li>Client:
                                        <strong><a href="http://startbootstrap.com">Start Bootstrap</a>
                                    </strong>
                                    </li>
                                    <li>Date:
                                        <strong><a href="http://startbootstrap.com">April 2014</a>
                                    </strong>
                                    </li>
                                    <li>Service:
                                        <strong><a href="http://startbootstrap.com">Web Development</a>
                                    </strong>
                                    </li> -->
								</ul>
								<button class="btn btn-success btnDonload btn-lg" id="<?=$game['id_game'];?>" type="button" style="margin-bottom: 10px;"><i class="fa fa-download"></i> Baixar</button>
								<br>
                                <button class="btn btn-success btn-lg" type="button" data-dismiss="modal"><i class="fa fa-backward"></i> Voltar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
<?php } ?>