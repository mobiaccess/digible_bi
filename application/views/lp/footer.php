<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <style>
      body{
        margin-top: 0px;
      }
      div {
        font-size: 12px;
        text-align: center;
        margin-top: 0px;
      }
      div span{
        font-family: tim_sansregular, arial, sans-serif;
        max-width: 390px;
        display: table;
        text-align: center;
        margin: 0 auto;
      }

      @media screen and (max-width: 328px) and (min-width: 200px){
        div{
          font-size: 11px !important;
        }
        body{
          margin-top: 2px;
        }
      } 
    </style>
  </head>
  <body>
    <div>
      <span>
        <small>
        <p style="color:#fff; margin-top:20px; margin-left: auto; margin-right: auto; width: 18em; font-size: 14px; text-align: justify; text-justify: inter-word;">
            Aprenda futebol com o Neymar Jr e se torne um craque! Compartilhe seus videos e concorra a experiências incríveis!</small>
        </p>
      </span>
    </div>
  </body>
</html>