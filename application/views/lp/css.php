<?php
 header("content-type:text/css");
?>

@charset "UTF-8";

html{
  max-width:100%;
  height: auto;
}
body{
  background: white url("http://lp.njrxp.com/application/themes/widow/img/bg_tim.jpg") no-repeat center top;
}

iframe {
background-color: rgba(0,0,0,0);
display:block;
border: none;
width:100%;
pointer-events: none;
}

#headeriframe {
    text-align: center;
    width: 100%;
    height: auto;
    min-height: 240px;
    margin: 0 0 0 0;
  }
@media screen and (min-width: 420px){
  #headeriframe{
    min-height: 260px !important;
  }
}  

#footeriframe {
    margin-top: 0px;
    text-align: center;
    bottom:0;
    max-height: 90px;
    width: 100%;
}

#otp_cancel{
	width:110px;
}

#otp_send{
	width:110px;
}

.system_message{
margin-top: 0px;
}

.system_message_no_iframe{
margin-top: 20px;
}


@font-face {font-family: 'tim_sansregular'; src: url('timsans-regular-webfont.woff2')at('woff2'), url('timsans-regular-webfont.woff')at('woff'); font-weight: normal; font-style: normal;}
@font-face {font-family: 'tim_sansbold'; src: url('timsans-bold-webfont.woff2')at('woff2'), url('timsans-bold-webfont.woff')at('woff'); font-weight: normal; font-style: normal;}
@font-face {font-family: 'tim_sansmedium'; src: url('timsans-medium-webfont.woff2')at('woff2'), url('timsans-medium-webfont.woff')at('woff'); font-weight: normal; font-style: normal;}
@font-face {font-family: 'tim_sansbold_italic'; src: url('timsans-bolditalic-webfont.woff2')at('woff2'), url('timsans-bolditalic-webfont.woff')at('woff'); font-weight: normal; font-style: normal;}
@font-face {font-family: 'tim_sanslight'; src: url('timsans-light-webfont.woff2')at('woff2'), url('timsans-light-webfont.woff')at('woff'); font-weight: normal; font-style: normal; }
#TIM-Auth-mobile-number {
    font-family: tim_sansregular, arial, sans-serif;
    margin-bottom: 0px;
    margin-top: 0px;
    
}

#TIM-Auth-mobile-number #TIM-login-box-header{ background-color:#004488; height:48px; position:relative; left:0; top:0; right:0; z-index:500001}
#TIM-Auth-mobile-number #TIM-login-box-header #TIM-login-box-logo{background-image:url(../imgs/lightbox-logo-tim.png); background-repeat:no-repeat; background-size:contain; background-size:90px 25px; width:90px; height:25px; float:left; display:block; margin:12px 0 0 20px; font-size:0px; text-indent:-3000px}

#TIM-Auth-mobile-number #TIM-login-box{position:relative; top:0; left:50%; width:500px; margin-left:-250px; margin-top:0; background: rgba(0, 0, 0, 0); z-index:500001; clear:both;background-color: transparent !important;}

#TIM-Auth-mobile-number #TIM-login-box-header #TIM-login-box-number{float:right; display:block; margin:12px 20px 0 0px; font-family:tim_sansbold, arial, sans-serif; color:#fff; font-size:20px; line-height:28px; letter-spacing: 1px;}

#TIM-Auth-mobile-number #TIM-login-box-track{width:404px; margin:44px auto 0 auto; height:105px; clear:both; background-image:url(../imgs/track.png); background-repeat:no-repeat; background-position:50% 31px; background-size:contain;}
#TIM-Auth-mobile-number .TIM-login-box-track-3-etapas{width:278px !important}
#TIM-Auth-mobile-number .stage2on5{background-image:url(../imgs/trilha-stage2on5.png) !important;}
#TIM-Auth-mobile-number .stage3on5{background-image:url(../imgs/trilha-stage3on5.png) !important;}
#TIM-Auth-mobile-number .stage4on5{background-image:url(../imgs/trilha-stage4on5.png) !important;}
#TIM-Auth-mobile-number .stage5on5{background-image:url(../imgs/trilha-stage5on5.png) !important;}
#TIM-Auth-mobile-number .stage2on4{background-image:url(../imgs/trilha-stage2on4.png) !important;}
#TIM-Auth-mobile-number .stage3on4{background-image:url(../imgs/trilha-stage3on4.png) !important;}
#TIM-Auth-mobile-number .stage4on4{background-image:url(../imgs/trilha-stage4on4.png) !important;}
#TIM-Auth-mobile-number .stage2on3{background-image:url(../imgs/trilha-stage2on3.png) !important;}
#TIM-Auth-mobile-number .stage3on3{background-image:url(../imgs/trilha-stage3on3.png) !important;}
#TIM-Auth-mobile-number #TIM-login-box-track .TIM-login-box-track-item{width:65px; height:65px; border-radius:33px; background-color:#f0f0f0; float:left; display:block; position:relative; background-image:url(../imgs/trilha-icons.png); background-repeat:no-repeat; background-position:0px -65px;}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-1{background-position:0px -65px}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-1-enable{background-position:0px 0px; background-color:#0078c1 !important;}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-2{background-position:-65px -65px}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-2-enable{background-position:-65px 0px; background-color:#0078c1 !important;}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-3{background-position:-130px -65px}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-3-enable{background-position:-130px 0px; background-color:#0078c1 !important;}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-4{background-position:-195px -65px}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-4-enable{background-position:-195px 0px; background-color:#0078c1 !important;}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-5{background-position:-260px -65px}
#TIM-Auth-mobile-number #TIM-login-box-track .track-item-5-enable{background-position:-260px 0px; background-color:#0078c1 !important;}

#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-1, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-1-enable{left:0}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-2, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-2-enable{left:20px}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-3, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-3-enable{left:40px}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-4, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-4-enable{left:60px}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-5, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-5-enable{left:80px}

#TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-3, #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-3-enable{left:48px}
#TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-4, #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-4-enable{left:96px}
#TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-5, #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-5-enable{left:144px}

#TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-4, #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-4-enable{left:42px}
#TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-5, #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-5-enable{left:84px}

#TIM-Auth-mobile-number #TIM-login-box-track label{width:85px; height:30px; line-height:15px; font-size:13px; color:#fff; text-align:center; position:relative; display:block; float:left; margin-left:-10px}
#TIM-Auth-mobile-number #TIM-login-box-track .label-enable{color:#0078c1 !important}

#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas #label-item-1{left:0;}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas #label-item-2{left:10px;}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas #label-item-3{left:20px;}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas #label-item-4{left:30px;}
#TIM-Auth-mobile-number .TIM-login-box-track-5-etapas #label-item-5{left:40px;}

#TIM-Auth-mobile-number .TIM-login-box-track-4-etapas #label-item-3{left:38px}
#TIM-Auth-mobile-number .TIM-login-box-track-4-etapas #label-item-4{left:76px}
#TIM-Auth-mobile-number .TIM-login-box-track-4-etapas #label-item-5{left:114px}

#TIM-Auth-mobile-number .TIM-login-box-track-3-etapas #label-item-4{left:32px}
#TIM-Auth-mobile-number .TIM-login-box-track-3-etapas #label-item-5{left:64px}

#TIM-Auth-mobile-number #TIM-login-box-phrase{margin:0px auto 5px auto; max-width: 385px; height:auto; overflow:hidden; font-size:16px; text-align:center; color:#FFF !important; line-height:18px; clear:both;font-weight: bold;}
/* ajuste marketing */
#TIM-Auth-mobile-number #TIM-login-box-phrase-he{margin:0px auto 5px auto; max-width: 385px; overflow:hidden; font-size:14px; text-align:center; color:#FFF; line-height:18px; clear:both;font-weight: bold;}

#TIM-Auth-mobile-number #TIM-login-box-form, #TIM-Auth-mobile-number #TIM-login-box-form{max-width:464px; margin:0 auto; text-align:center}
#TIM-Auth-mobile-number #TIM-login-box-form{color:#fff !important; font-size:12px;}
#TIM-Auth-mobile-number #TIM-login-box-form a{color:#fff !important; font-weight:bold; text-decoration:underline}
#TIM-Auth-mobile-number #TIM-login-box-form input{ background-color:#f2f3f4;}
#TIM-Auth-mobile-number #TIM-login-box-form input{ background-color:#f2f3f4;}
#TIM-Auth-mobile-number #TIM-login-box-form input[type="text"]{width:257px; height:36px; border-radius:10px; margin:0 auto; border:none; outline:none; padding-left:21px; font-family:tim_sansregular; color:#333333; font-size:14px; text-align:left; background-repeat:no-repeat; background-position:17px 50%; letter-spacing:1px; line-height:25px; padding-top:0px;}
#TIM-Auth-mobile-number #TIM-login-box-form input[type="text"]#numero-phone-TIM{width:232px; padding-left:46px; background-image:url(../imgs/icon-phone.png);}
#TIM-Auth-mobile-number #TIM-login-box-form input[type="text"]#codigo-TIM{font-size:25px; line-height:25px; padding-top:10px; height:38px}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-question-mark{cursor:pointer; position:absolute; }
#TIM-Auth-mobile-number #TIM-login-box-form .tooltip {position:absolute; display: inline-block; margin:5px 0 0 -35px;}
#TIM-Auth-mobile-number #TIM-login-box-form .tooltip .tooltiptext{opacity:0; filter: alpha(opacity=0); width: 120px; bottom: 100%; left: 50%; margin-left: -60px; background-color: #c1e0f2; color: #004488; text-align: center; padding: 15px 10px; border-radius: 6px; font-size:13px; position: absolute; z-index: 500002; -webkit-transition: all ease-out 0.2s; -moz-transition: all ease-out 0.2s; -o-transition: all ease-out 0.2s; -ms-transition: all ease-out 0.2s; transition: all ease-out 0.2s}
#TIM-Auth-mobile-number #TIM-login-box-form .tooltip .tooltiptext::after {content: " "; position: absolute; top: 100%; left: 50%; margin-left: -10px; border-width: 10px; border-style: solid; border-color: #c1e0f2 transparent transparent transparent;}
#TIM-Auth-mobile-number #TIM-login-box-form .tooltip:hover .tooltiptext { opacity:100; filter: alpha(opacity=100);}

#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-form-info{clear:both; margin:0 auto; text-align:center; padding-top:0}
#TIM-Auth-mobile-number #TIM-login-box-form .TIM-login-form-info-checkbox{width:278px; text-align:left !important;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-form-info input[type="checkbox"]{margin:0; padding:0; font-size:28px; appearance:none; text-align:left; float:left; width:28px; height:28px; content:url(../imgs/checkbocx-unchecked_not_transparent.png); display: block !important; width: 15px; float: left; margin-bottom: -30px; z-index: 31; position: absolute;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-form-info input[type="checkbox"]:checked{content:url(../imgs/checkbocx-checked.png)}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-form-info input[type="checkbox"]:checked{content:url(../imgs/checkbocx-checked.png)}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-form-info label{color: #FFF;float:left; width:245px; display:block; margin-left:40px; line-height:14px;padding-top:6px; padding-left:0;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-form-info label{float:left; width:245px; display:block; margin-left:40px; line-height:14px;padding-top:6px; padding-left:0; margin-bottom: 4px;}
/*#TIM-login-form-checkbox-label-he {margin-top: 7px;}*/

#TIM-Auth-mobile-number #TIM-login-box-form .carregando{margin:30px auto 0 auto;}

#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons{position:relative; margin:30px auto 0 auto;margin-bottom:0px; max-width:412px}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons-otp{position:relative; margin:50px auto;margin-bottom:0px; max-width:412px}
#TIM-Auth-mobile-number #TIM-login-box-form .buttons-tela-01{width:278px}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas .buttons-tela-01 .detach button{width:278px !important; float:left !important;}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons{ display: flex; justify-content: center; max-width:340px !important; margin-top: 0px;}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons-otp{max-width:340px !important;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons button{width:100%; height:40px; bottom:0; position:relative; text-align:center; font-size:14px; text-transform:uppercase; color:#ffffff; background-color:#E91D3F; border:none; outline:none; border-radius:0px; cursor:pointer; float:left; margin-left:5px !important}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons-otp button{width:110px; height:40px; bottom:0; position:relative; text-align:center; font-size:14px; text-transform:uppercase; color:#ffffff; background-color:#8d8d8d; border:none; outline:none; border-radius:20px; cursor:pointer; float:left; margin-left:5px !important}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons button{font-size:13px; margin-left:0px !important}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons-otp button{font-size:13px; margin-left:0px !important}


/* ajuste marketing */
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons-otp button.detach-he{background-color:#0078c1; width: 278px; float: none !important;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons button.detach{background-color:#9a1ed4;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons-otp button.detach{background-color:#0078c1; float:right;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons button.detach-he{background-color:#9a1ed4; float:right;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .tooltip {position:relative; display:inline-block; margin:0; padding-left:3px; width:auto;}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons .tooltip{padding-left:5px; margin-left: 5px !important; display: inline-block;}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons button[type="submit"]{font-weight:bold;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .tooltip .tooltiptext { opacity:0; filter: alpha(opacity=0); width: 150px; bottom: 100%; left: 50%;  margin-left: -60px; background-color: #e32521; color: #ffffff; text-align: center; padding: 15px 10px; border-radius: 6px; font-size:13px; position: absolute; z-index: 500100 !important; -webkit-transition: all ease-out 0.2s; -moz-transition: all ease-out 0.2s; -o-transition: all ease-out 0.2s; -ms-transition: all ease-out 0.2s; transition: all ease-out 0.2s; text-transform:none;}
#TIM-Auth-mobile-number .TIM-login-box-3-etapas .buttons-tela-01 .tooltip .tooltiptext {margin-left: -85px !important;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .tooltip .tooltiptext::after { content: " "; position: absolute; top: 100%; left: 50%; margin-left: -10px; border-width: 10px; border-style: solid; border-color: #e32521 transparent transparent transparent;}
/*#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .tooltip:hover .tooltiptext { opacity:100; filter: alpha(opacity=100);}*/
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .message{font-size:13px; margin-top:15px;}
#TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .message .countdown{font-family:tim_sansbold, tim_sansregular, arial, sans-serif; font-size:20px}
/* sombra do lightbox */
<!-- #TIM-Auth-mobile-number #shade{ position:absolute; /*background-color:#000000; opacity: 0.85; filter: alpha(opacity=85);*/ width:100%; height:0% !important; left:0; top:0; right:0; margin:0; z-index:500000} -->
#shade{ position:absolute; /*background-color:#000000; opacity: 0.85; filter: alpha(opacity=85);*/ width:100%; height:0% !important; left:0; top:0; right:0; margin:0; z-index:500000}


@media screen {
  
  #TIM-Auth-mobile-number #TIM-login-box{padding-left:5% !important; padding-right:5% !important; height:auto !important; margin:0 auto !important; z-index:500001; left:0 !important; margin-left:0 !important; width:90% !important}
  
  #TIM-Auth-mobile-number #TIM-login-box-header{ height:40px !important}
  #TIM-Auth-mobile-number #TIM-login-box-header #TIM-login-box-logo{background-size:73px 20px !important; width:73px !important; height:20px !important; margin-top:10px !important}
  #TIM-Auth-mobile-number #TIM-login-box-header #TIM-login-box-number{margin-top:10px !important; font-size:18px !important; line-height:22px !important}
  
  #TIM-Auth-mobile-number #TIM-login-box-track{width:278px !important; height:80px !important; background-position: center 20px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .TIM-login-box-track-item{width:45px !important; height:45px !important; border-radius:22px !important; background-image:url(../imgs/trilha-icons-70pct.png) !important;}
  
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-2{background-position:-46px -46px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-2-enable{background-position:-46px 0px !important;}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-3{background-position:-92px -46px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-3-enable{background-position:-92px 0px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-4{background-position:-138px -46px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-4-enable{background-position:-138px 0px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-5{background-position:-183px -46px !important}
  #TIM-Auth-mobile-number #TIM-login-box-track .track-item-5-enable{background-position:-183px 0px !important}
  
  #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-2, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-2-enable{left:13px}
  #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-3, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-3-enable{left:26px}
  #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-4, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-4-enable{left:39px}
  #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-5, #TIM-Auth-mobile-number .TIM-login-box-track-5-etapas .track-item-5-enable{left:52px}
  
  #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-3, #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-3-enable{left:33px}
  #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-4, #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-4-enable{left:66px}
  #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-5, #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas .track-item-5-enable{left:99px}
  
  #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-4, #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-4-enable{left:72px}
  #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-5, #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas .track-item-5-enable{left:144px}
  
  #TIM-Auth-mobile-number #TIM-login-box-track label{width:54px; height:auto !important; line-height:12px; font-size:10px; margin-left:-6px}
  
  #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas #label-item-3{left:32px}
  #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas #label-item-4{left:63px}
  #TIM-Auth-mobile-number .TIM-login-box-track-4-etapas #label-item-5{left:92px}
  
  #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas #label-item-4{left:70px}
  #TIM-Auth-mobile-number .TIM-login-box-track-3-etapas #label-item-5{left:140px}
  
  #TIM-Auth-mobile-number #TIM-login-box-phrase{overflow:auto !important; font-size:14px !important;}
  
  #TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons button, #TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons button {width:100% !important; max-width:180px !important; clear:both !important; margin:0px  0px 5px 0px !important; float:none !important}
  #TIM-Auth-mobile-number #TIM-login-box-form #TIM-login-box-buttons .tooltip{width:100% !important; max-width:180px !important; clear:both !important; margin:0px auto !important; float:none !important; padding-left:0px}
  <!-- #TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons .tooltip{padding-left:0px;} -->
  #TIM-Auth-mobile-number #TIM-login-box-form, #TIM-Auth-mobile-number #TIM-login-box-form{height:auto !important; padding-bottom:0px !important}
  
  #TIM-Auth-mobile-number .TIM-login-box-3-etapas #TIM-login-box-form #TIM-login-box-buttons button{font-size:14px;}
  
  
  input.hecheckbox[type=checkbox] {
    display:none !important;
  }
  input.hecheckbox[type=checkbox] + label
  {
    background: url("../imgs/checkbocx-unchecked_not_transparent.png") no-repeat;
    background-size: 28px;
    width: 28px;
    min-height: 28px;
    display:inline-block;
    padding: 0 0 0 0px;
    padding-left: 33px;
    padding-top:6px;
  }
  input.hecheckbox[type=checkbox]:checked + label
  {
    background: url("../imgs/checkbocx-checked.png") no-repeat;
    background-size: 28px;
    width: 28px;
    min-height: 28px;
    display:inline-block;
    padding: 0 0 0 0px;
    padding-left: 33px;
    padding-top:6px;
  }
  
  #sign_button{
    background-color: #9a1ed4 !important;
    
  }

  #loginForm{
    justify-content: center; 
  }
  #Bharosa_Challenge_PadDataField{
    justify-content: center; 
    text-align: center !important;
    width:257px; 
    height:36px; 
    border-radius:10px; 
    margin:0 auto;
    border:none; 
    outline:none; 
    font-family:tim_sansregular; 
    color:#333333; 
    font-size:14px; 
    text-align:left; 
    background-repeat:no-repeat; 
    background-position:17px 50%; 
    letter-spacing:1px; 
    line-height:25px; 
    padding-top:0px;
    display:block;
  }