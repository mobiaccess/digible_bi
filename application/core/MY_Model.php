<?php

require_once(APPPATH.'libraries/MyException.class.php');

class MY_Model extends CI_Model {

	protected $Logger;
        
	protected $errorPrefix= 'G';

	protected $errorPadding= 6;
        
        protected $match;

	public function __construct(){
		parent::__construct();
	}

	public function setLogger(&$pLogger) {
		$this->Logger = $pLogger;
	}

	private function getMicroTime() {
		list($_usec, $_sec) = explode(" ", microtime());
		return ((float) $_usec + (float) $_sec);
	}

	protected function execute($conn, $query, $arrParam=null) {
		try {
			$this->setErrorVerbosity($conn);
			$startTime = $this->getMicroTime();
			$rows = $conn->query($query, $arrParam);
			$this->Logger->info('['.get_class($this).'] query[' . $conn->last_query() . ']');
			$endTime = $this->getMicroTime();
			$totalTime = $endTime-$startTime;
			$this->Logger->info('Total de tempo gasto: '.$totalTime);
			$error= $this->getLastError($conn);
			if($error){
				throw new MyException($error);
			}
			//$exec= $rows->row_array();
			//$this->Logger->info('Return: '.print_r($exec,true));
			$this->Logger->info('['.get_class($this).']Rotina executada');
			return true;
		} catch (Exception $e) {
			$this->Logger->info('['.get_class($this).']Rotina nao executada');
			$this->Logger->info('['.get_class($this).']query[' . $conn->last_query() . ']');
			$this->Logger->error('['.get_class($this).']error[' . $e . ']');
			$this->Logger->info('['.get_class($this).']Fim');
			throw new MyException($e->getMessage(),$e->getCode());
		}
	}

	protected function executeRow($conn, $query, $arrParam=null) {
		try {
			$this->setErrorVerbosity($conn);
			$startTime = $this->getMicroTime();
			$rows = $conn->query($query, $arrParam);
			$this->Logger->info('['.get_class($this).']query[' . $conn->last_query() . ']');
			$endTime = $this->getMicroTime();
			$totalTime = $endTime-$startTime;
			$this->Logger->info('Total de tempo gasto: '.$totalTime);
			$error= $this->getLastError($conn);
                        $this->Logger->info('\$error: ['.$error.']');                        
			if($error){
				throw new MyException($error);
			}
                        
			if(!$rows){
				$this->Logger->info('Query nao retornou linhas');
				return array();
			}
			$exec = $rows->row_array();
			$this->Logger->debug('Return: '.print_r($exec,true));
			$this->Logger->info('['.get_class($this).']Rotina executada');
			return $exec;
		}catch (Exception $e){
			$this->Logger->info('['.get_class($this).']Rotina nao executada');
			$this->Logger->info('['.get_class($this).']query[' . $conn->last_query() . ']');
			$this->Logger->error('['.get_class($this).']error[' . $e . ']');
			$this->Logger->info('['.get_class($this).']Fim');
			throw new MyException($e->getMessage(),$e->getCode());
		}
	}

	protected function executeRows($conn, $query, $arrParam=null) {
		try {
			$this->setErrorVerbosity($conn);
			$startTime = $this->getMicroTime();
			$rows = $conn->query($query, $arrParam);
			$this->Logger->info('['.get_class($this).']query[' . $conn->last_query() . ']');
			$endTime = $this->getMicroTime();
			$totalTime = $endTime-$startTime;
			$this->Logger->info('Total de tempo gasto: '.$totalTime);
			$error= $this->getLastError($conn);
			if($error){
				throw new MyException($error);
			}
			if(!$rows){
				$this->Logger->info('Consulta nao retornou registros');
				return array();
			}
			$exec = $rows->result_array();
			$this->Logger->debug('Return: '.print_r($exec,true));
			$this->Logger->info('['.get_class($this).']Rotina executada');
			return $exec;
		} catch (Exception $e) {
			$this->Logger->info('['.get_class($this).']Rotina nao executada');
			$this->Logger->debug('['.get_class($this).']query[' . $conn->last_query() . ']');
			$this->Logger->error('['.get_class($this).']error[' . $e . ']');
			$this->Logger->info('['.get_class($this).']Fim');
			throw new MyException($e->getMessage(),$e->getCode());
		}
	}

	protected function updateAffectedRows($conn, $query, $arrParam=null) {
		$this->setErrorVerbosity($conn);
		$conn->trans_start();
		$startTime = $this->getMicroTime();
		$rows = $conn->query($query, $arrParam);
		if($conn->trans_status() === FALSE){
			$this->Logger->info('['.get_class($this).']Rotina nao executada');
			$this->Logger->info('['.get_class($this).']query[' . $conn->last_query() . ']');
			$this->Logger->info('['.get_class($this).']Fim');
			$conn->trans_rollback();
			return "updated fail";
		}else{
			$this->Logger->info('['.get_class($this).']query[' . $conn->last_query() . ']');
			$endTime = $this->getMicroTime();
			$totalTime = $endTime-$startTime;
			$this->Logger->info('Total de tempo gasto: '.$totalTime);
			$exec = $rows->affected_rows();
			$this->Logger->debug('Return: '.print_r($exec,true));
			$this->Logger->info('['.get_class($this).']Rotina executada');
			$conn->trans_commit();
			return $exec;
		}
	}

	protected function getTimestampWithMiliseconds(){
		$fator=1000000;
		$milisegundos= microtime(true);
		return date('Y-m-d H:i:s.'.(int)(($milisegundos-(int)$milisegundos)*$fator));
	}

	protected function setErrorVerbosity($conn){
		$conn->call_function('set_error_verbosity', $conn->conn_id, PGSQL_ERRORS_VERBOSE);
	}

	protected function getLastError($conn){
		$error= $conn->call_function('last_error',$conn->conn_id);
		if(preg_match('/^(ERROR):  ([a-zA-Z0-9]*): (.*)/i',$error,$matches)){
                        $this->match = $matches;
			return '['.$matches[2].']'.$matches[3];
		}else{
			return false;
		}
	}
	protected function formatError($pErrorCode){
		if(is_numeric($pErrorCode)){
			$pErrorCode= abs($pErrorCode);
		}
		return $this->errorPrefix.str_pad($pErrorCode, $this->errorPadding, '0', STR_PAD_LEFT);
	}        
    
        public function to_pg_array($set,$type="string") {

            settype($set, 'array'); // can be called with a scalar or array

            $result = array();

            foreach ($set as $t) {

                if (is_array($t)) {
                    $result[] = to_pg_array($t);
                } else {
                    $t = str_replace('"', '\\"', $t); // escape double quote
                    if ( ! is_numeric($t) ) // quote only non-numeric values
                        $t = ( $type == 'int' ) ? $t : '"' . $t . '"'; // verificando o tipo
                    if ( $t != '""'  )
                        $result[] = $t; // para resolver um problema de termos json = {""}
                }

            }

            return '{' . implode(",", $result) . '}'; // format - verify if existis a valid return. If not, return empty string
        }
        
        protected function treatParams($params) {
            foreach($params as $key => $value) {
                if(!isset($params[$key]) || trim($params[$key]) == '')
                    $params[$key] = null;
            }

            if ($params == null) {
                $this->Logger->info("[".__METHOD__."] \$params: NULL");
                return $params;
            }
            
            $this->Logger->info("[".__METHOD__."] \$params: ". print_r ($params, true));
            
            return $params;
        }
        
        protected function formatDate($date) {
            return ( ! is_null($date) && ! empty($date) && trim($date) != "" ) ? date_format(date_create($date), 'Y-m-d H:i:s') : null;
        }
        
        function cleanString($text) {
            $utf8 = array(
                '/[áàâãªä]/u'   =>   'a',
                '/[ÁÀÂÃÄ]/u'    =>   'A',
                '/[ÍÌÎÏ]/u'     =>   'I',
                '/[íìîï]/u'     =>   'i',
                '/[éèêë]/u'     =>   'e',
                '/[ÉÈÊË]/u'     =>   'E',
                '/[óòôõºö]/u'   =>   'o',
                '/[ÓÒÔÕÖ]/u'    =>   'O',
                '/[úùûü]/u'     =>   'u',
                '/[ÚÙÛÜ]/u'     =>   'U',
                '/ç/'           =>   'c',
                '/Ç/'           =>   'C',
                '/ñ/'           =>   'n',
                '/Ñ/'           =>   'N',
                '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
                '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
                '/[“”«»„]/u'    =>   ' ', // Double quote
                '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
            );
            
            return preg_replace(array_keys($utf8), array_values($utf8), $text);
        }
        
        function getBoolean() {
            return array(
                array('label'=> 'TODOS', 'value'=> '2')
                , array('label'=> 'ATIVO', 'value'=> '1')
                , array('label'=> 'INATIVO', 'value'=> '0')
            );
        }
    
}

?>