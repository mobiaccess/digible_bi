<?php

require_once(APPPATH . 'libraries/MyException.class.php');


class MY_Controller extends CI_Controller{

    protected $pid;
    protected $Logger;
    protected $section;

    public function __construct() {

        parent::__construct();
        $this->pid = getmypid();
        $this->setLogger();

        //$this->template->title('Home', 'Publicador Zodiack');

    }
    
    private function setLogger(){
            $this->config->load('logger', true);
            $logPath = $this->config->item('log_path', 'logger');
            $logFilename = strtolower(get_class($this));
            $logLevel = $this->config->item('log_level', 'logger');
            $this->Logger = new Acotel_Logger_Logger($logLevel, $logPath, $logFilename);
            $this->Logger->startTransaction();
            $this->Logger->info('[ENVIRONMENT]['.strtoupper(ENVIRONMENT).']');
            $this->Logger->info("[PROCESS ID][{$this->pid}]");
    }

    public function __destruct() {
            if ($this->Logger) {
                    $this->Logger->endTransaction();
            }

            flush();
            //posix_kill( getmypid(), 28 );
    }

    public function requestSiteURL(){
            echo json_encode(array("url" => site_url("")));
    }

    public function replaceSpecialChars(){
            $phrase = $this->input->get("phrase");
            echo json_encode(array("phrase" => simpleText($phrase)));
    }

    public function removeSpecialChars(){
            $phrase = $this->input->get("phrase");
            echo json_encode(array("phrase" => simpleOnlyLettersAndUnderline($phrase)));
    }

    /**
     * Método usado em Item e Tag
     */
    public function json_tags(){

        echo json_encode($this->tags_model->getTags(array('busca'=>$this->input->post('query'),'fields'=>'id_tag as id, tag as name','field'=>'tag','order'=>'asc','limit'=>10,'offset'=>'0')),JSON_NUMERIC_CHECK);

    }

        public function moveFile($sourceFileName, $sourcePath, $destinationFileName, $destinationPath){
                $this->Logger->info('[' . __METHOD__ . '] Movendo de: ' . $sourcePath . $sourceFileName . ' para: ' . $destinationPath . $destinationFileName);
                
                if(copy($sourcePath.$sourceFileName, $destinationPath.$destinationFileName)){
                        if(unlink($sourcePath.$sourceFileName)){
                                $this->Logger->info('[' . __METHOD__ . '] Arquivo movido com sucesso');
                                return true;
                        }
                        $this->Logger->info('[' . __METHOD__ . '] Error ao mover o arquivo');
                        return false;
                }
                $this->Logger->info('[' . __METHOD__ . '] Error ao mover o arquivo');
                return false;
        }
        
}

?>
