<?php

//$lang['required'] = 'Campo obrigatório.';
$lang['required'] = 'O campo %s é obrigatório.';
$lang['isset'] = 'O campo %s deve ter algum valor.';
//$lang['valid_email'] = 'Campo %s inválido.';
$lang['valid_email'] = 'O campo %s deve conter um endereço de e-mail válido.';
$lang['valid_emails'] = 'O campo %s deve conter todos os endereços de e-mails válidos.';
$lang['valid_url'] = 'O campo %s deve conter uma URL válida.';
$lang['valid_ip'] = 'O campo %s deve conter um IP válido.';
$lang['min_length'] = 'O campo %s deve ter pelo menos %s caracteres em comprimento.';
$lang['max_length'] = 'O campo %s não pode exceder %s caracteres de tamanho.';
$lang['exact_length'] = 'O campo %s deve ter exatamente %s caracteres em comprimento.';
$lang['alpha'] = 'O campo %s deve conter somente caraceteres do alfabeto.';
$lang['alpha_numeric'] = 'O campo %s deve conter somente caracteres alfa-numéricos.';
$lang['alpha_dash'] = 'O campo %s deve conter somente caracteres alfa-numéricos, sublinhados, e traços.';
$lang['numeric'] = 'O campo %s deve conter somente números.';
$lang['is_numeric'] = 'O campo %s deve conter somente caracteres numéricos.';
$lang['integer'] = 'O campo %s deve conter um número inteiro.';
$lang['regex_match'] = "The %s field is not in the correct format.";
$lang['matches'] = 'O campo %s não corresponde ao campo %s.';
$lang['is_unique'] = "O campo %s já existe. Por favor, tente um outro.";
$lang['is_natural'] = 'O campo %s deve conter apenas números positivos.';
$lang['is_natural_no_zero'] = 'O campo %s deve conter um número maior que zero.';
$lang['decimal'] = "The %s field must contain a decimal number.";
$lang['less_than'] = "The %s field must contain a number less than %s.";
$lang['greater_than'] = "The %s field must contain a number greater than %s.";
$lang['valid_senha'] = "A %s não corresponde.";

?>