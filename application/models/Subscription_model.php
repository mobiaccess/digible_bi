<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'libraries/SubscriptionException.class.php');

class Subscription_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->eivas = $this->load->database('njrxp', true);
    }

}

