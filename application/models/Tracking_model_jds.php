<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2019-06-22
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tracking_model_jds extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->eivas = $this->load->database('jogos_de_sempre', true);
    }

    ### Make Logical Files ###

    public function makeSubscriptionLogicalFile($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.create_logical_files_subscription(?);";

        try {
            $this->execute($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function makeCanceledLogicalFile($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.create_logical_files_canceled(?);";

        try {
            $this->execute($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function makeBillingLogicalFile($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.create_logical_files_billing(?);";

        try {
            $this->execute($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function makeLandingLogLogicalFile($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.create_logical_files_access(?);";

        try {
            $this->execute($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    ### Make Files ###

    public function makeBaseSubscription($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.make_subs_base(?);";

        try {
            $ret = $this->executeRow($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function makeBaseCanceled($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.make_canc_base(?);";

        try {
            $ret = $this->executeRow($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function makeBaseBilling($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.make_bill_base(?);";

        try {
            $ret = $this->executeRow($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function makeBaseAccess($date) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.make_access_base(?);";

        try {
            $ret = $this->executeRow($this->eivas, $sql, array($date));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    ### Create Fisical FIles ###


    public function getSubscriptionLogicalFiles($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_subscription_logical_files(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getCanceledLogicalFiles($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_canceled_logical_files(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getBillingLogicalFiles($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_billing_logical_files(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getAccessLogicalFiles($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_access_logical_files(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getSubscriptionContentFile($idFile, $referenceDate, $carrierCode) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_subscription_content_files(?,?,?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($idFile, $referenceDate, $carrierCode));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getCanceledCountentFile($idFile, $referenceDate, $carrierCode) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_canceled_content_files(?,?,?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($idFile, $referenceDate, $carrierCode));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getBillingContentFile($idFile, $referenceDate, $carrierCode) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_billing_content_files(?,?,?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($idFile, $referenceDate, $carrierCode));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getAccessCountentFile($idFile, $referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_access_content_files(?,?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($idFile, $referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function setCreateSubscriptionFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_create_subscription_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function setCreateCanceledFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_create_canceled_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function setCreateBillingFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_create_billing_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function setCreateAccessFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_create_access_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function getSubscriptionFilesToSend($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_subscription_files_to_send(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getCanceledFilesToSend($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_canceled_files_to_send(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getBillingFilesToSend($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_billing_files_to_send(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function getAccessFilesToSend($referenceDate) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.get_access_files_to_send(?)";

        try {
            $ret = $this->executeRows($this->eivas, $sql, array($referenceDate));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
        return $ret;
    }

    public function setSentSubscriptionFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_sent_subscription_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function setSentCanceledFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_sent_canceled_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function setSentBillingFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_sent_billing_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function setSentAccessFile($idFile) {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.set_sent_access_file(?);";

        try {
            $this->execute($this->eivas, $sql, array($idFile));
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

    public function createTables() {
        $this->Logger->info('[' . __METHOD__ . ']Start');

        $sql = "select * from etl.create_tables();";

        try {
            $this->execute($this->eivas, $sql);
        } catch (MyException $e) {
            $this->Logger->info('[' . __METHOD__ . '] exception: '. $e->getSpecialMessage());
        }

        $this->Logger->info('[' . __METHOD__ . ']End');
    }

}

