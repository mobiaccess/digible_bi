<?php
/**
 * Library para manter menu de categorias atualizado;
 * 
 * @author Fernando Siggelkow <fernando.siggelkow@acotel.com>
 * @since 2017-05-15
 * 
 **/
class Mymenu {

	protected $CI;
	protected $Logger;

	public function __construct($params) {
		$this->CI =& get_instance();
		$this->Logger = $params['logger'];
	}

	public function getMenuCategory() {

		// $this->CI->load->library('session');
		$this->CI->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'my_'));

		// if (!$categories = $this->CI->session->categories) {
		if (!$categories = $this->CI->cache->get('menu_category')) {

			$this->CI->load->model('home_model');
            $this->CI->home_model->setLogger($this->Logger);

            $obj = json_decode($this->CI->home_model->getCategories());
            $categories = $obj->categories;

            // $this->CI->session->categories = $categories;
            $this->CI->cache->save('menu_category', $categories, 300);

		}

		return $categories;

	}

}