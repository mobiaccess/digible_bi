<?php
/**
 * Faz o billing do cliente por SDP (alternativa ao billing por Centro Estrela).
 *
 * @package Acotel
 * @subpackage Billing
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Faz o billing do cliente por SDP (alternativa ao billing por Centro Estrela).
 *
 * @package Acotel
 * @subpackage Billing
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_Billing_SDP extends Acotel_API_GenericAPI {
	/**
	 * Array with configuration options
	 *
	 * @var array
	 */
	private $config = array(
		'timeout' => 30,
	);

	private static $reservation_codes = array();

	/**
	 *
	 *
	 * @param array|string $config
	 * @throws Exception
	 * @throws Acotel_Exceptions_ConfigurationNotFoundException
	 * @see Acotel_Billing_SDP::setConfig()
	 */
	public function __construct($config = null) {
		parent::__construct();
		if ($config !== null) {
			$this->setConfig($config);
		}
	}

	/**
	 * Sets which configuration will be used. Expected configuration is:
	 * <ul>
	 * <li><b>url</b>: Full URL of API</li>
	 * <li><b>cfs</b></li>
	 * <li><b>sender</b></li>
	 * <li><b>resource</b></li>
	 * <li><b>price</b></li>
	 * </ul>
	 *
	 * @param array|string $config If is a string, the method will call
	 *                             {@link Acotel_Config::get()} to get the
	 *                             params. If an array is passed, then it will
	 *                             be used instead of calling config.
	 * @throws Acotel_Exceptions_ConfigurationNotFoundException
	 * @see Acotel_Config::get()
	 */
	public function setConfig($config) {
		if (is_array($config)) {
			$cfg = $config;
		} else {
			$cfg = Acotel_Config::get($config);
			if (!$cfg) {
				throw new Acotel_Exceptions_ConfigurationNotFoundException();
			}
		}

		$this->config = array_merge($this->config, $cfg);
	}

	/**
	 * Reserves the credit.
	 *
	 * @param Acotel_MSISDN_MSISDN|string $msisdn
	 * @param float $price
	 * @return string Transaction ID
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 */
	public function reserve($msisdn) {
		if ($msisdn instanceof Acotel_MSISDN_MSISDN) {
			$number = $msisdn->getNumber();
		} else {
			$number = Acotel_MSISDN_MSISDN::formatMSISDN($msisdn);
		}

		$params = array(
			'msisdn' => $msisdn,
		);

		$response = $this->executeAction('reservecredit', $params);
		if ($response['reservation_id']) {
			$reserve_id = $response['reservation_id'];
			self::$reservation_codes[$reserve_id] = $msisdn;
		}

		return $response;
	}

	/**
	 * Commit previous reserved credit.
	 *
	 * @param string $reservation_id
	 */
	public function commit($reservation_id) {
		$params = array(
			'reservation_id' => $reservation_id,
			'msisdn' => self::$reservation_codes[$reservation_id],
		);

		$response = $this->executeAction('commitcredit', $params);
		unset(self::$reservation_codes[$reservation_id]);
		return $response;
	}

	/**
	 * Cancels previous reserved credit.
	 *
	 * @param string $reservation_id
	 */
	public function rollback($reservation_id) {
		$params = array(
			'reservation_id' => $reservation_id,
			'msisdn' => self::$reservation_codes[$reservation_id],
		);

		$response = $this->executeAction('rollbackcredit', $params);
		unset(self::$reservation_codes[$reservation_id]);
		return $response;
	}

	/**
	 * Reserve and commit in one operation.
	 *
	 * @param string $msisdn
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 */
	public function reserveAndCommit($msisdn) {
		$number = Acotel_MSISDN_MSISDN::formatMSISDN($msisdn);

		$params = array(
			'msisdn' => $number,
		);

		return $this->executeAction('reserveandcommit', $params);
	}

	/**
	 * Returns the contract ID.
	 *
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 */
	public function getContractInfo() {
		return $this->executeAction('getcontractid');
	}

	/**
	 *
	 * @param string $action
	 * @param array $params
	 * @throws Acotel_Billing_Exceptions_BillingException
	 * @see Acotel_BillingSDP::parseResponse()
	 */
	protected function executeAction($action, array $params = array()) {
		$params['action'] = $action;
		$params['sender'] = $this->config['sender'];
		$params['cfs'] = $this->config['cfs'];
		$params['resource'] = $this->config['resource'];
		$params['price'] = $this->config['price'];
		$params['sdp_soap_call_url'] = $this->config['soap_call_url'];

		$this->setURL($this->config['url']);
		$this->setParams($params);

		$data = $this->getResponse('array');
		if (!$data) {
			return null;
		}

		$response = $data['sdp']['response'];
		$request = $data['sdp']['request'];

		/*
		if ($response['code'] != 1) {
			if (isset($response['msg'])) {
				$msg = $response['msg'];
			} else {
				$msg = '';
			}
			$this->throwException($msg, $response['code']);
			return;
		}
		*/

		$info = array(
			'credit_message' => @$response['code'],
			'credit_description' => @$response['msg'],
			'service_provider' => @$request['service_provider'],
			'contract_id' => @$request['contractid'],
			'application_id' => @$request['application_id'],
		);

		if (array_key_exists('account_type', $response)) {
			$info['account_type']= $response['account_type'];
		}else{
			$info['account_type']= NULL;
		}

		if (array_key_exists('reservation_id', $response)) {
			$info['reservation_id']= $response['reservation_id'];
		}else{
			$info['reservation_id']= NULL;
		}

		return $info;
	}

	/**
	 * Throws an Exception, with correct message for each error code.
	 *
	 * @param string $resp_msg
	 * @param int $code
	 * @throws Acotel_Billing_Exceptions_BillingException
	 * @see http://wiki.acotelbr.com.br/sdp_codes
	 */
	protected function throwException($resp_msg, $code) {
		switch ($code) {
		case -23:
			$msg = "Connection error [$resp_msg: $code].";
			break;
		case -22:
			$msg = "Unsupported operation [$resp_msg: $code].";
			break;
		case -6:
			$msg = "Unknown user [$code].";
			break;
		case -1:
			$msg = "Insufficient funds [$code].";
			break;
		case -12:
			$msg = "Timeout [$resp_msg: $code].";
			break;
		case -20:
			$msg = "Other error [$resp_msg: $code].";
			break;
		case -21:
			$msg = "Post paid blacklist status [$code].";
			break;
		case -24:
			$msg = "Socket error [$resp_msg: $code].";
			break;
		case -25:
			$msg = "Soap error: timeout contacting TIM [$resp_msg: $code].";
			break;
		case -26:
			$msg = "SDP config error: check your application config. " .
				"If it's ok, then could be a problem on SDP config " .
				"[$resp_msg: $code].";
			break;
		default:
			$msg = "Unknown error from SDP API [$resp_msg: $code].";
			break;
		}

		throw new Acotel_Billing_Exceptions_BillingException($msg, $code);
	}
}
?>
