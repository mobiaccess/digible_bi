<?php
/**
 * XML parser
 *
 * @package Acotel
 * @subpackage Parser
 * @author Diogo Capistrano Nobre <diogo.nobre@acotel.com>
 * @author Thiago Costa <thiago.costa@acotel.com>
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * XML parser that converts an XML file (or string) into an array or a
 * SimpleXMLElement object.
 *
 * Usage:
 * <code>
 * $parser = new Acotel_Parser_XMLParser();
 * $array = $parser->transformToArray($xml_string_or_filename);
 * </code>
 *
 * @package Acotel
 * @subpackage Parser
 * @author Diogo Capistrano Nobre <diogo.nobre@acotel.com>
 * @author Thiago Costa <thiago.costa@acotel.com>
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_Parser_XMLParser {
	public function __construct(){
	}

	/**
	 * Transform XML to SimpleXMLElement object. Encapsulates all exceptions
	 * returned by SimpleXML into Acotel_Exceptions_InvalidXMLException.
	 *
	 * @param string $xml XML as string or path do XML file
	 * @return SimpleXMLElement
	 * @throws Acotel_Exceptions_InvalidXMLException
	 */
	public function transformToSimpleXML($xml){
		try {
			return new SimpleXMLElement($xml);
		} catch (Exception $e) {
			throw new Acotel_Exceptions_InvalidXMLException($e->getMessage(),
				$e->getCode());
		}
	}

	/**
	 * Transform a XML into an array. The attributes are returned in the same
	 * format as tags.
	 *
	 * @param string XML string or filename
	 * @return array
	 */
	public function transformToArray($xml) {
		$simplexml = $this->transformToSimpleXML($xml);
		$tagName = $simplexml->getName();
		$array = array(
			$tagName => $this->parse($simplexml),
		);

		foreach($simplexml->attributes() as $attr => $value){
			$array[$tagName][$attr] = (string) $value;
		}

		return $array;
	}

	private function parse($xml) {
		$array = array();
		foreach ($xml as $elem) {
			if (count($elem)) {
				$value = $this->parse($elem);
			} else {
				$value = (string) $elem[0];
			}

			if ($elem->attributes()) {
				$original_value = $value;
				$value = array();
				foreach ($elem->attributes() as $attr => $val) {
					$value[$attr] = (string) $val;
				}
				if (is_array($original_value)) {
					$value = array_merge($value, $original_value);
				} else {
					$value['value'] = $original_value;
				}
			}

			$tagName = $elem->getName();
			if (array_key_exists($tagName, $array)) {
				$tmp = $array[$tagName];
				$array[$tagName] = array($tmp, $value);
			} else {
				$array[$tagName] = $value;
			}
		}
		return $array;
	}
}
?>
