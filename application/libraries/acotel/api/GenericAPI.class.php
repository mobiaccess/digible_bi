<?php
/**
 * Generic class for Acotel APIs.
 *
 * @package Acotel
 * @subpackage API
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Including HTTP_Request2 library
 */
require_once 'HTTP/Request2.php';
require_once 'HTTP/Request2/Adapter/Mock.php';

/**
 * Generic class for Acotel APIs. This class can be used standalone, but
 * the primary use is to be subclassed by more specific API classes.
 *
 * @package Acotel
 * @subpackage API
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_API_GenericAPI {
	protected $url;
	protected $params = array();
	protected $request;

	/**
	 * Construct the object and configure timeout for HTTP request
	 */
	public function __construct() {
		$config = array(
			'adapter' => 'HTTP_Request2_Adapter_Socket',
			'connect_timeout' => 2,
			'use_brackets' => false,
			'follow_redirects' => true,
		);

		$request = new HTTP_Request2(null, HTTP_Request2::METHOD_GET, $config);
		$this->request = $request;
	}

	/**
	 * Sets which adapter will be used when make the request. Valid values are
	 * 'curl' (makes the request using curl extension) or 'socket' (the default,
	 * will use PHP bultin socket functions). You can also pass a
	 * HTTP_Request2_Adapter_Mock instance (used only for unit test).
	 *
	 * @param string|HTTP_Request2_Adapter_Mock $adapter
	 * @throws InvalidArgumentException
	 */
	public function setAdapter($adapter) {
		if ($adapter == 'curl') {
			$adapterClassOrObject = 'HTTP_Request2_Adapter_Curl';
		} else if ($adapter == 'socket') {
			$adapterClassOrObject = 'HTTP_Request2_Adapter_Socket';
		} else if ($adapter instanceof HTTP_Request2_Adapter_Mock) {
			// Used only on unit tests
			$adapterClassOrObject = $adapter;
		} else {
			$msg = "Adapter [$adapter] is invalid. Use 'curl' or 'socket'.";
			throw new InvalidArgumentException($msg);
		}

		$this->request->setConfig('adapter', $adapterClassOrObject);
	}

	/**
	 * Sets the method used to make the request. Possible values are 'GET'
	 * or 'POST'.
	 *
	 * @param string $method Method to make the request (GET or POST)
	 */
	public function setMethod($method) {
		if ($method == 'GET') {
			$this->request->setMethod(HTTP_Request2::METHOD_GET);
		} else {
			$this->request->setMethod(HTTP_Request2::METHOD_POST);
		}
	}

	/**
	 * Configure base URL for request.
	 *
	 * @param string $url
	 */
	public function setURL($url) {
		$valid_url = filter_var($url, FILTER_VALIDATE_URL,
			FILTER_FLAG_SCHEME_REQUIRED);

		if (!$valid_url) {
			throw new InvalidArgumentException("Invalid URL: [$url]");
		}

		$this->url = $url;
	}

	/**
	 * Add extra params for request. If it is a GET request, the params will be
	 * appended to URL, otherwise will be send in the body of request.
	 *
	 * @param string $name Name of parameter
	 * @param mixed $value Param value
	 */
	public function addParam($name, $value) {
		$this->params[$name] = $value;
	}

	/**
	 * Instead of calling addParam() multiple times, this method receives an
	 * associative array with param_name => param_value [same as call
	 * addParam(param_name, param_value)].
	 *
	 * @param array $params
	 * @see Acotel_API_GenericAPI::addParam
	 */
	public function setParams(array $params) {
		foreach ($params as $name => $value) {
			$this->addParam($name, $value);
		}

	}

	/**
	 * Build URL of request using the specified params. Throws
	 * BadMethodCallException exception if URL wasn't sepecified.
	 *
	 * @return string URL of request
	 * @throws BadMethodCallException
	 */
	protected function buildURL() {
		if (!$this->url) {
			throw new BadMethodCallException("API URL not defined.");
		}

		$url = $this->url;

		if ($this->params) {
			$method = $this->request->getMethod();
			if ($method == HTTP_Request2::METHOD_POST) {
				$this->request->addPostParameter($this->params);
			} else {
				$url .= '?' . http_build_query($this->params);
			}
		}

		return $url;
	}

	/**
	 * Get API response according to specified $format. Current allowed formats
	 * are:
	 * <ul>
	 * <li>raw: returns the content as is.</li>
	 * <li>simplexml: parse the content and returns as SimpleXMLElement
	 *     object</li>
	 * <li>array: parse response (that must be received in XML format) and
	 *     returns as an array.</li>
	 * <li>json: treats response as a valid JSON string and parse it before
	 *     returning.</li>
	 * </ul>
	 *
	 * @param string $format Output format: raw, simplexml or array
	 * @return mixed Output according to $format param
	 * @throws Acotel_API_Exception
	 * @throws InvalidArgumentException
	 * @throws BadMethodCallException
	 */
	public function getResponse($format = 'raw') {
		$url = $this->buildURL();
		$this->request->setUrl($url);

		try {
			$response_obj = $this->request->send();
			$status = $response_obj->getStatus();
			if ($status != 200) {
				$msg = "$status " . $response_obj->getReasonPhrase();
				throw new Acotel_API_Exception($msg);
			}
		} catch (HTTP_Request2_Exception $e) {
			throw new Acotel_API_Exception($e->getMessage());
		}

		$response = $response_obj->getBody();

		if ($format == 'raw') {
			return $response;
		} else if ($format == 'simplexml') {
			return simplexml_load_string($response);
		} else if ($format == 'array') {
			return $this->transformToArray($response);
		} else if ($format == 'json') {
			return json_decode($response, true);
		} else {
			$msg = "Incorrect format specified [$format]";
			throw new InvalidArgumentException($msg);
		}
	}

	private function transformToArray($response) {
		$parser = new Acotel_Parser_XMLParser();
		return $parser->transformToArray($response);
	}
}
?>
