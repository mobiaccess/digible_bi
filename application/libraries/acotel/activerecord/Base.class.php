<?php
/**
 * @package Acotel
 * @subpackage ActiveRecord
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_ActiveRecord_Base {
	// opcoes da classe
	protected $options = array();
	protected $db_conn;

	private $properties = array();
	private $values = array();
	private $modified_attrs = array();

	private static $logger;

	public function __construct() {
		$this->connectDB();
	}

	public static function setLogger(Acotel_Logger_Logger $logger) {
		self::$logger = $logger;
	}

	protected function set_value($n, $v) {
		if (!$this->has_property($n)) {
			$msg = "Invalid property $n.";
			throw new Acotel_ActiveRecord_Exceptions_InvalidAttributeException($msg);
		}

		$this->values[$n] = $v;
		$this->modified_attrs[] = $n;
	}

	protected function get_value($n) {
		if (array_key_exists($n, $this->values)) {
			return $this->values[$n];
		} else {
			return null;
		}
	}

	public function __set($n, $v) {
		return $this->set_value($n, $v);
	}

	public function __get($n) {
		return $this->get_value($n);
	}

	protected function add_property($name, array $params = array()) {
		if (!isset($params['db_column'])) {
			$params['db_column'] = $name;
		}

		$this->properties[$name] = $params;
	}

	protected function has_property($name) {
		return array_key_exists($name, $this->properties);
	}

	public function save() {
		// insere ou atualiza o objeto no banco
		// dependendo do id
		$id = $this->get_value('id');
		if (!$id) {
			$this->insert();
		} else {
			$this->update();
		}
	}

	private function insert() {
		$fields = array();
		$values = array();
		foreach ($this->properties as $name => $info) {
			if ($name == 'id') {
				continue;
			}

			$fields[] = $info['db_column'];
			$values[] = $this->get_value($name);
		}

		$sql = "INSERT INTO " . $this->options['table_name'] . " ("
			. implode(',', $fields)
			. ") VALUES ("
			. implode(',', array_fill(0, count($fields), '?'))
			. ") RETURNING "
			. $this->properties['id']['db_column'];

		$stm = $this->db_conn->prepare($sql);

		try {
			$stm->execute($values);
			self::logSQL('success', $sql, $params);
			$row = $stm->fetch();
			$this->values['id'] = $row[0];
		} catch (PDOException $e) {
			$msg = (string) $e->getMessage();
			$code = (int) $e->getCode();
			self::logSQL('error', $sql, $params, $msg);
		}
	}

	private function update() {
		// nada modificado
		if (!$this->modified_attrs) {
			return;
		}

		$this->modified_attrs = array_unique($this->modified_attrs);

		$sql = "UPDATE " . $this->options['table_name'] . " SET ";
		$values = array();
		foreach ($this->modified_attrs as $name) {
			$db_column = $this->properties[$name]['db_column'];
			$sql .= "$db_column = ?,";
			$values[] = $this->get_value($name);
		}

		$sql = substr($sql, 0, -1) . " WHERE " . $this->properties['id']['db_column'] . " = ?";
		$values[] = $this->get_value('id');

		$stm = $this->db_conn->prepare($sql);

		$this->modified_attrs = array();

		try {
			$stm->execute($values);
			self::logSQL('success', $sql, $values);
		} catch (PDOException $e) {
			$msg = (string) $e->getMessage();
			$code = (int) $e->getCode();
			self::logSQL('error', $sql, $values, $msg);
		}
	}

	public function delete() {
		// apaga o proprio objeto do banco
		$field_name = $this->properties['id']['db_column'];

		$sql = "DELETE FROM " . $this->options['table_name'] . " WHERE "
			. "$field_name = ?";
		$stm = $this->db_conn->prepare($sql);

		$stm->execute(array($this->get_value('id')));
		$this->values['id'] = null;
	}

	/**
	 * @throws Acotel_ActiveRecord_Exceptions_InvalidAttributeException
	 * @throws PDOException
	 */
	public static function destroy(array $attributes) {
		// apaga os dados do banco
		// o $attributes e' usado no where (com AND)
		// retorna true ou false
		$sql = "DELETE FROM " . self::$options['table_name'] . " WHERE ";
		$params = array();
		foreach ($attributes as $name => $value) {
			if (!$this->has_property($name)) {
				$msg = "Invalid property $name.";
				throw new Acotel_ActiveRecord_Exceptions_InvalidAttributeException($msg);
			}

			$db_column = $this->properties[$name];
			$sql .= " $db_column = :$db_column AND";
			$params[':' . $db_column] = $value;
		}

		$sql = substr($sql, 0, -3);

		$stm = $this->db_conn->prepare($sql);

		try {
			$stm->execute($params);
			self::logSQL('success', $sql, $params);
		} catch (PDOException $e) {
			$msg = (string) $e->getMessage();
			$code = (int) $e->getCode();
			self::logSQL('error', $sql, $params, $msg);
			#throw new Acotel_Database_Exceptions_SQLException($msg, $code);
		}
	}

	protected function connectDB() {
		if ($this->db_conn !== null) {
			return;
		}

		if (!$this->options) {
			throw new LogicException('Missing class configuration.');
		}

		$manager = Acotel_Database_Connection_DatabaseConnectionManager::getInstance();
		$this->db_conn = $manager->getConnection($this->options['config']);
	}

	private function getPropertiesByDBColumn() {
		$list = array();
		foreach ($this->properties as $name => $info) {
			$list[$info['db_column']] = $name;
		}
		return $list;
	}

	protected function fillWithDBData($row) {
		$props = $this->getPropertiesByDBColumn();
		foreach ($row as $db_column => $value) {
			if (isset($props[$db_column])) {
				$this->values[$props[$db_column]] = $value;
			}
		}
	}

	protected function fill(array $attrs) {
		foreach ($attrs as $attr => $value) {
			if (isset($this->properties[$attr])) {
				$this->values[$attr] = $value;
			}
		}
	}

	/**
	 * Remove \t and \n used for indentation from SQL.
	 *
	 * @param string $sql
	 * @return string Query cleaned
	 */
	protected static function reformatSQL($sql) {
		$lines = explode("\n", $sql);
		$new_line = "";
		foreach ($lines as $line) {
			$new_sql .= ' ' . ltrim($line);
		}
		return trim($new_sql, "\n");
	}


	protected static function logSQL($type, $sql, $params, $msg = null) {
		$event = array(
			'data' => array(
				'sql' => self::reformatSQL($sql),
				'params' => $params,
			),
		);

		if ($type === 'success') {
			$event['name'] = 'sql-execution-successful';
			self::$logger->debug(json_encode($event));
		} elseif ($type === 'warn') {
			$event['name'] = 'sql-execution-error';
			$event['data']['error_msg'] = $msg;
			self::$logger->warn(json_encode($event));
		} else {
			$event['name'] = 'sql-execution-error';
			$event['data']['error_msg'] = $msg;
			self::$logger->error(json_encode($event));
		}
	}
}
?>
