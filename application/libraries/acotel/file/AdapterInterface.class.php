<?php
/**
 * Adapter interface for Acotel_File_Adapter_*
 *
 * @package Acotel
 * @subpackage File
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Adapter interface for Acotel_File_Adapter_*
 *
 * @package Acotel
 * @subpackage File
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
interface Acotel_File_AdapterInterface {
	function copy($origin, $destination);
}
?>