<?php
/**
 *
 * @package Acotel
 * @subpackage File
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 *
 * @package Acotel
 * @subpackage File
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_File_Adapter_SSH implements Acotel_File_AdapterInterface {
	private $conn;
	private $sftp_handler;
	private $options = array(
		'port' => 22,
	);

	public function __construct(array $options) {
		$this->options = array_merge($this->options, $options);
		$this->connect();
	}

	public function __destruct() {
		$this->closeConnection();
	}

	/**
	 * Copy file to $destination on server
	 *
	 * @param string $origin
	 * @param string $destination
	 */
	public function copy($origin, $destination) {
		$fd_origin = fopen($origin, 'r');
		$fd_dest = fopen("ssh2.sftp://{$this->sftp_handler}$destination", 'w');

		while (!feof($fd_origin)) {
			$data = fread($fd_origin, 8192);
			fwrite($fd_dest, $data);
		}

		fclose($fd_dest);
		fclose($fd_origin);

		return true;
	}

	private function connect() {
		$host = $this->options['host'];
		$port = $this->options['port'];
		$username = $this->options['username'];
		$password = $this->options['password'];

		$this->conn = ssh2_connect($host, $port);
		if ($this->conn) {
			ssh2_auth_password($this->conn, $username, $password);
			$this->sftp_handler = ssh2_sftp($this->conn);
		}
	}

	private function closeConnection() {
		if ($this->conn) {
			ssh2_exec($this->conn, 'exit');
		}
	}
}
?>