<?php
/**
 *
 * @package Acotel
 * @subpackage File
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 *
 * @package Acotel
 * @subpackage File
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 * @see Acotel_File_AdapterInterface
 */
class Acotel_File_Manager {
	private $adapter;

	/**
	 * Sets which adapter will be used to access the files.
	 *
	 * @param string $adapter Class that implements Acotel_File_AdapterInterface
	 * @param array $options Options passed to adapter
	 */
	public function setAdapter($adapter, array $options = null) {
		$this->adapter = new $adapter($options);
	}

	public function copy($origin, $destination) {
		return $this->adapter->copy($origin, $destination);
	}
}
?>