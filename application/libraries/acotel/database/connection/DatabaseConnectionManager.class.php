<?php
/**
 * Database Connection Manager
 *
 * @package Acotel
 * @subpackage Database
 * @author Thiago Costa <tcosta>
 */

/**
 * Database Connection Manager
 *
 * @package Acotel
 * @subpackage Database
 * @author Thiago Costa <tcosta>
 */
class Acotel_Database_Connection_DatabaseConnectionManager {

	private static $instance;
	private $listOfActiveConnections = array();

	private function __construct(){
	}

	/**
	 * Get an existing instance (create a new one if necessary)
	 *
	 * @return Acotel_Database_Connection_DatabaseConnectionManager
	 */
	public static function getInstance(){
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Retrieve database config from configuration array
	 *
	 * @param string $ident Database identifier
	 * @return array Configuration array
	 * @throws Acotel_Exceptions_ConfigurationNotFound
	 * @throws Acotel_Exceptions_IdentNotFound
	 */
	private function getDatabaseConfig($ident){
		$database = Acotel_Config::get('database');
		if (!$database) {
			throw new Acotel_Exceptions_ConfigurationNotFoundException();
		}

		if (!isset($database[$ident])) {
			throw new Acotel_Exceptions_IdentNotFoundException();
		}

		return $database[$ident];
	}

	/**
	 * Add new created connection to the active connections list
	 *
	 * @param string $ident Database identifier
	 * @param Acotel_Database_Connection_DatabaseConnection $databaseConnection
	 */
	private function addConnectionToList($ident,
		Acotel_Database_Connection_DatabaseConnection $databaseConnection){
		$this->listOfActiveConnections[$ident] = $databaseConnection;
	}

	/**
	 * Return existing active connection
	 *
	 * @param string $ident Database identifier
	 * @return Acotel_Database_Connection_DatabaseConnection
	 */
	private function getActiveConnection($ident){
		return (isset($this->listOfActiveConnections[$ident]))
			? $this->listOfActiveConnections[$ident] : null;
	}

	private function parseIdent($ident){
		return trim($ident);
	}

	private function createNewDatabaseConnection($ident){
		$config = $this->getDatabaseConfig($ident);
		$driver = $config['driver'];
		$DatabaseConnection = new Acotel_Database_Connection_DatabaseConnection($driver, $config);
		$this->addConnectionToList($ident, $DatabaseConnection);
	}

	/**
	 * Return an existing connection if any or create a new one
	 *
	 * @param string $ident Database identifier
	 * @return PDO
	 */
	public function getConnection($ident){
		$ident = $this->parseIdent($ident);

		if (!$this->getActiveConnection($ident)) {
			$this->createNewDatabaseConnection($ident);
		}

		return $this->getActiveConnection($ident)->getPDO();
	}

	/**
	 * Get list of active connections created
	 *
	 * @return array
	 */
	public function getListOfActiveConnections(){
		return $this->listOfActiveConnections;
	}

	/**
	 * Resets the instance, forcing the creation of new object on
	 * getInstance(). Must be used on unit tests.
	 */
	public static function reset() {
		self::$instance = null;
	}
}
