<?php
/**
 * Database Connection
 *
 * @package Acotel
 * @subpackage Database
 * @author Thiago Costa <tcosta>
 */

/**
 * Database Connection
 *
 * @package Acotel
 * @subpackage Database
 * @author Thiago Costa <tcosta>
 */
class Acotel_Database_Connection_DatabaseConnection {
	private $driver;
	private $options;
	private $PDO;

	/**
	 * Set parameters and connect to the database. Currently supported drivers
	 * are 'pgsql' (for PostgreSQL) and 'sqlite' (used for tests).
	 *
	 * @param string $driver 'pgsql' or 'sqlite'
	 * @param array $options Options specific to each driver
	 * @link http://php.net/pdo-pgsql.connection
	 * @link http://php.net/pdo-sqlite.connection
	 */
	public function __construct($driver, array $options){
		$this->driver = $driver;
		$this->options = $options;

		$this->connect();
	}

	/**
	 * Establish a database connection using the specified options passed to
	 * contructor
	 */
	private function connect() {
		$dsn = $this->buildDSN();
		$user = isset($this->options['user']) ? $this->options['user'] : null;
		$pass = isset($this->options['pass']) ? $this->options['pass'] : null;
		$PDO = new PDO($dsn, $user, $pass);
		$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->PDO = $PDO;
	}

	/**
	 * Builds the Data Source Name (DSN) for specified driver.
	 */
	private function buildDSN() {
		switch ($this->driver) {
		case 'pgsql':
			return 'pgsql:host=' . $this->options['host'] . ';port='
				. $this->options['port'] . ';dbname=' . $this->options['dbname'];
		case 'sqlite':
			return 'sqlite:' . $this->options['file'];
		}
	}

	/**
	 * Retrieve PDO object
	 *
	 */
	public function getPDO(){
		return $this->PDO;
	}
}
