<?php
/**
 * Value Object
 *
 * @package Acotel
 * @subpackage Database
 * @author Thiago Costa <tcosta>
 */

/**
 * Value Object
 *
 * @package Acotel
 * @subpackage Database
 * @author Thiago Costa <tcosta>
 */
class Acotel_Database_Vo_Vo {
	public function __set($key, $val) {
		$k = 'set' . ucwords(str_replace('_', '', $key));
		if (method_exists($this, $k)) {
			$this->$k($val);
		}
	}

}
?>