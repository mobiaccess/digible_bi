<?php
/**
 * Exception thrown when an error ocurred executing the query.
 *
 * @package Acotel
 * @subpackage Database
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 *
 */

/**
 * Exception thrown when an error ocurred executing the query.
 *
 * @package Acotel
 * @subpackage Database
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 *
 */
class Acotel_Database_Exceptions_SQLException extends Exception {
}
?>