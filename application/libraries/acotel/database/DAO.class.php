<?php
/**
 * Data Access Objects
 *
 * @package Acotel
 * @subpackage Database
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Base class for Data Access Objects
 *
 * @package Acotel
 * @subpackage Database
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
abstract class Acotel_Database_DAO implements SplSubject {
	/**
	 * Database connection manager instance
	 * @var Acotel_Database_Connection_DatabaseConnectionManager
	 */
	protected $manager;

	/**
	 * Observers listening to events
	 * @var SplObjectStorage
	 */
	private $observers;

	private $cachedStatements = array();

	public function __construct() {
		$this->manager = Acotel_Database_Connection_DatabaseConnectionManager::getInstance();
		$this->observers = new SplObjectStorage();
	}

	/**
	 * Executes the query and returns the PDOStatement object. If a PDOException
	 * is thrown, then it will be catched and a
	 * Acotel_Database_Exceptions_SQLException will be thrown instead, with
	 * same message and code.
	 *
	 * @param string $connIdent Connection identifier
	 * @param string|PDOStatement $sql Query to be executed
	 * @param array $params Parameters that will be passed to
	 *                      {@link PDOStatement::execute()} method
	 * @throws Acotel_Database_Exceptions_SQLException
	 */
	protected function executeQuery($connIdent, $sql, array $params = null, $cache = false) {
		$dbConn = $this->manager->getConnection($connIdent);

		if (isset($this->cachedStatements[$sql])) {
			$stm = $this->cachedStatements[$sql];
		} else {
			$stm = $dbConn->prepare($sql);
			if ($cache === true) {
				$this->cachedStatements[$sql] = $stm;
			}
		}

		try {
			$stm->execute($params);
			$this->logSQL('success', $sql, $params);
		} catch (PDOException $e) {
			$msg = (string) $e->getMessage();
			$code = (int) $e->getCode();
			$this->logSQL('error', $sql, $params, $msg);
			throw new Acotel_Database_Exceptions_SQLException($msg, $code);
		}

		return $stm;
	}

	protected function logSQL($type, $sql, $params, $msg = null) {
		$event = array(
			'data' => array(
				'sql' => $this->reformatSQL($sql),
				'params' => $params,
			),
		);

		if ($type === 'success') {
			$event['name'] = 'sql-execution-successful';
			$event['type'] = 'info';
		} elseif ($type === 'warn') {
			$event['name'] = 'sql-execution-error';
			$event['type'] = 'warn';
			$event['data']['error_msg'] = $msg;
		} else {
			$event['name'] = 'sql-execution-error';
			$event['type'] = 'error';
			$event['data']['error_msg'] = $msg;
		}

		$this->notify($event);
	}

	/**
	 * Remove \t and \n used for indentation from SQL.
	 *
	 * @param string $sql
	 * @return string Query cleaned
	 */
	protected function reformatSQL($sql) {
		$lines = explode("\n", $sql);
		$new_sql = "";
		foreach ($lines as $line) {
			$new_sql .= ' ' . ltrim($line);
		}
		return trim($new_sql, "\n");
	}

	/**
	 * Attach an observer to DAO
	 *
	 * @param SplObserver $obj
	 */
	public function attach(SplObserver $obj) {
		$this->observers->attach($obj);
	}

	/**
	 * Detach an observer to DAO
	 *
	 * @param SplObserver $obj
	 */
	public function detach(SplObserver $obj) {
		$this->observers->detach($obj);
	}

	/**
	 * Notify all observers when $event ocurred
	 *
	 * @param mixed $event
	 */
	public function notify($event = null) {
		foreach ($this->observers as $obj) {
			$obj->update($this, $event);
		}
	}

	/**
	 * Inserts object on database
	 *
	 * @param Acotel_Database_Vo_Vo $obj
	 */
	public function insert(Acotel_Database_Vo_Vo $obj) {
		throw new RuntimeException('Method insert not implemented');
	}

	/**
	 * Updates object on database
	 *
	 * @param Acotel_Database_Vo_Vo $obj
	 */
	public function update(Acotel_Database_Vo_Vo $obj) {
		throw new RuntimeException('Method update not implemented');
	}

	/**
	 * Delete object from database
	 *
	 * @param Acotel_Database_Vo_Vo $obj
	 */
	public function delete(Acotel_Database_Vo_Vo $obj) {
		throw new RuntimeException('Method delete not implemented');
	}
}
?>
