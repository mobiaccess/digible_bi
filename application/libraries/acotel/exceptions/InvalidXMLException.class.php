<?php
/**
 * Exception throw when an invalid XML was received
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 *
 */

/**
 * Exception throw when an invalid XML was received
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 *
 */
class Acotel_Exceptions_InvalidXMLException extends Exception {
}
?>
