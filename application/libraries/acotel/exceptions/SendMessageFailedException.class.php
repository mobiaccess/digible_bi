<?php
/**
 * Exception throw if an error occurs when sending a message
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Thiago Costa <tcosta>
 *
 */

/**
 * Exception throw if an error occurs when sending a message
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Thiago Costa <tcosta>
 *
 */
class Acotel_Exceptions_SendMessageFailedException extends Exception {
}
?>