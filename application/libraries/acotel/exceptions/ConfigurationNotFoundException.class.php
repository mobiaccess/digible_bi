<?php
/**
 * Exception throw when specific configuration isn't defined.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Thiago Costa <tcosta>
 */

/**
 * Exception throw when specific configuration isn't defined.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Thiago Costa <tcosta>
 */
class Acotel_Exceptions_ConfigurationNotFoundException extends Exception {
}
?>