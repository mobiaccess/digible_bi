<?php
/**
 * Exception throw when a file isn't found.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Exception throw when a file isn't found.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_Exceptions_FileNotFoundException extends RuntimeException {
}
?>
