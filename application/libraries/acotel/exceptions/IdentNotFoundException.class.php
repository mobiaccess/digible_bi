<?php
/**
 * Exception throw when a required Ident option isn't specified.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Thiago Costa <tcosta>
 */

/**
 * Exception throw when a required Ident option isn't specified.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Thiago Costa <tcosta>
 */
class Acotel_Exceptions_IdentNotFoundException extends Exception {
}
?>