<?php
/**
 * Exception throw when an invalid configuration option is passed to an object.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 *
 */

/**
 * Exception throw when an invalid configuration option is passed to an object.
 *
 * @package Acotel
 * @subpackage Exceptions
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 *
 */
class Acotel_Exceptions_InvalidConfigurationException extends Exception {
}
?>
