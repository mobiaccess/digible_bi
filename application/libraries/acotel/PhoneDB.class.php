<?php
class Acotel_PhoneDB {
	/**
	 * Config array. Allowed keys are 'url' (required), 'account' (required)
	 * and 'module' (optional)
	 * @var array
	 */
	private $config;

	/**
	 * Device ID
	 * @var int
	 */
	private $device_id;

	/**
	 * Feature list for specific device
	 * @var array
	 */
	private $features;

	/**
	 * Constructor
	 *
	 * @param string|int $device_info Device ID or user agent string
	 * @param array $config Configuration array
	 * @throws InvalidArgumentException
	 */
	public function __construct($device_info, array $config) {
		$this->config = $config;

		if (!is_numeric($device_info)) {
			try {
				$this->device_id = $this->getIDDeviceFromUserAgent($user_agent);
			} catch (UnexpectedValueException $e) {
				throw new InvalidArgumentException($e->getMessage());
			}
		} else {
			$this->device_id = (int) $device_info;
		}
	}

	/**
	 * Returns the device ID using the user-agent to identify the cell phone.
	 *
	 * @var string $user_agent User agetn string
	 * @return int Device ID
	 * @throws UnexpectedValueException
	 */
	public function getIDDeviceFromUserAgent($user_agent) {
		$url = $this->config['url'] . '?Account=' . $this->config['account'] .
			'&Action=IDDev&UA=' . urlencode($user_agent);
		$response = $this->execute($url);

		$xml = new XMLReader();
		$xml->xml($string, null, LBXML_COMPACT & LIBXML_NOBLANKS);

		$device_id = null;

		while ($xml->read()) {
			if ($xml == 'DEVICE' && $xml->nodeType = XMLReader::ELEMENT) {
				$device_id = $xml->getAttribute('id');
				break;
			}
		}

		if (!$device_id || $device_id == -1 || $device_id == -2) {
			$msg = "Device ID not found User-Agent string [$user_agent]";
			throw new UnexpectedValueException($msg);
		}

		return $device_id;
	}

	/**
	 *
	 * @return array Feature list
	 */
	public function getFeatures() {
		// as features ja foram carregadas
		if ($this->features) {
			return $this->features;
		}

		$id_device = $this->device_id;

		$url = $this->config['url'] . '?Account=' . $this->config['account'] . '&Action=Fea&Mod=' . $this->config['module'] . '&IdDev=' . $id_device;
		$xml_string = $this->execute($url);

		$this->features = array();

		$xml = new XMLReader();
		$xml->xml($xml_string, null, LIBXML_COMPACT & LIBXML_NOBLANKS);
		while ($xml->read()) {
			if ($xml->name == 'NAME' && $xml->nodeType == XMLReader::ELEMENT) {
				// avança para pegar o nome da feature
				$xml->read();
				$feature_name = $xml->value;

				while ($xml->read()) {
					if ($xml->name == 'VALUE' && $xml->nodeType == XMLReader::ELEMENT) {
						// avança para pegar o valor
						$xml->read();
						$this->features[$feature_name] = $xml->value;
						break;
					}
				}
			}
		}

		return $this->features;
	}

	/**
	 * Returns the value for feature specified in $name. If value is "yes" or
	 * "no" then the method returns TRUE or FALSE. If the specified feature
	 * doesn't exists then returns NULL.
	 *
	 * @var string $name Name of the feature
	 * @return mixed Feature value or bool if value is "yes" or "no"
	 */
	public function getFeature($name) {
		$features = $this->getFeatures();
		if (!array_key_exists($name, $features)) {
			return null;
		}

		$feat = $features[$name];
		if ($feat == 'yes') {
			return true;
		} elseif ($feat == 'no') {
			return false;
		} else {
			return $feat;
		}
	}

	public function getScreenSize() {
		$id_device = $this->device_id;

		$features = $this->getFeatures();
		if (!$features) {
			return null;
		}

		return array(
			'USABLEHEIGHT' => $features['USABLEHEIGHT'],
			'USABLEWIDTH' => $features['USABLEWIDTH'],
			'HEIGHT' => $features['HEIGHT'],
			'WIDTH' => $features['WIDTH'],
		);
	}

	private function execute($url){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_HEADER, false);

		$res = curl_exec($ch);
		curl_close($ch);

		return trim($res);
	}
}
?>
