<?php
class Acotel_View {
	private $_cfg_name;
	private $_tpl;
	private $_vars = array();

	public function __construct($config) {
		$this->_cfg_name = $config;
	}

	public function __set($n, $v) {
		$this->_vars[$n] = $v;
	}

	public function __get($n) {
		return $this->_vars[$n];
	}

	public function set_template($tpl) {
		$this->_tpl = $tpl;
	}

	public function render() {
		if ($this->_tpl) {
			$tpl_dir = Acotel_Config::get($this->_cfg_name);
			// So ativa o output buffer se ja nao estiver ativado
			if (!ob_list_handlers()) {
				ob_start();
			}
			include $tpl_dir . '/' . $this->_tpl;
			return ob_get_clean();
		}
	}
}
?>
