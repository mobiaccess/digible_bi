<?php

//--------------------------------------------------------------------------------------------
// AcoMMDB Class
//
// Version 1.6 - 18/12/2006
//   Add GetCategoryQat Method
//
// Version 1.7 - 12/01/2007
//   Add GetSectorsQat Method
//
// Version 1.8 - 23/01/2007
//   Update FindIndexQat Method
//   Add GetIndexQat Method
//
// Version 1.9 - 29/01/2007
//   Correct 2 bugs (write performante and GetObjectQat call)
//
// Version 1.10 - 22/02/2007
//   Add GetReccomentationsQat
//
// Version 1.11 - 27/02/2007
//   Add New parametro to GetIndexQat
//   Add SearchKeysQat method
//
// Version 1.12 - 02/03/2007
//   Correct a Bug
//   Add SearchIndexQat method
//
// Version 1.13 - 12/03/2007
//   Add some parameters and correct bugs
//
// Version 1.14 - 17/07/2007
//   Add json output into WritePerformance
//   Add parameter ExcludeParam to GetCategoryQat
//
//--------------------------------------------------------------------------------------------

class Acotel_MMDB_ApiCollection {

	private $UrlSite;
	private $XmlFolder;
	private $PathCache;
	private $PathPerformance;
	private $Version = "4.1a";
	private $Url;

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function __construct($UrlSite, $XmlFolder, $PathCache=null, $PathPerformance=null){ 
    $this->UrlSite = $UrlSite; 
    $this->XmlFolder = $XmlFolder; 
    $this->PathCache = $PathCache; 
    $this->PathPerformance = $PathPerformance; 
  } 

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetUrl(){
    return ($this->Url);
  }  

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  private function CompleteUrl($Url){
    $Pos = strpos ($Url, "?");
    $Pos++;
    $Params = substr($Url, $Pos, strlen($Url) - $Pos);
    //echo "Params=".$Params."<br>";  
  
    $TabParams = explode("&", $Params);
    //print_r($TabParams);
    
    $TabFieldsValues = array();
    for ($Param=0; $Param<count($TabParams); $Param++)
    {
      $FieldValue = explode("=", $TabParams[$Param]);
      $TabFieldsValues[$FieldValue[0]] = $FieldValue[1];  	
    }
  
    //print_r($TabFieldsValues);
    
    $Pos = strpos ($Url, "GetDevice.php");
    if ($Pos > 0)
    {
      $TabParamIn = $this->GetDevice($TabFieldsValues, FALSE);	
    }

    $Pos = strpos ($Url, "GetObject.php");
    if ($Pos > 0)
    {
      $TabParamIn = $this->GetObject($TabFieldsValues, FALSE);	
    }
    
    //print_r($TabParamIn);
      
    $Url = $this->GetUrl(); 	
    return $Url;	
  }	

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function SearchIndexQat($TabParamIn, $Execute = TRUE){
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
      
    $TabParamOut["IDQat"]     = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]       = $TabParamIn["Qat"];
    $TabParamOut["Operator"]  = $TabParamIn["Operator"];
    $TabParamOut["Fields"]    = $TabParamIn["Fields"];
    $TabParamOut["FreeKeys"]  = $TabParamIn["FreeKeys"];
    $TabParamOut["FixedKeys"] = $TabParamIn["FixedKeys"];
    $TabParamOut["Groupby"]   = $TabParamIn["Groupby"];
    $TabParamOut["IDSectors"] = $TabParamIn["IDSectors"];
    $TabParamOut["Device"]    = $TabParamIn["Device"];
    $TabParamOut["RecordMin"] = $TabParamIn["RecordMin"];
    $TabParamOut["RecordMax"] = $TabParamIn["RecordMax"];
    
    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
        
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }
    
    if ($TabParamIn["New"] == "Yes")
    {
      $TabParamOut["New"] = "Yes";	
    }
    else
    {
      $TabParamOut["New"] = "No";	
    }
    
    $Cache = "No";	
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("SearchIndexQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function SearchKeysQat($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
      
    $TabParamOut["IDQat"]        = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]          = $TabParamIn["Qat"];
    $TabParamOut["Operator"]     = $TabParamIn["Operator"];
    $TabParamOut["IDSectors"]    = $TabParamIn["IDSectors"];
    $TabParamOut["IDFieldKey"]   = $TabParamIn["IDFieldKey"];
    $TabParamOut["KeysFieldKey"] = $TabParamIn["KeysFieldKey"];
    $TabParamOut["IDFields"]     = $TabParamIn["IDFields"];
    //$TabParamOut["Device"]       = $TabParamIn["Device"];
    $TabParamOut["RecordMin"]    = $TabParamIn["RecordMin"];
    $TabParamOut["RecordMax"]    = $TabParamIn["RecordMax"];
    
    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
        
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }
    
    if ($TabParamIn["New"] == "Yes")
    {
      $TabParamOut["New"] = "Yes";	
    }
    else
    {
      $TabParamOut["New"] = "No";	
    }
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("SearchKeysQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetReccomendationsQat($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["IDQat"]       = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]         = $TabParamIn["Qat"];
    $TabParamOut["Operator"]    = $TabParamIn["Operator"];
    $TabParamOut["IDSector"]    = $TabParamIn["IDSector"];
    $TabParamOut["IDCategory"]  = $TabParamIn["IDCategory"];
    $TabParamOut["IDIndexPubl"] = $TabParamIn["IDIndexPubl"];
    $TabParamOut["User"]        = $TabParamIn["User"];
       
    //--------------------------------------------------
    // Set Default Parameters GetIndex
    //--------------------------------------------------
 
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
 

  
    $this->Url = $this->CreateUrl("GetReccomendationsQat.php", $TabParamOut);      
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
      
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetObjectQat($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["IDQat"]       = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]         = $TabParamIn["Qat"];
    $TabParamOut["Operator"]    = $TabParamIn["Operator"];
    $TabParamOut["IDIndexPubl"] = $TabParamIn["IDIndexPubl"];
    $TabParamOut["Device"]      = $TabParamIn["Device"];
    $TabParamOut["IDFields"]    = $TabParamIn["IDFields"];
    $TabParamOut["ImageResize"] = $TabParamIn["ImageResize"];
    $TabParamOut["Progresses"]  = $TabParamIn["Progresses"];
    
    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
    
    if (strlen($TabParamIn["IDPriceBilling"]) == 0)
    {
      $TabParamOut["IDPriceBilling"] = 2;	// Default SMS Premium
    }
    else
    {
      $TabParamOut["IDPriceBilling"] = $TabParamIn["IDPriceBilling"];	
    }

    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }
     
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("GetObjectQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetIndexQat($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["IDQat"]     = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]       = $TabParamIn["Qat"];
    $TabParamOut["Operator"]  = $TabParamIn["Operator"];
    $TabParamOut["Category"]  = $TabParamIn["Category"];
    $TabParamOut["Device"]    = $TabParamIn["Device"];
    $TabParamOut["RecordMin"] = $TabParamIn["RecordMin"];
    $TabParamOut["RecordMax"] = $TabParamIn["RecordMax"];
    $TabParamOut["IDFields"]  = $TabParamIn["IDFields"];
    
    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
             
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }

    if ($TabParamIn["New"] == "Yes")
    {
      $TabParamOut["New"] = "Yes";	
    }
    else
    {
      $TabParamOut["New"] = "No";	
    }
      
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("GetIndexQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetCategoryQat($TabParamIn, $Execute = TRUE)
  {
  	
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
   
    $TabParamOut["IDQat"]         = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]           = $TabParamIn["Qat"];
    $TabParamOut["Operator"]      = $TabParamIn["Operator"];
    $TabParamOut["Sectors"]       = $TabParamIn["Sectors"];
    $TabParamOut["Device"]        = $TabParamIn["Device"];
    $TabParamOut["ExcludeParam"]  = $TabParamIn["ExcludeParam"];
    
    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
            
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }
    
    if ($TabParamIn["Attributes"] == "All")
    {
      $TabParamOut["Attributes"] = "All";	
    }
    else
    {
      $TabParamOut["Attributes"] = "Main";	
    }
    
    if ($TabParamIn["Tree"] == "Multi")
    {
      $TabParamOut["Tree"] = "Multi";	
    }
    else
    {
      $TabParamOut["Tree"] = "Single";	
    }
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("GetCategoryQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------

      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------

      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;

    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetSectorsQat($TabParamIn, $Execute = TRUE)
  {
  	
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
   
    $TabParamOut["IDQat"]         = $TabParamIn["IDQat"];
    $TabParamOut["Qat"]           = $TabParamIn["Qat"];
    $TabParamOut["Operator"]      = $TabParamIn["Operator"];
    $TabParamOut["Service"]       = $TabParamIn["Service"];
    $TabParamOut["Device"]        = $TabParamIn["Device"];
    
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }

    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
    
     
       
    $this->Url = $this->CreateUrl("GetSectorsQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------

      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
     
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }
  
//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetStatistic($TabParamIn, $Execute = TRUE)
  {
     
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
   
    $TabParamOut["Operator"] = $TabParamIn["Operator"];
    $TabParamOut["Token"]    = $TabParamIn["Token"];
    $TabParamOut["Vas"]      = $TabParamIn["Vas"];
    
    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
            
    if ($TabParamIn["Name"] == "Yes")
    {
      $TabParamOut["Name"] = "Yes";	
    }
    else
    {
      $TabParamOut["Name"] = "No";	
    }
          
      
    $Cache = "No";	
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("GetStatistic.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Name"]  = $this->DecodeYesNo($TabParamOut["Name"]);
      $TabParamOut["Cache"] = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }  
                      
//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetDevice($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
 
    $TabParamOut["Operator"]      = $TabParamIn["Operator"];
    $TabParamOut["Service"]       = $TabParamIn["Service"];
    $TabParamOut["Sector"]        = $TabParamIn["Sector"];
    $TabParamOut["Device"]        = $TabParamIn["Device"];
    $TabParamOut["DeviceName"]    = $TabParamIn["DeviceName"];
    $TabParamOut["OperatorParam"] = $TabParamIn["OperatorParam"];
    $TabParamOut["ServiceParam"]  = $TabParamIn["ServiceParam"];
    $TabParamOut["SectorParam"]   = $TabParamIn["SectorParam"];
 
    //--------------------------------------------------
    // Set Default Parameters GetDevice
    //--------------------------------------------------
 
    if ($TabParamIn["DevicesList"] == "Yes")
    {
      $TabParamOut["DevicesList"] = "Yes";	
    }
    else
    {
      $TabParamOut["DevicesList"] = "No";		
    }
    
   
    if ($TabParamIn["Devices"] == "No")
    {
      $TabParamOut["Devices"] = "No";	
    }
    else
    {
      $TabParamOut["Devices"] = "Yes";	
    }
   
       
    if ($TabParamIn["Empty"] == "No")
    {
      $TabParamOut["Empty"] = "No";	
    }
    else
    {
      $TabParamOut["Empty"] = "Yes";	
    }
   
   
    switch($TabParamIn["Visible"])
    {
    case "No":
    case "Yes":
    case "Test":
      $TabParamOut["Visible"] = $TabParamIn["Visible"];
      break;
        
    default:
      $TabParamOut["Visible"] = "Yes";
    }
    
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;	
    
    


    //print_r ($TabParamout);     
    
    $this->Url = $this->CreateUrl("GetDevice.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters GetDevice
      //--------------------------------------------------
       
      $TabParamOut["DevicesList"] = $this->DecodeYesNo($TabParamOut["DevicesList"]); 	 
      $TabParamOut["Devices"]     = $this->DecodeYesNo($TabParamOut["Devices"]); 	
      $TabParamOut["Empty"]       = $this->DecodeYesNo($TabParamOut["Empty"]); 	
      $TabParamOut["Cache"]       = $this->DecodeYesNo($TabParamOut["Cache"]); 	
      
      $TabParamOut["Visible"]     = $this->DecodeVisible($TabParamOut["Visible"]); 	
      
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetObject($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["IDIndexPubl"] = $TabParamIn["Index"];
    if (strlen($TabParamOut["IDIndexPubl"]) == 0)
    {
      $TabParamOut["IDIndexPubl"] = $TabParamIn["IDIndexPubl"];
    }  
      
    $TabParamOut["IDObjectPubl"] = $TabParamIn["Object"];
    if (strlen($TabParamOut["IDObjectPubl"]) == 0)
    {
      $TabParamOut["IDObjectPubl"] = $TabParamIn["IDObjectPubl"];
    }  
    
    $TabParamOut["IDIndex"]     = $TabParamIn["IDIndex"];
    $TabParamOut["IDObject"]    = $TabParamIn["IDObject"];
    $TabParamOut["Code"]        = $TabParamIn["Code"];
    $TabParamOut["Operator"]    = $TabParamIn["Operator"];
    $TabParamOut["Area"]        = $TabParamIn["Area"];
    $TabParamOut["Sector"]      = $TabParamIn["Sector"];
    $TabParamOut["Group"]       = $TabParamIn["Group"];
    $TabParamOut["Device"]      = $TabParamIn["Device"];
    $TabParamOut["DeviceName"]  = $TabParamIn["DeviceName"];
    $TabParamOut["Category"]    = $TabParamIn["Category"];
    $TabParamOut["Progress"]    = $TabParamIn["Progress"];
    $TabParamOut["ImageResize"] = $TabParamIn["ImageResize"];
    $TabParamOut["ImageQuality"] = $TabParamIn["ImageQuality"];
    $TabParamOut["ImageFormat"] = $TabParamIn["ImageFormat"];
    $TabParamOut["Protection"]  = $TabParamIn["Protection"];

    //--------------------------------------------------
    // Set Default Parameters GetObject
    //--------------------------------------------------
   
    switch($TabParamIn["Xml"])
    {
    case "4.0":
    case "5.0":
      $TabParamOut["Xml"] = $TabParamIn["Xml"];	
      break;
      
    default:
      $TabParamOut["Xml"] = "3.0";	
      break;
    }
    
    
    if ($TabParamIn["Devices"] == "Yes")
    {
      $TabParamOut["Devices"] = "Yes";	
    }
    else
    {
      $TabParamOut["Devices"] = "No";	
    }
  
  
    if ($TabParamIn["Binary"] == "No")
    {
      $TabParamOut["Binary"] = "No";	
    }
    else
    {
      $TabParamOut["Binary"] = "Yes";
    }
  
  
    if ($TabParamIn["Fields"] == "Yes")
    {
      $TabParamOut["Fields"] = "Yes";	
    }
    else
    {
      $TabParamOut["Fields"] = "No";	
    }
  
  
    if ($TabParamIn["Prices"] == "Yes")
    {
      $TabParamOut["Prices"] = "Yes";	
    }
    else
    {
      $TabParamOut["Prices"] = "No";
    }
  
  
    if ($TabParamIn["Objects"] == "Yes")
    {
      $TabParamOut["Objects"] = "Yes";	
    }
    else
    {
      $TabParamOut["Objects"] = "No";	
    }
   
  
    if ($TabParamIn["Temp"] == "Yes")
    {
      $TabParamOut["Temp"] = "Yes";	
    }
    else
    {
      $TabParamOut["Temp"] = "No";	
    }
  
  
    if ($TabParamIn["Smil"] == "Yes")
    {
      $TabParamOut["Smil"]= "Yes";	
    }
    else
    {
      $TabParamOut["Smil"] = "No";	
    }
 
  
    if ($TabParamIn["Properties"] == "Yes")
    {
      $TabParamOut["Properties"] = "Yes";	
    }
    else
    {
      $TabParamOut["Properties"] = "No";	
    }
 
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;	

 

    //print_r ($TabParamOut);     
    //exit();
    
    
    $this->Url = $this->CreateUrl("GetObject.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
   
			# check cache ;
        $_arrFunc = array("FuncQueryType"=>"GetObject", "FuncCacheDisable"=>FUNC_CACHE_DISABLE);
        $_TabParamIn = array_merge($TabParamIn,$_arrFunc);
        $RetCache = $this->mmdb4_check_cache($_TabParamIn,$RetCacheXML);
        if($RetCache){
                return $RetCacheXML;
        } else {
		$time_start = microtime(true);
                $RetXML = $this->Execute($this->Url, $Account, $Source, $Cache);
		$time_end = microtime(true);
                # write the cache file ;
                if(stripos($RetXML, '<RESULT Status="OK">') && $TabParamIn["Cache"]!="Yes") {
                        $this->mmdb4_write_cache(array_merge($_TabParamIn,array("built_xml"=>$RetXML)));
                }
                return $RetXML;
        }
 
      //return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
  
      $TabParamOut["Devices"]    = $this->DecodeYesNo($TabParamOut["Devices"]); 	
      $TabParamOut["Binary"]     = $this->DecodeYesNo($TabParamOut["Binary"]); 	
      $TabParamOut["Fields"]     = $this->DecodeYesNo($TabParamOut["Fields"]); 	
      $TabParamOut["Prices"]     = $this->DecodeYesNo($TabParamOut["Prices"]); 	
      $TabParamOut["Objects"]    = $this->DecodeYesNo($TabParamOut["Objects"]); 	
      $TabParamOut["Temp"]       = $this->DecodeYesNo($TabParamOut["Temp"]); 	
      $TabParamOut["Smil"]       = $this->DecodeYesNo($TabParamOut["Smil"]); 	
      $TabParamOut["Properties"] = $this->DecodeYesNo($TabParamOut["Properties"]); 	
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]); 	
      
      $TabParamOut["Protection"]      = $this->DecodeProtection($TabParamOut["Protection"]); 
     
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetCategory($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["Category"] = $TabParamIn["Category"];
    $TabParamOut["Sector"]   = $TabParamIn["Sector"];
    $TabParamOut["Group"]    = $TabParamIn["Group"];
    $TabParamOut["Device"]   = $TabParamIn["Device"];

    //--------------------------------------------------
    // Set Default Parameters GetCategory
    //--------------------------------------------------
    
    switch($TabParamIn["Xml"])
    {
    case "4.0":
    case "5.0":
      $TabParamOut["Xml"] = $TabParamIn["Xml"];	
      break;
      
    default:
      $TabParamOut["Xml"] = "3.0";	
      break;
    }
    
    
    if ($TabParamIn["Empty"] == "No")
    {
      $TabParamOut["Empty"] = "No";	
    }
    else
    {
      $TabParamOut["Empty"] = "Yes";	
    }
    
    
    switch($TabParamIn["Visible"])
    {
    case "No":
    case "Yes":
    case "Test":
      $TabParamOut["Visible"] = $TabParamIn["Visible"];
      break;
        
    default:
      $TabParamOut["Visible"] = "Yes";
    }
 
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;	

 

    //print_r ($TabParamOut);     
    //exit();
    
    
    $this->Url = $this->CreateUrl("GetCategory.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
			# check cache ;
        $_arrFunc = array("FuncQueryType"=>"GetCategories", "FuncCacheDisable"=>FUNC_CACHE_DISABLE);
        $_TabParamIn = array_merge($TabParamIn,$_arrFunc);
        $RetCache = $this->mmdb4_check_cache($_TabParamIn,$RetCacheXML);
        if($RetCache){
                return $RetCacheXML;
        } else {
		$time_start = microtime(true);
                $RetXML = $this->Execute($this->Url, $Account, $Source, $Cache);
		$time_end = microtime(true);
                # write the cache file ;
                if(stripos($RetXML, '<RESULT Status="OK">') && $TabParamIn["Cache"]!="Yes") {
                        $this->mmdb4_write_cache(array_merge($_TabParamIn,array("built_xml"=>$RetXML)));
                }
                return $RetXML;
        }

      //return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Empty"]   = $this->DecodeYesNo($TabParamOut["Empty"]);
      $TabParamOut["Cache"]   = $this->DecodeYesNo($TabParamOut["Cache"]);
      
      $TabParamOut["Visible"] = $this->DecodeVisible($TabParamOut["Visible"]);
        
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetIndex($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["Category"]          = $TabParamIn["Category"];
    $TabParamOut["Operator"]          = $TabParamIn["Operator"];
    $TabParamOut["Area"]              = $TabParamIn["Area"];
    $TabParamOut["Sector"]            = $TabParamIn["Sector"];
    $TabParamOut["Group"]             = $TabParamIn["Group"];
    $TabParamOut["Device"]            = $TabParamIn["Device"];
    $TabParamOut["ValidityDate"]      = $TabParamIn["ValidityDate"];
    $TabParamOut["ValidityDateStart"] = $TabParamIn["ValidityDateStart"];
    $TabParamOut["ValidityDateEnd"]   = $TabParamIn["ValidityDateEnd"];
    $TabParamOut["RecordMin"]         = $TabParamIn["RecordMin"];
    $TabParamOut["RecordMax"]         = $TabParamIn["RecordMax"];
    $TabParamOut["Progress"]          = $TabParamIn["Progress"];
    $TabParamOut["Protection"]        = $TabParamIn["Protection"];
    $TabParamOut["ImageResize"]       = $TabParamIn["ImageResize"];
    $TabParamOut["ImageQuality"]      = $TabParamIn["ImageQuality"];
    $TabParamOut["ImageFormat"]       = $TabParamIn["ImageFormat"];
    $TabParamOut["PreviewSector"]     = $TabParamIn["PreviewSector"];
    $TabParamOut["PreviewDevice"]     = $TabParamIn["PreviewDevice"];
    $TabParamOut["SupportedDevices"]  = $TabParamIn["SupportedDevices"];
 
    //--------------------------------------------------
    // Set Default Parameters GetIndex
    //--------------------------------------------------
    
    switch($TabParamIn["Xml"])
    {
    case "4.0":
    case "5.0":
      $TabParamOut["Xml"] = $TabParamIn["Xml"];	
      break;
      
    default:
      $TabParamOut["Xml"] = "3.0";	
      break;
    }
    
    if ($TabParamIn["Binary"] == "No")
    {
      $TabParamOut["Binary"] = "No";	
    }
    else
    {
      $TabParamOut["Binary"] = "Yes";	
    }
    
    
    if ($TabParamIn["Fields"] == "Yes")
    {
      $TabParamOut["Fields"] = "Yes";	
    }
    else
    {
      $TabParamOut["Fields"] = "No";	
    }
    
    
    if ($TabParamIn["Prices"] == "Yes")
    {
      $TabParamOut["Prices"] = "Yes";	
    }
    else
    {
      $TabParamOut["Prices"] = "No";	
    }
    
    
    if ($TabParamIn["Objects"] == "Yes")
    {
      $TabParamOut["Objects"] = "Yes";	
    }
    else
    {
      $TabParamOut["Objects"] = "No";	
    }
    
    
    if ($TabParamIn["Temp"] == "Yes")
    {
      $TabParamOut["Temp"] = "Yes";	
    }
    else
    {
      $TabParamOut["Temp"] = "No";	
    }
    
    
    if ($TabParamIn["Smil"] == "Yes")
    {
      $TabParamOut["Smil"] = "Yes";	
    }
    else
    {
      $TabParamOut["Smil"] = "No";	
    }
    
    
    if ($TabParamIn["Properties"] == "Yes")
    {
      $TabParamOut["Properties"] = "Yes";	
    }
    else
    {
      $TabParamOut["Properties"] = "No";	
    }
    
    
    if ($TabParamIn["Alphabet"] == "Yes")
    {
      $TabParamOut["Alphabet"] = "Yes";	
    }
    else
    {
      $TabParamOut["Alphabet"] = "No";	
    }
    
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
 
    //print_r ($TabParamOut);     
    //exit();
    
    
    $this->Url = $this->CreateUrl("GetIndex.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
			# check cache ;
        $_arrFunc = array("FuncQueryType"=>"GetObjects", "FuncCacheDisable"=>FUNC_CACHE_DISABLE);
        $_TabParamIn = array_merge($TabParamIn,$_arrFunc);
        $RetCache = $this->mmdb4_check_cache($_TabParamIn,$RetCacheXML);
	if($RetCache){
		return $RetCacheXML;
	} else {
		$time_start = microtime(true);
		$RetXML = $this->Execute($this->Url, $Account, $Source, $Cache);
		$time_end = microtime(true);
		# write the cache file ;
        	if(stripos($RetXML, '<RESULT Status="OK">') && $TabParamIn["Cache"]!="Yes") {
			$this->mmdb4_write_cache(array_merge($_TabParamIn,array("built_xml"=>$RetXML)));
        	}
		return $RetXML;
	}

      //return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Binary"]     = $this->DecodeYesNo($TabParamOut["Binary"]);
      $TabParamOut["Fields"]     = $this->DecodeYesNo($TabParamOut["Fields"]);
      $TabParamOut["Prices"]     = $this->DecodeYesNo($TabParamOut["Prices"]);
      $TabParamOut["Objects"]    = $this->DecodeYesNo($TabParamOut["Objects"]);
      $TabParamOut["Temp"]       = $this->DecodeYesNo($TabParamOut["Temp"]);
      $TabParamOut["Smil"]       = $this->DecodeYesNo($TabParamOut["Smil"]);
      $TabParamOut["Properties"] = $this->DecodeYesNo($TabParamOut["Properties"]);
      $TabParamOut["Alphabet"]   = $this->DecodeYesNo($TabParamOut["Alphabet"]);
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
      $TabParamOut["SupportedDevices"] = $this->DecodeYesNo($TabParamOut["SupportedDevices"]);

      $TabParamOut["Protection"] = $this->DecodeProtection($TabParamOut["Protection"]);       

      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function FindIndex($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
   
    $TabParamOut["Category"]          = $TabParamIn["Category"];
    $TabParamOut["Operator"]          = $TabParamIn["Operator"];
    $TabParamOut["Area"]              = $TabParamIn["Area"];
    $TabParamOut["Field"]             = $TabParamIn["Field"];
    $TabParamOut["Key"]               = $TabParamIn["Key"];
    $TabParamOut["Sector"]            = $TabParamIn["Sector"];
    $TabParamOut["Group"]             = $TabParamIn["Group"];
    $TabParamOut["Device"]            = $TabParamIn["Device"];
    $TabParamOut["RecordMin"]         = $TabParamIn["RecordMin"];
    $TabParamOut["RecordMax"]         = $TabParamIn["RecordMax"];
    $TabParamOut["Progress"]          = $TabParamIn["Progress"];
    $TabParamOut["Protection"]        = $TabParamIn["Protection"];
    $TabParamOut["ImageResize"]       = $TabParamIn["ImageResize"];
    $TabParamOut["ImageQuality"]      = $TabParamIn["ImageQuality"];
    $TabParamOut["ImageFormat"]       = $TabParamIn["ImageFormat"];
    $TabParamOut["PreviewSector"]     = $TabParamIn["PreviewSector"];
    $TabParamOut["PreviewDevice"]     = $TabParamIn["PreviewDevice"];
 
    //--------------------------------------------------
    // Set Default Parameters FindIndex
    //--------------------------------------------------
     
    switch($TabParamIn["Xml"])
    {
    case "4.0":
    case "5.0":
      $TabParamOut["Xml"] = $TabParamIn["Xml"];	
      break;
      
    default:
      $TabParamOut["Xml"] = "3.0";	
      break;
    }
    
    
    if ($TabParamIn["Binary"] == "No")
    {
      $TabParamOut["Binary"] = "No";	
    }
    else
    {
      $TabParamOut["Binary"] = "Yes";	
    }
    
    
    if ($TabParamIn["Fields"] == "Yes")
    {
      $TabParamOut["Fields"] = "Yes";	
    }
    else
    {
      $TabParamOut["Fields"] = "No";	
    }
    
    
    if ($TabParamIn["Prices"] == "Yes")
    {
      $TabParamOut["Prices"] = "Yes";	
    }
    else
    {
      $TabParamOut["Prices"] = "No";	
    }
    
    
    if ($TabParamIn["Objects"] == "Yes")
    {
      $TabParamOut["Objects"] = "Yes";	
    }
    else
    {
      $TabParamOut["Objects"] = "No";	
    }
    
    
    if ($TabParamIn["Temp"] == "Yes")
    {
      $TabParamOut["Temp"] = "Yes";	
    }
    else
    {
      $TabParamOut["Temp"] = "No";	
    }
    
    
    if ($TabParamIn["Smil"] == "Yes")
    {
      $TabParamOut["Smil"] = "Yes";	
    }
    else
    {
      $TabParamOut["Smil"] = "No";	
    }
    
    
    if ($TabParamIn["Properties"] == "Yes")
    {
      $TabParamOut["Properties"] = "Yes";	
    }
    else
    {
      $TabParamOut["Properties"] = "No";	
    }
    
    
    if ($TabParamIn["Alphabet"] == "Yes")
    {
      $TabParamOut["Alphabet"] = "Yes";	
    }
    else
    {
      $TabParamOut["Alphabet"] = "No";	
    }
    
    
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;
 
    //print_r ($TabParamOut);     
    //exit();
    
    
    $this->Url = $this->CreateUrl("FindIndex.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Binary"]     = $this->DecodeYesNo($TabParamOut["Binary"]);
      $TabParamOut["Fields"]     = $this->DecodeYesNo($TabParamOut["Fields"]);
      $TabParamOut["Prices"]     = $this->DecodeYesNo($TabParamOut["Prices"]);
      $TabParamOut["Objects"]    = $this->DecodeYesNo($TabParamOut["Objects"]);
      $TabParamOut["Temp"]       = $this->DecodeYesNo($TabParamOut["Temp"]);
      $TabParamOut["Smil"]       = $this->DecodeYesNo($TabParamOut["Smil"]);
      $TabParamOut["Properties"] = $this->DecodeYesNo($TabParamOut["Properties"]);
      $TabParamOut["Alphabet"]   = $this->DecodeYesNo($TabParamOut["Alphabet"]);
      $TabParamOut["Cache"]      = $this->DecodeYesNo($TabParamOut["Cache"]);
      
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function FindIndexQat($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
   
    $TabParamOut["Operator"]  = $TabParamIn["Operator"];
    $TabParamOut["Qat"]       = $TabParamIn["Qat"];
    $TabParamOut["Fields"]    = $TabParamIn["Fields"];
    $TabParamOut["FreeKeys"]  = $TabParamIn["FreeKeys"];
    $TabParamOut["FixedKeys"] = $TabParamIn["FixedKeys"];
    $TabParamOut["Groupby"]   = $TabParamIn["Groupby"];
    $TabParamOut["IDSectors"] = $TabParamIn["IDSectors"];
    $TabParamOut["Device"]    = $TabParamIn["Device"];
    $TabParamOut["RecordMin"] = $TabParamIn["RecordMin"];
    $TabParamOut["RecordMax"] = $TabParamIn["RecordMax"];

    //--------------------------------------------------
    // Set Default Parameters 
    //--------------------------------------------------
        
    if ($TabParamIn["Output"] == "Json")
    {
      $TabParamOut["Output"] = "Json";	
    }
    else
    {
      $TabParamOut["Output"] = "Xml";	
    }
    
    $Cache = "No";	
    $TabParamOut["Cache"] = $Cache;
     
    $this->Url = $this->CreateUrl("FindIndexQat.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    //exit();
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
    
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"] = $this->DecodeYesNo($TabParamOut["Cache"]);
        
      return $TabParamOut;
      
    }  
     
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function GetTemplate($TabParamIn, $Execute = TRUE)
  {
    $Account = $TabParamIn["Account"]; 
    $Source  = $TabParamIn["Source"]; 

    $TabParamOut = array();
    
    $TabParamOut["IDTemplate"] = $TabParamIn["Template"];
    $TabParamOut["IDOperator"] = $TabParamIn["Operator"];
   
    //--------------------------------------------------
    // Set Default Parameters GetCategory
    //--------------------------------------------------
   
    if ($TabParamIn["Cache"] == "No")
    {
      $Cache = "No";	
    }
    else
    {
      $Cache = "Yes";	
    }
    $TabParamOut["Cache"] = $Cache;	

 

    //print_r ($TabParamOut);     
    //exit();
    
    
    $this->Url = $this->CreateUrl("GetTemplate.php", $TabParamOut);      
    //echo "Url=".$this->Url."<br>";
    
    if ($Execute == TRUE)
    {
      //--------------------------------------------------
      // Execute Request
      //--------------------------------------------------
      
      return $this->Execute($this->Url, $Account, $Source, $Cache);
      
    }
    else
    {
    	
      //--------------------------------------------------
      // Decoding Parameters
      //--------------------------------------------------
      
      $TabParamOut["Cache"] = $this->DecodeYesNo($TabParamOut["Cache"]);
  
      return $TabParamOut;
      
    }  
     
  }

    
//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function DecodeYesNo($ValueIn)
  {
    if ($ValueIn == "No")
    {
      $ValueOut = FALSE;	
    }
    else
    {
      $ValueOut = TRUE;	
    }
    return $ValueOut;
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function DecodeVisible($ValueIn)
  {
  
    switch($ValueIn)
    {
    case "No":
      $ValueOut = 0;      
      break;
    
    case "Yes":
      $ValueOut = 1;
      break;
      
    case "Test":
      $ValueOut = 2;
      break;
      
    default:
      $ValueOut = 1;
    } 
    return $ValueOut;
  }  

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function EncodeVisible($ValueIn)
  {
  
    $ValueOut = "";

    switch($ValueIn)
    {
    case 0:
      $ValueOut = "No";      
      break;
    
    case 1:
      $ValueOut = "Yes";
      break;
      
    case 2:
      $ValueOut = "Test";
      break;
    } 
    return $ValueOut;
  }  


//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function DecodeProtection($ValueIn)
  {
    
    switch($ValueIn)
    {
    case "NDS-Separate Delivery":
      $ValueOut = 1;
      break;
      
    case "NDS-ForwardLock":
      $ValueOut = 2;
      break;
  
    default:
     $ValueOut = 0;
     break;
    } 
    return $ValueOut;
  }  

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function EncodeProtection($ValueIn)
  {
    
    $ValueOut = "";

    switch($ValueIn)
    {
    case 1:
      $ValueOut = "NDS-Separate Delivery";
      break;
      
    case 2:
      $ValueOut = "NDS-ForwardLock";
      break;
    } 
    return $ValueOut;
  }  

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function CreateUrl($Function, $TabParam)
  {
   
    //$Url = $this->XmlFolder."/".$Function."?ACOMMDB_VERSION=".$this->Version."&";	
    $Url = $this->XmlFolder."/".$Function."?";	
        
    while (list($Param) = each($TabParam))
    {
      //echo "Param=".$Param."<br>";
      //echo "Value=".$TabParam[$Param]."<br>";
      if ($Param != "Cache")
      {
        $Url .= $Param."=".$TabParam[$Param]."&";
      }  
    }
    
    $Url = substr($Url, 0, strlen($Url) - 1);
    
    return $Url;
  
  }

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  private function Execute($Url, $Account, $Source, $Cache)
  {
    list($usec, $sec) = explode(" ",microtime()); 
    $TimeStart = ((float)$usec + (float)$sec); 
    
    if ($Cache == "Yes" && strlen($this->PathCache) > 0 && strlen($this->PathPerformance) > 0)
    //if ($Cache == TRUE && strlen($this->PathCache) > 0 && strlen($this->PathPerformance) > 0)
    {
      //echo "Url=".$Url."<br>";

      $AcoMMDBHttp = FALSE;
      	
      $Path = $this->PathCache."/".crc32($Url).".cache";	
      //echo "Path=".$Path."<br>";
      
      $Handle = @fopen($Path, "r");
    }
    else
    {
      $Handle = FALSE;
    }  
    
    //echo "Handle=".$Handle."<br>";
    //exit();
    
    if ($Handle == FALSE)
    {
      $AcoMMDBHttp = TRUE;
      //echo "this->UrlSite=".$this->UrlSite."<br>";
      //echo "Url=".$Url."<br>";
      $UrlComplete = $this->UrlSite."/".$Url."&Account=".$Account."&Source=".$Source."&Cache=".$Cache."&ClassVersion=".$this->Version;
      if (strlen($this->PathPerformance) > 0)
      {
        $UrlComplete .= "&WritePerformance=No";
      }
      //echo "UrlComplete=".$UrlComplete."<br>";
      //exit();

      $Handle = @fopen($UrlComplete, "r");	
    }
    
    //echo "Handle=".$Handle."<br>";
    //exit();
   
    $Xml = "";   
    if ($Handle == TRUE)
    {  
      while(TRUE)
      {
        $Block = fread ($Handle, 1024);
        if ($Block == FALSE)
        {
          break;
        }
        else
        {
          $Xml .= $Block;
        }
      }
      fclose ($Handle);
    }  
    list($usec, $sec) = explode(" ",microtime()); 
    $TimeEnd = ((float)$usec + (float)$sec); 

    $TimeElab = $TimeEnd - $TimeStart;
    //echo "TimeElab=".$TimeElab." for ".$Url."<br>";

    
    if (strlen($this->PathPerformance) > 0)
    {
      $this->WritePerformance($Xml, $Url, $Account, $Source, $Cache, $AcoMMDBHttp, $TimeElab);
    }  
    
        
    return $Xml;
    
  }    

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------------------------------

  public function WritePerformance(&$Xml, $Url, $Account, $Source, $Cache, $AcoMMDBHttp, $TimeElab)
  {
    if (strlen($Xml) == 0)
    {
      return FALSE;	
    }
    
    // Write Logs
    $Day = date("d");
    $Month = date("m");
    $Year = date("Y");
  
    $Hour = date("H");
    $Minute = date("i");
    $Second = date("s");
  
    $Now = $Year."/".$Month."/".$Day." ".$Hour.":".$Minute.":".$Second;
  
    $Type = 2; // LOG_PERFORMANCE_XML

    
    $Function = "UNKNOWN";
    if (strpos($Url, "GetDevice.php") > 0)
    {
      $Function = "GETDEVICE";	
    }
    if (strpos($Url, "GetCategory.php") > 0)
    {
      $Function = "GETCATEGORY";	
    }
    if (strpos($Url, "GetIndex.php") > 0)
    {
      $Function = "GETINDEX";	
    }
    if (strpos($Url, "GetObject.php") > 0)
    {
      $Function = "GETOBJECT";	
    }
    if (strpos($Url, "FindIndex.php") > 0)
    {
      $Function = "FINDINDEX";	
    }
    if (strpos($Url, "GetDownload.php") > 0)
    {
      $Function = "GETDOWNLOAD";	
    }
    if (strpos($Url, "GetTemplate.php") > 0)
    {
      $Function = "GETTEMPLATE";	
    }
    
    
    
    if (strpos($Url, "GetSectorsQat.php") > 0)
    {
      $Function = "GETSECTORSQAT";	
    }
    if (strpos($Url, "GetCategoryQat.php") > 0)
    {
      $Function = "GETCATEGORYQAT";	
    }
    if (strpos($Url, "GetIndexQat.php") > 0)
    {
      $Function = "GETINDEXQAT";	
    }
    if (strpos($Url, "FindIndexQat.php") > 0)
    {
      $Function = "FINDINDEXQAT";	
    }
    if (strpos($Url, "GetObjectQat.php") > 0)
    {
      $Function = "GETOBJECTQAT";	
    }
    if (strpos($Url, "GetReccomendationsQat.php") > 0)
    {
      $Function = "GETRECCOMENDATIONSQAT";	
    }
    if (strpos($Url, "SearchKeysQat.php") > 0)
    {
      $Function = "SEARCHKEYSQAT";	
    }
    if (strpos($Url, "SearchIndexQat.php") > 0)
    {
      $Function = "SEARCHINDEXQAT";	
    }
    //echo "Function=".$Function."<br>";
    //exit();
    if ($Function == "UNKNOWN")
    {
      echo "Url=".$Url."<br>";
      exit();
    }  

   
    
    if ($Cache == "Yes")
    {
      $CacheRequest = 1;	
    }
    else
    {
      $CacheRequest = 0;	
    }
    
    $AcoMMDBUse = 1;
    
    if ($AcoMMDBHttp == TRUE)
    {
      $AcoMMDBHttp = 1;	
    }
    else
    {
      $AcoMMDBHttp = 0;	
    }
    
    
    $Key = 'Output=Json';
    $Pos = strpos($Url, $Key);
    if (strlen($Pos) > 0)
    {
    	$Output = "Json";
    }
    else
    {
    	$Output = "Xml";
    }
    //echo "Url=".$Url."<br>";
    //echo "Output=".$Output."<br>";
    //exit();

    
    $XmlSize = 0;
    $XmlRecords = 0;
    
    $XmlSize = strlen($Xml);
    //echo "XmlSize=".$XmlSize."<br>";
    
    
    
    if ($Output == "Xml")
    {
      $Key = '<InterfaceID>';
      $Start = strpos($Xml, $Key);
      if (strlen($Start) == NULL)
      {
        return FALSE;	
      }
      $Start = $Start + strlen($Key);
      
      $Key = '</InterfaceID>';
      $End = strpos($Xml, $Key, $Start);
      if (strlen($End) == NULL)
      {
        return FALSE;	
      }
      $IDXmlInterface = substr($Xml, $Start, $End - $Start);
      
      
      
      $Key = 'Cache="';
      $Start = strpos($Xml, $Key);
      if (strlen($Start) == NULL)
      {
        return FALSE;	
      }
      $Start = $Start + strlen($Key);
      
      $Key = '">';
      $End = strpos($Xml, $Key, $Start);
      if (strlen($End) == NULL)
      {
        return FALSE;	
      }
      $CacheResponse = substr($Xml, $Start, $End - $Start);

 
    
      if ($Function == "SEARCHINDEXQAT")
      {
        $Key = 'RecordsTotals="';
        $Start = strpos($Xml, $Key);
        if (strlen($Start) > 0)
        {
          $Start = $Start + strlen($Key);
      
          $Key = '">';
          $End = strpos($Xml, $Key, $Start);
          if (strlen($End) > 0)
          {
            $XmlRecords = substr($Xml, $Start, $End - $Start);
          }
        }  
      }	
    }
    
    
    
    if ($Output == "Json")
    {
      $Key = '"InterfaceID":';
      $Start = strpos($Xml, $Key);
      if (strlen($Start) == NULL)
      {
        return FALSE;	
      }
      $Start = $Start + strlen($Key);
      
      $Key = ',';
      $End = strpos($Xml, $Key, $Start);
      if (strlen($End) == NULL)
      {
        return FALSE;	
      }
      $IDXmlInterface = substr($Xml, $Start, $End - $Start);
      
      
      
      $Key = '"Cache":"';
      $Start = strpos($Xml, $Key);
      if (strlen($Start) == NULL)
      {
        return FALSE;	
      }
      $Start = $Start + strlen($Key);
       
      $Key = '",';
      $End = strpos($Xml, $Key, $Start);
      if (strlen($End) == NULL)
      {
        return FALSE;	
      }
      $CacheResponse = substr($Xml, $Start, $End - $Start);
       
     
     
      if ($Function == "SEARCHINDEXQAT")
      {
        $Key = '"RECORDSTOTALS":';
        $Start = strpos($Xml, $Key);
        if (strlen($Start) > 0)
        {
          $Start = $Start + strlen($Key);
      
          $Key = ',';
          $End = strpos($Xml, $Key, $Start);
          if (strlen($End) > 0)
          {
            $XmlRecords = substr($Xml, $Start, $End - $Start);
          }
        }  
      }	
    }
    
    if ($CacheResponse == "Yes")
    {
      $CacheResponse = 1;	
    }
    else
    {
      $CacheResponse = 0;	
    }
/*    
    echo "IDXmlInterface=".$IDXmlInterface."<br>";
    echo "CacheResponse=".$CacheResponse."<br>";
    echo "XmlRecords=".$XmlRecords."<br>";
    exit();
*/   
   
      
    $Buffer = $Now.", ".$Type.", ".$Function.", ".$Account.", ".$Source.", ".$CacheRequest.", ".$this->Version.", ".$AcoMMDBUse.", ".$AcoMMDBHttp.", ".$Url.", ".$IDXmlInterface.", ".$TimeElab.", ".$CacheResponse.", ".$XmlSize.", ".$XmlRecords;
    //echo "Buffer=".$Buffer."<br>";
    //exit();
    
    $Key = rand (1, 1000000000);
    
    //echo "WritePerformance<br>";
    
    $PathName = $this->PathPerformance."/".$Key.".log";
    //echo "PathName=".$PathName."<br>";
    //exit();
    
    $Handle = fopen($PathName, "w");
    //echo "Handle=".$Handle."<br>";
    fputs ($Handle, $Buffer, strlen($Buffer));
    fclose ($Handle);
    //exit();
    
    return TRUE;
 
  }

/*
  OLD FROM 17-04-2007
  public function WritePerformance(&$Xml, $Url, $Account, $Source, $Cache, $AcoMMDBHttp, $TimeElab)
  {
    if (strlen($Xml) == 0)
    {
      return FALSE;	
    }
    
    // Write Logs
    $Day = date("d");
    $Month = date("m");
    $Year = date("Y");
  
    $Hour = date("H");
    $Minute = date("i");
    $Second = date("s");
  
    $Now = $Year."/".$Month."/".$Day." ".$Hour.":".$Minute.":".$Second;
  
    $Type = 2; // LOG_PERFORMANCE_XML

    
    $Function = "UNKNOWN";
    if (strpos($Url, "GetDevice.php") > 0)
    {
      $Function = "GETDEVICE";	
    }
    if (strpos($Url, "GetCategory.php") > 0)
    {
      $Function = "GETCATEGORY";	
    }
    if (strpos($Url, "GetIndex.php") > 0)
    {
      $Function = "GETINDEX";	
    }
    if (strpos($Url, "GetObject.php") > 0)
    {
      $Function = "GETOBJECT";	
    }
    if (strpos($Url, "FindIndex.php") > 0)
    {
      $Function = "FINDINDEX";	
    }
    if (strpos($Url, "GetTemplate.php") > 0)
    {
      $Function = "GETTEMPLATE";	
    }



    if (strpos($Url, "GetSectorsQat.php") > 0)
    {
      $Function = "GETSECTORSQAT";	
    }
    if (strpos($Url, "GetCategoryQat.php") > 0)
    {
      $Function = "GETCATEGORYQAT";	
    }
    if (strpos($Url, "GetIndexQat.php") > 0)
    {
      $Function = "GETINDEXQAT";	
    }
    if (strpos($Url, "FindIndexQat.php") > 0)
    {
      $Function = "FINDINDEXQAT";	
    }
    if (strpos($Url, "GetObjectQat.php") > 0)
    {
      $Function = "GETOBJECTQAT";	
    }
    if (strpos($Url, "GetReccomentationsQat.php") > 0)
    {
      $Function = "GETRECCOMENTATIONSQAT";	
    }
    if (strpos($Url, "SearchKeysQat.php") > 0)
    {
      $Function = "SEARCHKEYSQAT";	
    }
    if (strpos($Url, "SearchIndexQat.php") > 0)
    {
      $Function = "SEARCHINDEXQAT";	
    }
    
    //echo "Url=".$Url."<br>";
    //echo "Function=".$Function."<br>";
    
        
    
   
    if ($Cache == "Yes")
    {
      $CacheRequest = 1;	
    }
    else
    {
      $CacheRequest = 0;	
    }
    
    $AcoMMDBUse = 1;
    
    if ($AcoMMDBHttp == TRUE)
    {
      $AcoMMDBHttp = 1;	
    }
    else
    {
      $AcoMMDBHttp = 0;	
    }
    
    
    
    $Key = '<InterfaceID>';
    $Start = strpos($Xml, $Key);
    if (strlen($Start) == NULL)
    {
      return FALSE;	
    }
    $Start = $Start + strlen($Key);
    //echo "Start=".$Start."<br>";
    
    $Key = '</InterfaceID>';
    $End = strpos($Xml, $Key, $Start);
    if (strlen($End) == NULL)
    {
      return FALSE;	
    }
    //echo "End=".$End."<br>";
    
    $IDXmlInterface = substr($Xml, $Start, $End - $Start);
    //echo "IDXmlInterface=".$IDXmlInterface."<br>";
    
    
    
    $Key = 'Cache="';
    $Start = strpos($Xml, $Key);
    if (strlen($Start) == NULL)
    {
      return FALSE;	
    }
    $Start = $Start + strlen($Key);
    //echo "Start=".$Start."<br>";
    
    $Key = '">';
    $End = strpos($Xml, $Key, $Start);
    if (strlen($End) == NULL)
    {
      return FALSE;	
    }
    //echo "End=".$End."<br>";
    
    $CacheResponse = substr($Xml, $Start, $End - $Start);
    //echo "CacheResponse=".$CacheResponse."<br>";
    //exit();
    if ($CacheResponse == "Yes")
    {
      $CacheResponse = 1;	
    }
    else
    {
      $CacheResponse = 0;	
    }
    
    $XmlSize = 0;
    $XmlRecords = 0;
  
    $XmlSize = strlen($Xml);
    //echo "XmlSize=".$XmlSize."<br>";
    
    if ($Function == "FINDINDEXQAT")
    {
      $Key = 'RecordsTotals="';
      $Start = strpos($Xml, $Key);
      if (strlen($Start) > 0)
      {
        $Start = $Start + strlen($Key);
        //echo "Start=".$Start."<br>";
    
        $Key = '">';
        $End = strpos($Xml, $Key, $Start);
        //echo "End=".$End."<br>";
        if (strlen($End) > 0)
        {
          $XmlRecords = substr($Xml, $Start, $End - $Start);
        }
      }  
    }	
    //echo "XmlRecords=".$XmlRecords."<br>";
    //exit();
      
    //$Buffer = $Now.", ".$Type.", ".$Function.", ".$Account.", ".$Source.", ".$CacheRequest.", ".$AcoMMDBUse.", ".$AcoMMDBHttp.", ".$Url.", ".$IDXmlInterface.", ".$TimeElab.", ".$CacheResponse;
    $Buffer = $Now.", ".$Type.", ".$Function.", ".$Account.", ".$Source.", ".$CacheRequest.", ".$this->Version.", ".$AcoMMDBUse.", ".$AcoMMDBHttp.", ".$Url.", ".$IDXmlInterface.", ".$TimeElab.", ".$CacheResponse.", ".$XmlSize.", ".$XmlRecords;
    //echo "Buffer=".$Buffer."<br>";
    //exit();
    
    $Key = rand (1, 1000000000);
    
    //echo "WritePerformance<br>";
    
    $PathName = $this->PathPerformance."/".$Key.".log";
    //echo "PathName=".$PathName."<br>";
    
    $Handle = fopen($PathName, "w");
    //echo "Handle=".$Handle."<br>";
    fputs ($Handle, $Buffer, strlen($Buffer));
    fclose ($Handle);
    
    return TRUE;
    
  }
*/ 



	private function mmdb4_write_cache($_TabParamIn){

		$_XmlIn = $_TabParamIn["built_xml"];

		# check if we must do cache or not ;
		if($_cache_disable) {
			return FALSE;
		}

		# get filename ;
		if($this->mmdb4_cache_generate_name($_TabParamIn, $_TabParamOut)) {
			$_dir_built             = $_TabParamOut["dir_built"];
			$_filename_built        = $_TabParamOut["filename_built"];
			$_file                  = $_TabParamOut["file"];
		} else {
			return FALSE;
		}

		# write file ;
		$fh = @fopen($_file, "w");
		if(!$fh) {
			return FALSE;
		}
		$fwritestats = @fwrite($fh, $_XmlIn);
		@fclose($fh);
		if(!$fwritestats) {
			return FALSE;
		}

		return TRUE;

	}

	private function mmdb4_check_and_create_dir($_dir){

		$_arrDir = array();
		$_arrCreated = array();
		$_arrDir = explode("/",$_dir);
		for($_j=0; $_j<sizeof($_arrDir); $_j++) {
			if(!is_dir($_conc . $_arrDir[$_j])) {
				if($_arrDir[$_j]) {
					$b = mkdir($_conc . $_arrDir[$_j],0777);
					if($b) {
						$_arrCreated[] = $_conc . $_arrDir[$_j];
					}
				}
			}
			$_conc .= $_arrDir[$_j] . "/";
			for($_k=0; $_k<sizeof($_arrCreated); $_k++) {
				@chmod($_arrCreated[$_k],0777);
			}
		}

	}

	private function mmdb4_cache_generate_name($_TabParamIn,&$_TabParamOut){

		$_array_remove_elements = array("Cache","Account","FuncCacheDisable","FuncQueryType","Source","XML","built_xml");
		foreach($_TabParamIn as $_key => $_value) {
			if(!in_array($_key,$_array_remove_elements)) {
				$_TabParamInForName[$_key] = $_value;
				//error_log("==== \$_key: [$_key] || \$_value: [$_value]\n",3,$logfile);
			}
		}
		
		ksort($_TabParamIn);
		
		$_xml                   = $_TabParamIn["Xml"];
		$_category              = $_TabParamIn["Category"];
		$_query_type            = $_TabParamIn["FuncQueryType"];
		$_device                = $_TabParamIn["Device"];
		$_index_code            = $_TabParamIn["Code"];
		$_index_id_publication  = $_TabParamIn["IDIndexPubl"];
		$_progress              = $_TabParamIn["Progress"];
		$_empty                 = $_TabParamIn["Empty"];
		$_visible_letter        = $_TabParamIn["Visible"];

		# check Xml ;
		if(!$_xml) {
			$_xml = "3.0";
		}
		# check device ;
		if(!$_device) {
			$_device = "ALL";
		}
		# check progress ;
		if(!$_progress) {
			$_progress = "ALL";
		}
		# check empty ;
		if(!$_empty) {
			$_empty = "ALL";
		}
		# give values to params ;
		foreach($_TabParamInForName as $_key => $_value) {
				$add_string .= "@" . $_key . "=" . $_value;
		}

		# generate name ;
		//mmdb4/x_3.0/d_699/c/4891.xml                  (cat)
		//mmdb4/x_3.0/d_699/c_4891/p_1100/i/c_19281     (index objects)
		//mmdb4/x_3.0/d_699/c_4891/p_1100/i/o_1928191   (index object)
		//mmdb4/x_3.0/d_699/c_4891/p_1100/i/ip_156271   (index publication)
		//error_log("\$_xml: [$_xml]\n",3,$logfile);
		//error_log("\$_category: [$_category]\n",3,$logfile);
		//error_log("\$_query_type: [$_query_type]\n",3,$logfile);

		switch($_query_type){

			case "GetCategories":

				$_dir_built              = FUNC_CACHE_DIR . "x_" . $_xml . "/d_" . $_device . "/c/";
				$_filename_built         = "c_" . $_category . ".";
				$_filename_built        .= $add_string;
				$_file                   = $_dir_built . md5($_filename_built) . ".xml";
				break;

			case "GetObjects":

				$_dir_built = FUNC_CACHE_DIR . "x_" . $_xml . "/d_" . $_device . "/c_" . $_category . "/p_" . $_progress . "/i/";
				$_filename_built         = "c_" . $_category . ".";
				$_filename_built        .= $add_string;
				$_file                   = $_dir_built . md5($_filename_built) . ".xml";
				break;

			case "GetObject":
				
			$_dir_built = FUNC_CACHE_DIR . "x_" . $_xml . "/d_" . $_device . "/c_" . $_category . "/p_" . $_progress . "/i/";
				if(is_numeric($_index_id_publication)) {
					$_filename_built         = "publication_" . $_index_id_publication;
					$_filename_built        .= $add_string;
				} else {
					$_filename_built         = "code_" . $_index_code;
					$_filename_built        .= $add_string;
				}
				$_file = $_dir_built . md5($_filename_built) . ".xml";
				break;

			default:
				return FALSE;
				break;
		}

		$_TabParamOut["dir_built"]      = $_dir_built;
        	$_TabParamOut["filename_built"] = $_filename_built;
        	$_TabParamOut["file"]           = $_file;

		return TRUE;
	}

	private function mmdb4_check_cache($_TabParamIn,&$_RetCacheXML){

		$logfile = "/tmp/mmdb4_check_cache.txt";
		$_RetCacheXML = array();
		$_cache_disable = $_TabParamIn["FuncCacheDisable"];

		# check if we must do cache or not ;
		if($_cache_disable) {
			return FALSE;
		}

		# get filename ;
		if($this->mmdb4_cache_generate_name($_TabParamIn,$_TabParamOut)) {
			$_dir_built             = $_TabParamOut["dir_built"];
			$_filename_built        = $_TabParamOut["filename_built"];
			$_file                  = $_TabParamOut["file"];
		} else {
			return FALSE;
		}

		# check if file exist and if we must use the cache or not ;
		if(file_exists($_file)) {

                	if(FUNC_CACHE_FILE_MAX_TIME != 0) {
                        	
				$_time_passed = time()-filemtime($_file);
                        	$_max_time = FUNC_CACHE_FILE_MAX_TIME * 60;
                        	
				if($_max_time >= $_time_passed) {
                                	$_faltam = $_max_time-$_time_passed;
                                	// use cache ;
                                	$_file_buffer = @file_get_contents($_file);
                                	if(!$_file_buffer) {
                                        	// could not open file ;
                                        	return FALSE;
                                	}
                                	$_RetCacheXML = $_file_buffer;
                                	if(!$_RetCacheXML) {
                                        	// could not find array ;
                                        	return FALSE;
                                	}
					error_log("CACHE OK [$_file]\n",3,$logfile);
                               		return TRUE;
                        	} else {
					// cache is old ;
					error_log("CACHE OLD [$_file]\n",3,$logfile);
                                	return FALSE;
                        	}
                	
			} else {
                        	// cache is disabled ;
                        	return FALSE;
                	}

        	} else {
		
			error_log("CACHE DOESNT EXIST [$_file]\n",3,$logfile);
                	// cache does not exist ;
                	$this->mmdb4_check_and_create_dir($_dir_built);
                	return FALSE;
        	}

        	return FALSE;

	}


   
}      

?>
