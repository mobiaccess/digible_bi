<?php
/**
 * MSISDN Manager
 *
 * @package Acotel
 * @subpackage MSISDN
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * MSISDN Manager
 *
 * Get information about MSISDN numbers via external APIs
 *
 * @package Acotel
 * @subpackage MSISDN
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_MSISDN_MSISDNManager {
	private static $instance;

	private function __construct() {
	}

	/**
	 * Returns a manager instance
	 *
	 * @return Acotel_MSISDN_MSISDNManager
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Retrieve account information about specified MSISDN number
	 *
	 * @param string $msisdn MSISDN number
	 * @return Acotel_MSISDN_MSISDN
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 * @throws Acotel_Exceptions_ConfigurationNotFoundException
	 * @see Acotel_MSISDN_MSISDN
	 */
	public function getAccountInfo($msisdn) {
		if (!Acotel_MSISDN_MSISDN::validateMSISDN($msisdn)) {
			throw new Acotel_MSISDN_Exceptions_InvalidMSISDNException();
		}

		$config = $this->getConfig();
		$url = $config['checker_api']['url'] . '&msisdn=' . $msisdn;

		$response = $this->makeRequest($url);
		$object = $this->transformXMLToObject($response);
		return $object;
	}

	private function getConfig() {
		$config = Acotel_Config::get('msisdn');
		if (!$config) {
			throw new Acotel_Exceptions_ConfigurationNotFoundException();
		}

		return $config;
	}

	private function makeRequest($url) {
		return file_get_contents($url);
	}

	private function transformXMLToObject($xml) {
		$info = simplexml_load_string($xml);

		$number = (string) $info->Header[0]->Msisdn[0];
		$carrier = (string) $info->Result[0]->AccountCarrier[0];
		$type = (string) $info->Result[0]->AccountType[0];
		$status = (string) $info->Result[0]->AccountStatus[0];

		$msisdn = new Acotel_MSISDN_MSISDN();
		$msisdn->setNumber($number);
		$msisdn->setAccountCarrier($carrier);
		$msisdn->setAccountType($type);
		$msisdn->setAccountStatus($status);

		return $msisdn;
	}
}
?>
