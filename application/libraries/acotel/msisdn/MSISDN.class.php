<?php
/**
 * MSISDN number
 *
 * @package Acotel
 * @subpackage MSISDN
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Represents MSISDN Number
 *
 * @package Acotel
 * @subpackage MSISDN
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_MSISDN_MSISDN {
	const ACCOUNT_TYPE_PREPAID = 0;
	const ACCOUNT_TYPE_POSTPAID = 1;

	const ACCOUNT_CARRIER_TIM = 1;
	const ACCOUNT_CARRIER_OTHER = 0;

	const ACCOUNT_STATUS_ACTIVE = 1;
	const ACCOUNT_STATUS_INACTIVE = 0;

	private $number;
	private $accountCarrier;
	private $accountType;
	private $accountStatus;

	public function setNumber($number) {
		if (!self::validateMSISDN($number)) {
			throw new Acotel_MSISDN_Exceptions_InvalidMSISDNException();
		}

		$this->number = $number;
	}

	public function getNumber() {
		return $this->number;
	}

	public function setAccountCarrier($carrier) {
		$this->accountCarrier = $carrier;
	}

	public function getAccountCarrier() {
		return $this->accountCarrier;
	}

	public function setAccountType($type) {
		$this->accountType = $type;
	}

	public function getAccountType() {
		return $this->accountType;
	}

	public function setAccountStatus($status) {
		$this->accountStatus = $status;
	}

	public function getAccountStatus() {
		return $this->accountStatus;
	}

	public function isTIM() {
		return $this->accountCarrier === self::ACCOUNT_CARRIER_TIM;
	}

	public function isPrepaid() {
		return $this->accountType === self::ACCOUNT_TYPE_PREPAID;
	}

	public function isPostpaid() {
		return $this->accountType === self::ACCOUNT_TYPE_POSTPAID;
	}

	/**
	 * Validates MSISDN number.
	 *
	 * @param int $msisdn MSISDN number
	 * @return bool
	 */
	public static function validateMSISDN($msisdn) {
		if (!$msisdn) {
			return false;
		} else if (is_numeric($msisdn)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Formats a incomplete MSISDN number. This method assumes that the
	 * country code is 55 (Brazil).
	 *
	 * @param string $msisdn
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 */
	public static function formatMSISDN($msisdn) {
		if (!self::validateMSISDN($msisdn)) {
			throw new Acotel_MSISDN_Exceptions_InvalidMSISDNException();
		}

		$len = strlen($msisdn);

		if ($len == 14 && strpos($msisdn, '00') === 0) {
			return substr($msisdn, 2);
		} else if ($len == 12 && strpos($msisdn, '00') === 0) {
			return '55' . substr($msisdn, 2);
		} else if ($len == 11 && strpos($msisdn, '0') === 0) {
			return '55' . substr($msisdn, 1);
		} else if ($len == 10) {
			return '55' . $msisdn;
		} else {
			return $msisdn;
		}
	}

	/**
	 * Parse an MSISDN number and return an array with these values:
	 * <ul>
	 * <li><b>country_code</b>: Coutry code (currently, only 2-digit number is
	 * supported)</li>
	 * <li><b>area_code</b>: Area code (or "DDD", 2 digits)</li>
	 * <li><b>number</b>: The rest of number</li>
	 * </ul>
	 *
	 * @param string $msisdn
	 * @return array Array with each part
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 */
	public static function parseNumber($msisdn) {
		$normalized_number = self::formatMSISDN($msisdn);
		return array(
			'country_code' => substr($normalized_number, 0, 2),
			'area_code' => substr($normalized_number, 2, 2),
			'number' => substr($normalized_number, 4),
		);
	}

	public function __toString() {
		return (string) $this->getNumber();
	}
}
?>
