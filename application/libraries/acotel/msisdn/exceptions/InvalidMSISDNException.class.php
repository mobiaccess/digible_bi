<?php
/**
 * Exception thrown for invalid MSISDN numbers
 *
 * @package Acotel
 * @subpackage MSISDN
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Exception thrown for invalid MSISDN numbers
 *
 * @package Acotel
 * @subpackage MSISDN
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_MSISDN_Exceptions_InvalidMSISDNException extends Exception {
}
?>
