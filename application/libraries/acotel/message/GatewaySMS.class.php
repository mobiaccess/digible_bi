<?php

class Acotel_Message_GatewaySMS {

	private $url;
	private $extraInfo;
	private $msisdnAndSenderCollection;
	private $message;
	private $codeForStatistcs;
	private $beginTime;
	private $endTime;
	private $serverName;
	private $applicationName;
	private $applicationIp;
	private $bodyRequest;

	public function __construct(){
	
	}

	public function setMsisdnAndSenderCollection($msisdnSenderCollection){
		$this->msisdnAndSenderCollection = $msisdnSenderCollection;
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function setExtraInfo($extraInfo){
		$this->extraInfo = $extraInfo;
	}

	public function setMessage($message){
		$this->message = $message;
	}

	public function setBeginTime($beginTime){
		$this->beginTime = $beginTime;
	}

	public function setEndTime($endTime){
		$this->endTime = $endTime;
	}

	public function setServerName($serverName){
		$this->serverName = $serverName;
	}

	public function setApplicationName($applicationName){
		$this->applicationName = $applicationName;
	}

	public function setApplicationIp($applicationIp){
		$this->applicationIp = $applicationIp;
	}

	public function setCodeForStatistics($cfs){
		$this->codeForStatistics = $cfs;
	}

	public function getBodyRequest(){
		return $this->bodyRequest;
	}

	private function prepareBodyRequest(){

		$this->validateRequiredFields();

		$req  = "<sms>\n";
		$req .= "	<moreInfo>{$this->extraInfo}</moreInfo>\n";
		$req .= "	<message>{$this->message}</message>\n";
		$req .= "	<weight>1</weight>\n";
		$req .= "	<beginTime>{$this->beginTime}</beginTime>\n";
		$req .= "	<endTime>{$this->endTime}</endTime>\n";
		$req .= "	<queueCountInserted>0</queueCountInserted>\n";
		$req .= "	<queueCountProcessed>0</queueCountProcessed>\n";
		$req .= "	<cfs>{$this->codeForStatistics}</cfs>\n";
		$req .= "	<servers>\n";
		$req .= "		<server>\n";
		$req .= "			<name>{$this->serverName}</name>\n";
		$req .= "		</server>\n";
		$req .= "	</servers>\n";
		$req .= "	<las>\n";
		$req .= "		<la>\n";
		$req .= "			<number>800</number>\n";
		$req .= "		</la>\n";
		$req .= "	</las>\n";
		$req .= "	<queue>\n";

		foreach($this->msisdnAndSenderCollection as $collection){
			$req .= "		<customer>\n";
			$req .= "			<msisdn>{$collection[0]}</msisdn>\n";
			$req .= "		</customer>\n";
		}

		$req .= "	</queue>\n";
		$req .= "	<application>\n";
		$req .= "		<name>{$this->applicationName}</name>\n";
		$req .= "		<ip>{$this->applicationIp}</ip>\n";
		$req .= "	</application>\n";
		$req .= "	<limit>0</limit>\n";
		$req .= "	<customersInQueue>0</customersInQueue>\n";
		$req .= "</sms>\n";

		$this->bodyRequest = $req;

	}

	public function send(){
		$this->prepareBodyRequest();
		return $this->sendRequestToGatewaySMSServer();
	}

	private function sendRequestToGatewaySMSServer(){
		return Acotel_Rest_RestClient::post( $this->url, $this->bodyRequest, null, null, "application/xml" );
	}

	private function validateRequiredFields(){
		try {

			if(!$this->msisdnAndSenderCollection){
				throw new Exception('setMsisdnAndSenderCollection()');
			}

			if(!$this->message){
				throw new Exception('setMessage()');
			}

			if(!$this->codeForStatistics){
				throw new Exception('setCodeForStatistics()');
			}
		
			if(!$this->serverName){
				throw new Exception('setServerName()');
			}

			if(!$this->applicationIp){
				throw new Exception('setApplicationIp()');
			}

		} catch(Exception $e){
			throw new Exception("You must invoke method {$e->getMessage()}");
		}
	}

}

?>
