<?php
/**
 * Class to send SMS.
 *
 * @package Acotel
 * @subpackage Message
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Includig HTTP_Request2 class
 */
require_once 'HTTP/Request2.php';
require_once 'HTTP/Request2/Adapter/Mock.php';

/**
 * Class to send SMS.
 *
 * To configure the class, use the 'JMR' key on {@link Acotel_Config::set()}
 * method and the following values:
 * <code>
 * array(
 *   'name' => 'groupname',
 *   'url' => 'API URL',
 * );
 * </code>
 *
 * @package Acotel
 * @subpackage Message
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 * @todo Implements check when config array was not specified
 * @todo Check for error response
 */
class Acotel_Message_SMS extends Acotel_API_GenericAPI {
	/**
	 * Max number of characters per message
	 */
	const MESSAGE_MAX_LENGTH = 160;

	private $options = array();

	public function __construct($config = null) {
		parent::__construct();

		$this->setMethod('POST');
		$this->request
			->setConfig('ssl_verify_peer', false)
			->setConfig('ssl_verify_host', false)
			->setHeader('Content-type: text/xml; charset=utf-8');

		if ($config) {
			$this->setOptions($config);
		}
	}

	/**
	 * Set options for the API. If $options is an array, then the options are set
	 * directly. If it is a string, then a call is made to
	 * {@link Acotel_Config::get()} passing this string as an argument.
	 *
	 * Expected options are:
	 * <ul>
	 * <li><b>url</b>: API URL</li>
	 * <li><b>group_name</b>: Group name passed on request</li>
	 * <li><b>csp</b>: CSP value</li>
	 * <li><b>sender</b>: Sender number</li>
	 * </ul>
	 * You can also pass only some keys - the values of other keys will be
	 * left as is.
	 *
	 * @param array|string $options Options arrray or the name of 
	 */
	public function setOptions($options) {
		if (is_array($options)) {
			$opts = $options;
		} else {
			$opts = Acotel_Config::get($options);
		}

		$this->options = array_merge($this->options, $opts);
		$this->setUrl($this->options['url']);
	}

	public function getOption($opt) {
		return $this->options[$opt];
	}

	/**
	 * Send $message to $msisdn number.
	 *
	 * @param string|Acotel_MSISDN_MSISDN $msisdn
	 * @param string $message Message to send
	 * @param string $responseFormat If specified, converts response to
	 *                               specified format
	 * @return mixed API response
	 * @throws BadMethodCallException
	 * @throws LengthException
	 * @throws Acotel_MSISDN_Exceptions_InvalidMSISDNException
	 */
	public function send($msisdn, $message, $responseFormat = 'raw') {
		if (!$msisdn) {
			throw new InvalidArgumentException("MSISDN not informed.");
		}

		$message = mb_substr($message, 0, self::MESSAGE_MAX_LENGTH, 'UTF-8');
		$message = $this->stripSpecialChars($message);

		if ($msisdn instanceof Acotel_MSISDN_MSISDN) {
			$send_to = $msisdn->getNumber();
		} else {
			if (!Acotel_MSISDN_MSISDN::validateMSISDN($msisdn)) {
				throw new Acotel_MSISDN_Exceptions_InvalidMSISDNException();
			}
			$send_to = $msisdn;
		}

		if (!array_key_exists('cfs', $this->options)) {
			throw new BadMethodCallException('CFS not informed.');
		}

		if (!array_key_exists('sender', $this->options)) {
			throw new BadMethodCallException('Sender not informed.');
		}

		$body = $this->buildRequestBody($send_to, $message);
		$this->request->setBody($body);

		return $this->getResponse($responseFormat);
	}

	private function stripSpecialChars($message) {
		$patterns = array(
			'/[áäàãâ]/u',
			'/[ÁÄÀÃÂ]/u',
			'/[éëèê]/u',
			'/[ÉËÈÊ]/u',
			'/[íìî]/u',
			'/[ÍÌÎ]/u',
			'/[óöòõô]/u',
			'/[ÓÖÒÕÔ]/u',
			'/[úüùû]/u',
			'/[ÚÜÙÛ]/u',
			'/ç/u',
			'/Ç/u',
			'/ñ/u',
			'/Ñ/u',
			'/\$/u',
			'/%/u',
			'/\+/u',
		);

		$replacements = array(
			'a',
			'A',
			'e',
			'E',
			'i',
			'I',
			'o',
			'O',
			'u',
			'U',
			'c',
			'C',
			'n',
			'N',
			'S',
			'%25',
			'%2b',
		);

		return preg_replace($patterns, $replacements, $message);
	}

	private function buildRequestBody($msisdn, $message) {
		$opt = $this->options;
		return "<?xml version=\"1.0\"?>"
			. "<!DOCTYPE message SYSTEM \"/home/jinny/init/jxml_sai.dtd\">"
			. "<message>"
			. "<submit>"
			. "<application_type>" . $opt['cfs'] ."/0</application_type>"
			. "<da>"
			. "<number>" . $msisdn . "</number>"
			. "</da>"
			. "<oa>"
			. "<number>" . $opt['sender'] . "</number>"
			. "</oa>"
			. "<group_name>" . $opt['group_name'] . "</group_name>"
			. "<ud>"
			. "<text><![CDATA[" . $message . "]]></text>"
			. "</ud>"
			. "<pid>0</pid>"
			. "</submit>"
			. "</message>";
	}
}
?>
