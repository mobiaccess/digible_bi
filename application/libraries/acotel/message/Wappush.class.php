<?php

class Acotel_Message_Wappush {

	private $plexIp;
	private $plexGroup;
	private $connectionTimeout;
	private $msisdn;
	private $sender;
	private $cfs;
	private $text;
	private $link;
	private $originalLink;
	private $request;
	private $response;

	public function __construct(){

	}

	public function setPlexIp($plexIp){
		$this->plexIp = $plexIp;
	}

	public function setPlexGroup($plexGroup){
		$this->plexGroup = $plexGroup;
	}

	public function setConnectionTimeoutInSeconds($timeout){
		$this->connectionTimeout = $timeout;
	}

	public function setMsisdn($msisdn){
		$this->msisdn = $msisdn;
	}

	public function setSender($sender){
		$this->sender = $sender;
	}

	public function setCodeForStatistics($cfs){
		$this->cfs = $cfs;
	}

	public function setText($text){
		$this->text = $text;
	}

	public function setLink($link){
		$this->link = htmlspecialchars( urldecode($link) );
		$this->originalLink = $link;
	}

	public function getResponse(){
		return $this->response;
	}

	public function getRequest(){
		return $this->request;
	}

	private function setRequest(){

		$DateNow	= date('YmdHis');
		$ExpTime	= date('Y-m-d\TH:i:s\Z', strtotime('+1 year'));
		$DateSI		= date('is');

		$XML  = "--asdlfkjiurwghasf\r\n";
		$XML .= "Content-Type: application/xml\r\n\r\n";
		$XML .= "<?xml version=\"1.0\"?>\r\n";
		$XML .= "<!DOCTYPE pap PUBLIC \"-//WAPFORUM//DTD PAP 1.0//EN\"
			\"http://www.wapforum.org/DTD/pap_1.0.dtd\">\r\n";
		$XML .= "<pap>\r\n";
		$XML .= "	<push-message push-id=\"".$DateNow."\"
					source-reference=\"{$this->sender}/{$this->cfs}/{$this->plexGroup}\"
					progress-notes-requested=\"false\">\r\n";

		$XML .= "		<address address-value=\"wappush={$this->msisdn}/TYPE=PLMN@im.jinny.ie\">
					</address>\r\n";

		$XML .= "	</push-message>\r\n";
		$XML .= "	<additional_parameters>\r\n";
		$XML .= "		<spservice></spservice>\r\n";
		$XML .= "		<session_id></session_id>\r\n";
		$XML .= "		<group_name>{$this->plexGroup}</group_name>\r\n";
		$XML .= "		<operator_id></operator_id>\r\n";
		$XML .= "		<sender_id>{$this->sender}</sender_id>\r\n";
		$XML .= "		<tariff></tariff>\r\n";
		$XML .= "		<cost></cost>\r\n";
		$XML .= "	</additional_parameters>\r\n";
		$XML .= "</pap>\r\n\r\n";

		$XML .= "--asdlfkjiurwghasf\r\n";
		$XML .= "Content-Type: text/vnd.wap.si\r\n";
		$XML .= "X-Wap-Initiator-URI: testroof\r\n\r\n";
		$XML .= "<?xml version=\"1.0\"?>\r\n";
		$XML .= "<!DOCTYPE si PUBLIC \"-//WAPFORUM//DTD SI 1.0//EN\" \"http://www.wapforum.org/DTD/si.dtd\">\r\n";
		$XML .= "<si>\r\n";
		$XML .= "	<indication href=\"{$this->link}\" si-id=\"".$DateSI."\"
				si-expires=\"".$ExpTime."\" action=\"signal-high\">{$this->text}</indication>\r\n";
		$XML .= "</si>\r\n";
		$XML .= "--asdlfkjiurwghasf--";

		//$MsgToPost  = "POST /cgi-bin/ppg_cgix HTTP/1.1\r\n"; // Plex
		$MsgToPost  = "POST /fcgi-bin/jar_pap_sai.fcgi HTTP/1.1\r\n"; // JMR
		$MsgToPost .= "Content-Length: " . (strlen($XML)) . "\r\n";
		$MsgToPost .= "Content-Type: multipart/related; type=\"application/xml\"; boundary=\"asdlfkjiurwghasf\"\r\n";
		$MsgToPost .= "Host: 127.0.0.1\r\n";
		$MsgToPost .= "Connection: close\r\n\r\n";
		$MsgToPost .= $XML;

		$this->request = $MsgToPost;
	}

	public function send(){
		$this->setRequest();
		$this->execute();
	}

	private function execute(){

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->plexIp);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->connectionTimeout);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->request);

		$data = curl_exec($ch);
		$this->response = $data;

		if (curl_errno($ch)) {
			throw new Exception( curl_error($ch) );
		} else {
			curl_close($ch);
		}

	}

}

?>
