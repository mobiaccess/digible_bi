<?php

/**
 *
 * @package Acotel
 * @subpackage Template
 *
 * @author Thiago Costa <thiago.costa@acotel.com>
 * @example example.php
 * @source
 */ 

class Acotel_Template_TemplateManager {

	protected $file;

	public function __construct($file){
		$this->file = $file;
	}

	/**
	 * Retorna o conteúdo do template processado
	 * @param string $file
	 * @return string
	 */ 
	public function get($file=""){
		return $this->compile($file);
	}

	/**
	 * Exibe o conteúdo do template processado
	 * @param string $file
	 */ 
	public function display($file=""){
		echo $this->compile($file);
	}

	/**
	 * Compila o template
	 * @param string $file
	 */  
	private function compile($file=""){
		
		$tplFile = $file == "" ? $this->file : $file;
		
		ob_start();
		extract(get_object_vars($this));
		include $tplFile;

		return ob_get_clean();

	}

	/**
	 * Monta o header XML
	 * Caso nenhum parâmetro seja passado, o header será criado com versão = 1.0
	 * e encoding = UTF-8
	 * @param string $version
	 * @param string $encoding
	 * @return string
	 */ 
	public function displayHeader($version='1.0', $encoding='UTF-8'){
		return '<?xml version="' . $version . '" encoding="' . $encoding . '"?>';
	}

}

?>
