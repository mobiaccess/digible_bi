<?php

$current_dir = dirname(__FILE__);
require(dirname($current_dir) . '/TemplateManager.class.php');

$tpl_file = $current_dir . '/example.tpl.php';

$items = array('Item 1', 'Item 2', 'Item 3');

$tpl = new Acotel_Template_TemplateManager($tpl_file);

$tpl->username = 'Fulaninho';
$tpl->email = 'fulano@acotel.com';
$tpl->items = $items;

$tpl->display();

?>
