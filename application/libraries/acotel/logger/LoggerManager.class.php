<?php
/**
 * Logger manager
 *
 * @package Acotel
 * @subpackage Logger
 * @author Thiago Costa <tcosta>
 */

/**
 * Class to manage Acotel_Logger_Logger instances.
 *
 * @package Acotel
 * @subpackage Logger
 * @author Thiago costa <tcosta>
 * @see Acotel_Logger_Logger
 */
class Acotel_Logger_LoggerManager {
	/**
	 * Manager (self) instance
	 * @var Acotel_Logger_LoggerManager
	 */
	private static $instance;

	/**
	 * List of active loggers
	 * @var array
	 */
	private $loggers = array();

	/**
	 * Unique identifier for this execution
	 * @var string
	 */
	private $uniqID = null;

	private function __construct() {
		$this->uniqID = $this->generateUniqID();
	}

	/**
	 * Returns an instance of logger manager
	 *
	 * @return Acotel_Logger_LoggerManager
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Set custom uniq ID (token)
	 * @param string $uniqID
	 */
	public function setUniqID($uniqID){
		$this->uniqID = $uniqID;
	}

	/**
	 * Returns the unique identifier for this manager
	 *
	 * @return string
	 */
	public function getUniqID() {
		return $this->uniqID;
	}

	/**
	 * @param string $ident
	 * @throws Acotel_Exceptions_ConfigurationNotFoundException
	 * @throws Acotel_Exceptions_IdentNotFoundException
	 */
	private function getLoggerConfig($ident) {
		$logger = Acotel_Config::get('logger');
		if (!$logger) {
			throw new Acotel_Exceptions_ConfigurationNotFoundException();
		}

		if (empty($logger[$ident])) {
			throw new Acotel_Exceptions_IdentNotFoundException();
		}

		return $logger[$ident];
	}

	private function parseIdent($ident) {
		return trim($ident);
	}

	private function createNewLogger($ident, $loggerClass) {
		$config = $this->getLoggerConfig($ident);
		$level = $config['level'];
		$path = $config['path'];
		if (isset($config['filename'])) {
			$filename = $config['filename'];
		} else {
			$filename = str_replace('-', '_', $ident);
		}
		$Logger = new $loggerClass($level, $path, $filename, $this->uniqID);
		$this->loggers[$ident] = $Logger;
	}

	/**
	 * Return a Acotel_Logger_Logger instance (or its subclass). If the instance
	 * for specified $ident doesn't exists, you can specify an class on
	 * $loggerClass. Otherwise, the instance already created will be returned.
	 *
	 * @param string $ident Logger indentifier
	 * @param string $loggerClass Name of logger class (must be or subclass
	 *                            Acotel_Logger_Logger)
	 * @return Acotel_Logger_Logger
	 * @throws LogicException
	 */
	public function getLogger($ident, $loggerClass = 'Acotel_Logger_Logger') {
		if ($loggerClass != 'Acotel_Logger_Logger'
			&& !is_subclass_of($loggerClass, 'Acotel_Logger_Logger')
		) {
			$msg = "'$loggerClass' must be subclass of Acotel_Logger_Logger";
			throw new LogicException($msg);
		}

		$ident = $this->parseIdent($ident);
		if (!isset($this->loggers[$ident])) {
			$this->createNewLogger($ident, $loggerClass);
		}
		return $this->loggers[$ident];
	}

	/**
	 * Generates an unique identifier that stays the same durign all execution.
	 *
	 * @return string
	 */
	private function generateUniqID() {
		$_str = "";
		$_randlen_string = 6;
		$_randlen_num = 4;

		$charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$charset_len = strlen($charset) - 1;
		for ($i = 0; $i < $_randlen_string; $i++) {
			$_str .= $charset[mt_rand(0, $charset_len)];
		}

		$charset = "0123456789";
		$charset_len = strlen($charset) - 1;
		for ($i = 0; $i < $_randlen_num; $i++) {
			$_str .= $charset[mt_rand(0, $charset_len)];
		}

		return strtoupper($_str);
	}

	public function getListOfActiveLoggers() {
		return $this->loggers;
	}
}
?>
