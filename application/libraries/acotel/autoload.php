<?php
/**
 * Autoload for Acotel_* classes
 *
 * @package Acotel
 */

/**
 * Autoload for Acotel_* classes
 *
 * @param string $classname
 */
function acotel_autoload($classname) {
    
	if (strpos($classname, 'Acotel_') !== 0) {
		return;
	}

	$dir = dirname(__FILE__);
	$classname = str_replace('Acotel_', '', $classname);
	$pos = strrpos($classname, '_');
	if ($pos === false) {
		$file = "$dir/$classname.class.php";
	} else {
		$package = strtolower(substr($classname, 0, $pos));
		$class = substr($classname, $pos + 1);

		$file = "$dir/" . str_replace('_', '/', $package) . "/$class.class.php";
	}

        require $file;
}
spl_autoload_register('acotel_autoload');

$path_pear_dependecies = dirname(__FILE__) . '/pear_dependencies';
set_include_path(get_include_path() . PATH_SEPARATOR . $path_pear_dependecies);
?>
