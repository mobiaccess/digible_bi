<?php
/**
 * Storage for config variables
 *
 * @package Acotel
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */

/**
 * Storage for config variables
 *
 * @package Acotel
 * @author Rodrigo Machado <rodrigo.machado@acotel.com>
 */
class Acotel_Config {
	/**
	 * Storage for variables
	 * @var array
	 */
	private static $config = array();

	/**
	 * Set '$value' for configuration '$name'. If '$value' is an array, and the
	 * value stored on key '$name' is an array, the value is merged on stored
	 * configuration. Otherwise, the value overwrites previous stored config.
	 *
	 * @param string $name Name (key) to identify the value
	 * @param mixed $value
	 */
	public static function set($name, $value) {
		if (array_key_exists($name, self::$config)
			&& is_array(self::$config[$name])
			&& is_array($value)
		) {
			self::$config[$name] = array_merge(self::$config[$name], $value);
		} else {
			self::$config[$name] = $value;
		}
	}

	/**
	 * Retrieve value associated with key '$name'.
	 *
	 * @param string $name Name (key) used to store the value
	 * @return mixed Value associated with key '$name'
	 */
	public static function get($name) {
		return self::$config[$name];
	}

	/**
	 * Retrieve all configuration data
	 *
	 * @return array
	 */
	public static function get_all() {
		return self::$config;
	}
}
?>
