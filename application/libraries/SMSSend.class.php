<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class SMSSend{
		static public function send($config, $msisdn, $mt, &$pLogger=null){
			if($pLogger){
				$pLogger->info('Enviando MT...');
				$pLogger->debug('Msisdn['.print_r($msisdn,true)."]");
			}
			switch ($config['name']){
				case 'gateway':
						GatewaySMS::send($config, $msisdn, $mt, $pLogger);
					break;
				
                                 case 'jmr': 
					JmrSMS::send($config, $msisdn, $mt, $pLogger);
					break;
                                    
				 default:
					NewPlex::send($config, $msisdn, $mt, $pLogger);
					break;
				
			}
		}
	}
?>
