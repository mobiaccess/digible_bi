<?php

class FTPConnection {

    private $Debug;
    private $debugLevel = 1;
    private $ssl = false;
    private $passive = false;
    private $timeout = 30;
    private $maxAttempt = 3;
    private $port = 21;
    private $fullPath;
    private $server;
    private $username;
    private $password;
    private $connid;

    public function __construct() {
        
    }

    public function useSecureConnection($ssl) {

        if ($ssl) {
            $this->ssl = $ssl;
        }
    }

    public function behindFirewall($passive) {

        if ($passive) {
            $this->passive = $passive;
        }
    }

    public function setPort($port) {

        if ($port && is_numeric($port)) {
            $this->port = $port;
        }
    }

    public function setTimeout($timeout) {

        if ($timeout) {
            $this->timeout = $timeout;
        }
    }

    public function setConfig($server, $username, $password) {

        if (!$connid = $this->connect($server, $username, $password)) {
            return false;
        }

        if (!$this->login($connid, $username, $password)) {
            return false;
        }

        $this->fullPath = @ftp_pwd($connid);
        $this->server = $server;
        $this->username = $username;
        $this->password = $password;
        $this->connid = $connid;

        //print $this->fullPath;
        return true;
    }

    private function login($connid, $username, $password) {

        if (!ftp_login($connid, $username, $password)) {
            return false;
        }

        return true;
    }

    private function connect($server, $username, $password) {

        if ($this->ssl) {
            if (!$connid = @ftp_ssl_connect($server, $this->port, $this->timeout)) {
                return false;
            }
        } else {
            if (!$connid = @ftp_connect($server, $this->port, $this->timeout)) {
                return false;
            }
        }

        return $connid;
    }

    public function isFile($file) {
        if (@ftp_chdir($this->connid, $this->fullPath . $file)) {
            return false;
        }

        return true;
    }

    public function listContent($path = "") {

        if (!$path) {
            $path = $this->fullPath;
        }

        if (!$content = @ftp_nlist($this->connid, $path)) {
            return false;
        }

        return $content;
    }
    
    public function listFile($path = "") {

        if (!$path) {
            $path = $this->fullPath;
        }
        
        $content = ftp_nlist($this->connid, $path);
        
        if(!$content){
            return false;
        }
        
        if( is_array($content) ) {
            foreach( $content as $file){
                if( substr($file,-1)!="." ){
                    $return[] = $file;
                }
            }
            return $return;
        }else{
            return false;
        }
    }
    
    /** Este metodo explora todos os niveis de diretorio
     * 
     * @param type $path
     * @return boolean
     */
    public function listFileRecursive($path = "") {

        if (!$path) {
            $path = $this->fullPath;
        }
        
        $content = ftp_nlist($this->connid, $path);
        
        
        
        if(!$content){
            return false;
        }
        
        if( is_array($content) ) {
            
            $return = array();
            
            foreach( $content as $file){
                
                if(!$this->isFile($file)){
                    
                    $result = $this->listFileRecursive($path . $file);
                    
                    if(is_array($result)){
                        $return = array_merge($return, $result);
                    }
                    
                }else{
                    
                    if( substr($file,-1)!="." ){
                        $return[] = $file;
                    }
                
                    
                }
                
            }
            return $return;
        }else{
            return false;
        }
    }
    
    public function delete($file, $recursive = false) {

        if (@ftp_chdir($this->connid, $file)) {
            if (!@ftp_rmdir($this->connid, $file)) {
                return false;
            }

            return true;
        }


        if (!@ftp_delete($this->connid, $file)) {
            return false;
        }

        return true;
    }

    /* public function delete( $file ){

      //if( @ftp_chdir( $this->connid, $this->fullPath . $file ) ){
      if( @ftp_chdir( $this->connid, $file ) ){

      $status = true;

      $files = @ftp_nlist( $this->connid, "." );
      if( $files ){
      if( sizeof( $files ) > 0 ){

      foreach( $files as $file ){
      if( !@ftp_delete( $this->connid, $file ) ){
      $status = false;
      break;
      }

      }
      }
      }

      return true;

      }

      if( !@ftp_delete( $this->connid, $file ) ){
      return false;
      }

      return true;
      } */

    public function readFileContent($file, $asArray = false) {

        $nfile = "ftp://" . $this->username . ":" . $this->password . "@" . $this->server . "/" . $file;

        $File = new File();
        if (!$content = $File->read($nfile, $asArray)) {
            return false;
        }

        return $content;
    }

    public function upload($source, $destination, $recursive = false) {

        $File = new File();
        $basename = $File->getBasename($source);

        $dest = $destination . "/" . $basename;

        $handleCount = 0;
        do {
            $uploadStatus = ftp_put($this->connid, $dest, $source, FTP_BINARY);
            $handleCount++;
        } while ((!$uploadStatus ) && ( $handleCount < $this->maxAttempt ));

        if (!$uploadStatus) {
            return false;
        }

        return $dest;
    }
    
    public function uploadFile($source, $remote_file) {

        $handleCount = 0;

        do {
            $uploadStatus = ftp_put($this->connid, $remote_file, $source, FTP_BINARY);
            $handleCount++;
        } while ((!$uploadStatus ) && ( $handleCount < $this->maxAttempt ));

        if (!$uploadStatus) {
            throw new Exception("Could not send data from file: $source.");
        }

        return $remote_file;
    }

    public function renameFile($oldname, $newname) {

        $handleCount = 0;
        
        if(!ftp_rename($this->connid, $oldname, $newname)){
            return false;
        }
        
        return true;
    }

    public function getFileSize($file) {

        $res = ftp_size($this->connid, $file);
        
        if($res != -1){
            return $res;
        }
        
        throw new Exception("couldn't get the size");
    }

    public function download($source, $destination, $recursive = false) {

        $File = new File();
        $basename = $File->getBasename($source);

        if (!$destination) {

            $destination = getcwd();
        } else {

            if (!$File->exists($destination)) {
                if (!@mkdir($destination, 0777, true)) {
                    return false;
                }
            }
        }

        $dest = $destination . "/" . $basename;

        $handleCount = 0;
        do {
            $downloadStatus = ftp_get($this->connid, $dest, $source, FTP_BINARY);
            $handleCount++;
        } while ((!$downloadStatus ) && ( $handleCount < $this->maxAttempt ));

        if (!$downloadStatus) {
            return false;
        }

        return $dest;
    }
    
    /** Realiza o download lhe dando um nome unico
     * 
     * @param type $source
     * @param type $destination
     * @return string|boolean
     */
    public function downloadWithUniqueName($source, $destination) {

        $File = new File();
        $basename = $File->getBasename($source);

        if (!$destination) {

            $destination = getcwd();
        } else {

            if (!$File->exists($destination)) {
                if (!@mkdir($destination, 0777, true)) {
                    return false;
                }
            }
        }

        $dest = $destination . "/" . uniqid() . "_" . $basename;

        $handleCount = 0;
        do {
            $downloadStatus = ftp_get($this->connid, $dest, $source, FTP_BINARY);
            $handleCount++;
        } while ((!$downloadStatus ) && ( $handleCount < $this->maxAttempt ));

        if (!$downloadStatus) {
            return false;
        }

        return $dest;
    }
    
    /* public function download( $source, $destination, $createdir=true ){

      $destination = trim( $destination );
      if( substr( $destination, -1, strlen( $destination ) ) != "/" ){
      $destination = $destination . "/";
      }

      $File = new File();
      if( !$File->exists( $destination ) ){
      if( !$createdir ){
      return false;
      }

      if( !mkdir( $destination, 0777, true ) ){
      return false;
      }
      }

      $aSource = array( $source );
      if( @ftp_chdir( $this->connid, $source ) ){
      $aSource = array();
      if( $files = @ftp_nlist( $this->connid, "." ) ){
      $aSource = $files;
      }
      }

      $status = true;
      foreach( $aSource as $src ){

      ftp_chmod( $this->connid, 0777, $src );

      //if( !ftp_get( $this->connid, $destination . $src, $this->fullPath . $source . $src, FTP_BINARY ) ){
      if( !ftp_get( $this->connid, $destination . $src, $source . $src, FTP_BINARY ) ){
      $status = false;
      break;
      }
      }

      return $status;
      } */

    /* public function upload( $srcPath, $dstPath ){

      if( !@ftp_chdir( $this->connid, $dstPath ) ){
      if( !ftp_mkdir( $this->connid, $dstPath ) ){
      return false;
      }
      }

      $aSrcDir = dir( $srcPath );
      $status = true;
      while( $file = $aSrcDir->read() ){

      if( $file != "." && $file != ".." ){

      $source = $srcPath . "/" . $file;
      $destination = $dstPath . "/" . $file;

      if( is_dir( $source ) ){

      if( !@ftp_chdir( $this->connid, $destination ) ){

      if( !@ftp_mkdir( $this->connid, $destination ) ){
      $status = false;
      break;
      }
      }

      $this->download( $source, $destination );

      } else {

      if( !@ftp_put( $this->connid, $destination, $source, FTP_BINARY ) ){
      $status = false;
      break;
      }
      }
      }
      }

      return $status;

      } */

    public function disconnect() {

        if (!$this->connid) {
            return false;
        }

        if (!@ftp_close($this->connid)) {
            return false;
        }

        return true;
    }

    public function createDir($dirname) {

        if (ftp_mkdir($this->connid, $dirname)) {
            return false;
        }

        return true;
    }

    public function __destruct() {

        $this->disconnect();
    }

}

?>
