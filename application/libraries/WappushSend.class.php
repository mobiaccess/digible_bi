<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class WappushSend{
        static public function send($pConfig, $pMsisdn, $pSender, $pUrl, $pMessage, &$pLogger=null){
            if($pLogger){
                $pLogger->info('['.__METHOD__.']Start');
                $pLogger->info('['.__METHOD__.']Enviando WapPush...');
                $pLogger->debug('['.__METHOD__.']msisdn['.print_r($pMsisdn,true)."]");
            }

            try {
                $Wappush = new Acotel_Message_Wappush();
                $Wappush->setPlexIp($pConfig['plex_ip']);
                $Wappush->setPlexGroup($pConfig['plex_group']);
                $Wappush->setConnectionTimeoutInSeconds($pConfig['connection_timeout']);
                $Wappush->setMsisdn($pMsisdn);
                $Wappush->setSender($pSender);
                $Wappush->setCodeForStatistics('AW00');
                $Wappush->setLink($pUrl);
                $Wappush->setText($pMessage);

                if($pLogger){
                    $pLogger->debug('['.__METHOD__.']Wappush[' . print_r($Wappush, true).']');
                    $pLogger->debug('['.__METHOD__.']Wappush[' . print_r($Wappush, true).']');
                }

                $Wappush->send();
            } catch (Exception $e) {
                if($pLogger){
                    $pLogger->error('['.__METHOD__.']['.$e->getCode().']'.$e->getMessage() . "\n" . $e->getTraceAsString());
                }
                return false;
            }

            if($pLogger){
                $pLogger->info('['.__METHOD__.']Request: ' . $Wappush->getRequest());
                $pLogger->info('['.__METHOD__.']Response: ' . $Wappush->getResponse());
                $pLogger->info('['.__METHOD__.']End');
            }
            return true;
        }
    }
?>