<?php

/**
 *
 *@author Fernando Siggelkow <fernando.siggelkow@mobiaccess.com.br>
 *@since 2015-06-21 
 */

class Filecdr {

    // private $CI;

    // public function __construct() {
    //     $this->CI =& get_instance();
    // }

    /**
     * $filename is a full path
     * $arrContentFile is rows to CDR
     */
    public function writeFile($filename, $arrContentFile, $Logger) {

        // Check if file exists
        if (file_exists($filename)) {
            $Logger->info("Arquivo ($filename) já criado!");
            return true;
        }

        if (!$handle = fopen($filename, 'a')) {
            $Logger->error("Não foi possível abrir o arquivo ($filename)");
            return false;
        }

        foreach($arrContentFile as $row) {
            $conteudo = implode(";", $row);
            $conteudo = $conteudo . "\n";
            //print_r($conteudo);
            if (fwrite($handle, $conteudo) === FALSE) {
                $Logger->error("Não foi possível escrever no arquivo ($filename)");
                return false;
            }
        }

        fclose($handle);
        $Logger->info('Done.');
        return true;
    }


}