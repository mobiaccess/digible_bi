<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class JmrSMS{

        static public function send($config, $msisdn, $mt, &$pLogger=null){
            $jmr = new Acotel_Message_SMS($config);
            $response = array();
            if($pLogger){
                $pLogger->debug('Message['.$mt.']');
            }
            if(is_array($msisdn)){
                foreach ($msisdn AS $val){
                    $response[] = $jmr->send($val, $mt);
                }
            }else{
                $response[] = $jmr->send($msisdn, $mt);
            }
            if($pLogger){
                $pLogger->debug('JMR Response: '.print_r($response, true));
            }
        }

    }
?>