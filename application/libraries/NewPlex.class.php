<?php

/**
 * NewPlex class
 *
 * @author Patrick Sava
 * @version 2.0
 */
require_once(APPPATH . 'libraries/SFTPConnection.class.php');

class NewPlex {
    # Local paths attributes;

    private $rawPath;
    private $bakPath;
    private $badPath;
    private $loadingPath;

    # FTP data;
    private $ftpHostname;
    private $ftpPost;
    private $ftpUsername;
    private $ftpPassword;
    private $ftpPath;

    # NewPlex Context
    private $ctxGroup;
    private $ctxRSR;

    # Application Specific atributes;
    private $appName;
    private $appId;
    private $appCfs;
    private $appValidPeriod;
    private $appSender;
    private $plexPostUrl;

    # This Logger Object is an instance of Acotel_Logger_Logger class;
    private $Logger;

    public function __construct($newPlexConfig, &$Logger) {
        $Logger->info("Creating an instance of NewPlex SMS");
        $Logger->info("[NewPlex Constructor]PARAMS \n" . print_r(func_get_args(), true));

        try {
            # Setting localpaths data
            $this->rawPath = $newPlexConfig['raw_path'];
            $this->bakPath = $newPlexConfig['bak_path'];
            $this->badPath = $newPlexConfig['bad_path'];
            $this->loadingPath = $newPlexConfig['loading_path'];

            # Setting ftp data
            $this->ftpHostname = $newPlexConfig['ftp_hostname'];
            $this->ftpPort = $newPlexConfig['ftp_port'];
            $this->ftpUsername = $newPlexConfig['ftp_username'];
            $this->ftpPassword = $newPlexConfig['ftp_password'];
            $this->ftpPath = $newPlexConfig['ftp_path'];

            # Setting NewPlex context
            $this->ctxGroup = $newPlexConfig['ctx_group'];
            $this->ctxRSR = $newPlexConfig['ctx_rsr'];
            $this->ctxFileIndex = 1; //Indicates the order of the created files;
            # Setting Application Specifics
            $this->appName = $newPlexConfig['app_name'];
            $this->appId = $newPlexConfig['app_id'];
            $this->appCfs = $newPlexConfig['app_cfs'];
            $this->appText = $newPlexConfig['app_text'];
            $this->appValidPeriod = $newPlexConfig['app_valid_period'];
            $this->appSender = $newPlexConfig['app_sender'];

            $this->Logger = $Logger;
        } catch (Exception $ex) {
            $Logger->error("Failed to create an instance, some parameters are missing");
            throw new Exception("Failed to create an instance of NewPlex");
        }
    }

    /* Getters and Setters; */

    public function getAppName() {
        return $this->appName;
    }

    public function getAppId() {
        return $this->appId;
    }

    public function getAppCfs() {
        return $this->appCfs;
    }

    public function getAppText() {
        return $this->appText;
    }

    public function getAppValidPeriod() {
        return $this->appValidPeriod;
    }

    public function getAppSender() {
        return $this->appSender;
    }

    public function getFtpPath() {
        return $this->ftpPath;
    }

    public function getCtxGroup() {
        return $this->ctxGroup;
    }
    
    public function getPlexPostUrl() {
        return $this->plexPostUrl;
    }

    public function setAppName($appName) {
        $this->appName = $appName;
    }

    public function setAppId($appId) {
        $this->appId = $appId;
    }

    public function setAppCfs($appCfs) {
        $this->appCfs = $appCfs;
    }

    public function setAppText($appText) {
        $this->appText = $appText;
    }

    public function setAppValidPeriod($appValidPeriod) {
        $this->appValidPeriod = $appValidPeriod;
    }

    public function setAppSender($appSender) {
        $this->appSender = $appSender;
    }

    public function setFtpPath($path) {
        $this->ftpPath = $path;
    }
    
    public function setCtxGroup($ctxGroup) {
        $this->ctxGroup = $ctxGroup;
    }

    public function setPlexPostUrl($plexPostUrl) {
        $this->plexPostUrl = $plexPostUrl;
    }

    /* Getters and Setters; */

    public function createFile($arrMsisdn) {
        $this->Logger->info("[" . __METHOD__ . "]START");
        $this->Logger->debug("[" . __METHOD__ . "]Msisdns: \n" . print_r($arrMsisdn, true));
        try {
            if (!is_array($arrMsisdn)) {
                throw new Exception("Invalid Parameter");
            }
            $qtd_customers = count($arrMsisdn);
            $this->Logger->info("[" . __METHOD__ . "]Count of msisdns inside the given array:[$qtd_customers]");
            $data = date('YmdHis');

            $filename = $this->appName . '_' . $this->appId . '_' . $qtd_customers . '_' . $data . '_' . $this->ctxFileIndex . '.txt.tmp';
            $this->Logger->info("[" . __METHOD__ . "]Filename:[$filename]");

            $conteudo = $this->setTemplate($arrMsisdn);

            $this->Logger->info("[" . __METHOD__ . "]Creating the file");
            if (!$file = fopen($this->rawPath . $filename, 'w')) {
                // Throws an Exception if something go wrong while creating the file.
                throw new Exception("[" . __METHOD__ . "]ERROR 1: Failed to create the file.");
            }
            $this->Logger->info("[" . __METHOD__ . "]File created");
            $this->Logger->info("[" . __METHOD__ . "]Writing the file");
            if (fwrite($file, $conteudo) === FALSE) {
                // Throws an Exception if something go wrong while writing the file.
                throw new Exception("[" . __METHOD__ . "]ERROR 2: Failed to write the content inside the created file.");
            }
            $this->Logger->info("[" . __METHOD__ . "]File written");

            $this->ctxFileIndex++;
            $this->Logger->info("[" . __METHOD__ . "]File " . $filename . " created OK.");
            fclose($file);

            $this->Logger->info("[" . __METHOD__ . "]END");
            return $filename;
        } catch (Exception $ex) {
            $this->Logger->error("[" . __METHOD__ . "]" . $ex->getMessage());
            $this->Logger->error("[" . __METHOD__ . "]" . $ex->getTraceAsString());
            $this->Logger->error("[" . __METHOD__ . "]END");
            return false;
        }
    }

    private function setTemplate($arrMsisdn) {
        $this->Logger->info("[" . __METHOD__ . "]START");
        $conteudo = "---BEGIN_SMS---\r\n";
        $conteudo .= "GRP:$this->ctxGroup\r\n";
        $conteudo .= "SRC:$this->appSender\r\n";
        $conteudo .= "RSR:$this->ctxRSR\r\n";

        if (!empty($this->appValidPeriod)) {
            $conteudo .= "VP:$this->appValidPeriod\r\n";
        }

        $conteudo .= "AT:$this->appCfs\r\n";
        $conteudo .= "---BEGIN_TEXT---\r\n";
        $conteudo .= $this->appText . "\r\n";
        $conteudo .= "---END_TEXT---\r\n";
        $conteudo .= "---END_SMS---\r\n";

        $this->Logger->info("[" . __METHOD__ . "]File content template header:\n" . $conteudo);
        $this->Logger->info("[" . __METHOD__ . "]Writing the msisdns");

        $conteudo .= "---BEGIN_DST---\r\n";

        foreach ($arrMsisdn as $msisdn) {
            if (is_array($msisdn)) {
                // Handles bidimensional arrays
                $conteudo .= $msisdn['msisdn'] . "\r\n";
            } else {
                // Handles a unidimensional array
                $conteudo .= "$msisdn\r\n";
            }
        }

        $conteudo .= "---END_DST---";

        $this->Logger->info("[" . __METHOD__ . "]END");
        return $conteudo;
    }

    public function sendFile($filename) {
        $this->Logger->info("[" . __METHOD__ . "]START");
        $this->Logger->info("[" . __METHOD__ . "]PARAMS" . print_r(func_get_args(), true));

        try {
            $this->Logger->info("[" . __METHOD__ . "]Connecting to " . $this->ftpHostname . ":" . $this->ftpPort);
            $sftp = new SFTPConnection($this->ftpHostname, $this->ftpPort);
            $this->Logger->info("[" . __METHOD__ . "]Logging...");
            $sftp->login($this->ftpUsername, $this->ftpPassword);

            $this->Logger->info("[" . __METHOD__ . "]Uploading the file to newplex server...");
            if ($sftp->uploadFiles($this->rawPath . $filename, $this->ftpPath . $filename)) {
                $this->Logger->info("[" . __METHOD__ . "]File uploaded");
                $this->moveFiles($filename);
                $this->Logger->info("[" . __METHOD__ . "]File moved from RAW to BAK local directory");
                $newName = str_replace('.tmp', '', $filename);
                $this->Logger->info("[" . __METHOD__ . "]Renaming file in sftp server");
                $sftp->renameFile($this->ftpPath, $filename, $newName);
                $this->Logger->info("[" . __METHOD__ . "]Old file name: $filename");
                $this->Logger->info("[" . __METHOD__ . "]New file name: $newName");
            }

            $this->Logger->info("[" . __METHOD__ . "]File successfully transfered!");
            $this->Logger->info("[" . __METHOD__ . "]END");
            return true;
        } catch (Exception $ex) {
            $this->Logger->info("[" . __METHOD__ . "]" . $ex->getTraceAsString());
            $this->Logger->info("[" . __METHOD__ . "]ERROR 3: Failed to upload file.");
            $this->Logger->info("[" . __METHOD__ . "]END");
            return false;
        }
    }

    public function sendFiles($filenames) {
        $this->Logger->info("[" . __METHOD__ . "]START");
        $this->Logger->info("[" . __METHOD__ . "]PARAMS" . print_r(func_get_args(), true));

        try {
            $this->Logger->info("[" . __METHOD__ . "]Connecting to " . $this->ftpHostname . ":" . $this->ftpPort);
            $sftp = new SFTPConnection($this->ftpHostname, $this->ftpPort);
            $this->Logger->info("[" . __METHOD__ . "]Logging...");
            $sftp->login($this->ftpUsername, $this->ftpPassword);

            foreach ($filenames as $filename) {
                $this->Logger->info("[" . __METHOD__ . "]Uploading the file to newplex server...");
                if ($sftp->uploadFiles($this->rawPath . $filename, $this->ftpPath . $filename)) {
                    $this->Logger->info("[" . __METHOD__ . "]File uploaded");
                    $this->moveFiles($filename);
                    $this->Logger->info("[" . __METHOD__ . "]File moved from RAW to BAK local directory");
                    $newName = str_replace('.tmp', '', $filename);
                    $this->Logger->info("[" . __METHOD__ . "]Renaming file in sftp server");
                    $sftp->renameFile($this->ftpPath, $filename, $newName);
                    $this->Logger->info("[" . __METHOD__ . "]Old file name: $filename");
                    $this->Logger->info("[" . __METHOD__ . "]New file name: $newName");
                }
            }
            $this->Logger->info("[" . __METHOD__ . "]File successfully transfered!");
            $this->Logger->info("[" . __METHOD__ . "]END");
            return true;
        } catch (Exception $ex) {
            $this->Logger->info("[" . __METHOD__ . "]" . $ex->getTraceAsString());
            $this->Logger->info("[" . __METHOD__ . "]ERROR 3: Failed to upload file.");
            $this->Logger->info("[" . __METHOD__ . "]END");
            return false;
        }
    }

    private function moveFiles($fileName) {

        if (copy($this->rawPath . $fileName, $this->bakPath . $fileName)) {
            if (unlink($this->rawPath . $fileName)) {
                return true;
            }
            return false;
        }
        return false;
    }

    function sendXml2Plex($_msisdn, $_msg, $_billing, $_special_character = FALSE) {

        $this->Logger->info("[" . __METHOD__ . "]START");
        $this->Logger->info("[" . __METHOD__ . "]PARAMS" . print_r(func_get_args(), true));

        $request = "<?xml version=\"1.0\" ?>";
        $request .= "<!DOCTYPE message SYSTEM \"/home/jinny/bin/ems.dtd\">"; // ******* PATH DA CONTROLLARE
        $request .= "<message>";
        $request .= "<submit>";
        $request .= "<application_type>" . substr($this->appCfs, 0, 5) . substr($this->appTfs, 0, 2) . substr($_billing, 0, 1) . "</application_type>";
        $request .= "<da>";
        $request .= "<number>" . $_msisdn . "</number>";
        $request .= "</da>";
        $request .= "<oa>";
        $request .= "<number>" . $this->appSender . "</number>";
        $request .= "</oa>";
        $request .= "<group_name>" . $this->ctxGroup . "</group_name>";
        if ($_special_character) {
            $request .= "<dcs>8</dcs>";
            $request .= "<ud type=\"unicode\">";
            $request .= "<text>" . substr($this->stringToHex($_msg), 0, 280) . "</text>";
        } else {
            $request .= "<ud>";
            $request .= "<text><![CDATA[" . stripslashes($this->clean_text2plex(substr($_msg, 0, 160))) . "]]></text>";
        }

        $request .= "</ud>";
        $request .= "<pid>0</pid>";
        $request .= "</submit>";
        $request .= "</message>";

        $this->Logger->info("[" . __METHOD__ . "]\$request" . print_r($request, true));

        $con = curl_init();
        curl_setopt($con, CURLOPT_URL, $this->plexPostUrl);
        curl_setopt($con, CURLOPT_POST, true);
        curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($con, CURLOPT_POSTFIELDS, $request);
        $plexResult = curl_exec($con);
        curl_close($con);

        $this->Logger->info("[" . __METHOD__ . "]\$plexResult" . print_r($plexResult, true));

        $this->Logger->info("[" . __METHOD__ . "]END");

        return $plexResult;
    }

    public function clean_text2plex($msg) {

        /**
         * Caracteres especiais permitidos
         *
         * .,-?!'@:;/()<>
         */
        return $msg;
        $result = $msg;

        $especiais_bloqueados = '+"#$%¨&*_¹²³£¢¬§{}°ºª´~^[]|\\`“”';

        // Troco caracteres especiais que nao sao permitidos por algum que se aproxime ao seu real significado
        $result = str_replace('"', "'", $result);
        $result = str_replace('“', "'", $result);
        $result = str_replace('”', "'", $result);
        $result = str_replace('#', '', $result);
        $result = str_replace('$', 'S', $result);
        $result = str_replace('%', '', $result);
        $result = str_replace('¨', '', $result);
        $result = str_replace('&', 'E', $result);
        $result = str_replace('*', '-', $result);
        //$result = str_replace('_', '-', $result);
        $result = str_replace('¹', '1', $result);
        $result = str_replace('²', '2', $result);
        $result = str_replace('³', '3', $result);
        $result = str_replace('£', 'L', $result);
        $result = str_replace('¢', 'c', $result);
        $result = str_replace('¬', '', $result);
        $result = str_replace('§', '', $result);
        $result = str_replace('{', '(', $result);
        $result = str_replace('}', ')', $result);
        $result = str_replace('°', '', $result);
        $result = str_replace('º', 'o', $result);
        $result = str_replace('ª', 'a', $result);
        $result = str_replace('´', "'", $result);
        $result = str_replace('~', '', $result);
        $result = str_replace('^', '', $result);
        $result = str_replace('[', '(', $result);
        $result = str_replace(']', ')', $result);
        $result = str_replace('|', '/', $result);
        $result = str_replace('\\', '/', $result);
        $result = str_replace('`', "'", $result);

        // Troco cedilha minusculo/maiusculo por c minusculo/maiusculo
        $result = str_replace('ç', "c", $result);
        $result = str_replace('Ç', "C", $result);

        // Troco a com acento por a sem acento
        $result = str_replace('á', "a", $result);
        $result = str_replace('à', "a", $result);
        $result = str_replace('ã', "a", $result);
        $result = str_replace('â', "a", $result);
        $result = str_replace('ä', "a", $result);

        // Troco A com acento por A sem acento
        $result = str_replace('Á', "A", $result);
        $result = str_replace('À', "A", $result);
        $result = str_replace('Ã', "A", $result);
        $result = str_replace('Â', "A", $result);
        $result = str_replace('Ä', "A", $result);

        // Troco e com acento por e sem acento
        $result = str_replace('é', "e", $result);
        $result = str_replace('è', "e", $result);
        $result = str_replace('ê', "e", $result);
        $result = str_replace('ë', "e", $result);

        // Troco E com acento por E sem acento
        $result = str_replace('É', "E", $result);
        $result = str_replace('È', "E", $result);
        $result = str_replace('Ê', "E", $result);
        $result = str_replace('Ë', "E", $result);

        // Troco i com acento por i sem acento
        $result = str_replace('í', "i", $result);
        $result = str_replace('ì', "i", $result);
        $result = str_replace('î', "i", $result);
        $result = str_replace('ï', "i", $result);

        // Troco I com acento por I sem acento
        $result = str_replace('Í', "I", $result);
        $result = str_replace('Ì', "I", $result);
        $result = str_replace('Î', "I", $result);
        $result = str_replace('Ï', "I", $result);

        // Troco o com acento por o sem acento
        $result = str_replace('ó', "o", $result);
        $result = str_replace('ò', "o", $result);
        $result = str_replace('õ', "o", $result);
        $result = str_replace('ô', "o", $result);
        $result = str_replace('ö', "o", $result);

        // Troco O com acento por O sem acento
        $result = str_replace('Ó', "O", $result);
        $result = str_replace('Ò', "O", $result);
        $result = str_replace('Õ', "O", $result);
        $result = str_replace('Ô', "O", $result);
        $result = str_replace('Ö', "O", $result);

        // Troco u com acento por u sem acento
        $result = str_replace('ú', "u", $result);
        $result = str_replace('ù', "u", $result);
        $result = str_replace('û', "u", $result);
        $result = str_replace('ü', "u", $result);

        // Troco U com acento por U sem acento
        $result = str_replace('Ú', "U", $result);
        $result = str_replace('Ù', "U", $result);
        $result = str_replace('Û', "U", $result);
        $result = str_replace('Ü', "U", $result);

        return $result;
    }

    function stringToHex($text) {

        $this->Logger->info("[" . __METHOD__ . "]START");
        $this->Logger->info("[" . __METHOD__ . "]PARAMS" . print_r(func_get_args(), true));

        $arrText = array_slice(explode("\1", trim(chunk_split($text, 1, "\1"))), 0, strlen($text));

        $this->Logger->info("[" . __METHOD__ . "]\$arrText" . print_r($arrText, true));

        $hexText = "";

        foreach ($arrText as $char) {
            $hexchar = dechex(ord($char));
            $lenChar = strlen($hexchar);
            for ($i = 0; $i < (4 - $lenChar); $i++) {
                $hexchar = "0" . $hexchar;
            }

            $hexText .= $hexchar;
        }

        $hexText = strtoupper($hexText);

        $this->Logger->info("[" . __METHOD__ . "]\$hexText" . print_r($hexText, true));

        $this->Logger->info("[" . __METHOD__ . "]END");

        return "$hexText\n";
    }

    static function send($config, $msisdn, $mt, $pLogger) {
        
        $pLogger->info("[" . __METHOD__ . "] Start");
        
        $newPlex = new NewPlex($config, $pLogger);
        
        $newPlex->setAppCfs($config['cfs']);
        $newPlex->setAppSender($config['sender']);
        $newPlex->setCtxGroup($config['group_name']);
        $newPlex->setPlexPostUrl($config['url']);
        
        $pLogger->info("[" . __METHOD__ . "] Sending via newplex...");
        $rtn = $newPlex->sendXml2Plex($msisdn, $mt, $config['billed']);
        if ($rtn) {
            $pLogger->debug('envianto mt [' . $mt . ']');
            $pLogger->debug('PLEX Response: ' . print_r($rtn, true));
        }
    }

}

?>