<?php
/**
 * This class extends SFTPConnection class and use the SCP to copy files
 * 
 * http://php.net/manual/pt_BR/function.ssh2-scp-send.php
 * 
 * @author Thiago Silveira <thiago.silveira@acotel.com>
 * @since 2014-10-07
 */

class SCPProcess extends SFTPConnection
{
    
    protected $scp;
    //protected $connection;

    public function __construct($host, $port)
    {
        parent::__construct($host, $port);
    }

    public function scp_send($local_file, $remote_file)
    {

        try {

            $this->scp = ssh2_scp_send($this->connection, $local_file, $remote_file, 0777);

        } catch (Exception $exc) {

            echo $exc->getTraceAsString();

        }

        if ( ! $this->scp )
            throw new Exception("Could not initialize SCP command.");

    }

}
?>