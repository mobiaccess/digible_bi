<?php
// http://php.oregonstate.edu/manual/en/function.ssh2-sftp.php

class SFTPConnection
{
        protected $connection;
	private $sftp;

	public function __construct($host, $port=22)
	{
		$this->connection = ssh2_connect($host, $port);
		//echo "->[" .$this->connection . "]\n";
		if (!$this->connection) {
			//echo "erro!\n";
			throw new Exception("Could not connect to $host on port $port.");
		}
	}

	public function login($username, $password)
	{
		if (! @ssh2_auth_password($this->connection, $username, $password))
			throw new Exception("Could not authenticate with username $username " . "and password $password.");
		$this->sftp = @ssh2_sftp($this->connection);
		if (! $this->sftp)
			throw new Exception("Could not initialize SFTP subsystem.");
	}

	public function uploadFile($local_file, $remote_file)
	{
		$sftp = $this->sftp;
                $stream = @fopen("ssh2.sftp://{$this->sftp}/{$remote_file}", 'w');
		if (!$stream)
			throw new Exception("Could not open file: $remote_file");
		$data_to_send = @file_get_contents($local_file);
		if ($data_to_send === false)
			throw new Exception("Could not open local file: $local_file.");
		if (@fwrite($stream, $data_to_send) === false)
			throw new Exception("Could not send data from file: $local_file.");
		@fclose($stream);
	}

	public function uploadFiles($local_file, $remote_file){
		return ssh2_scp_send($this->connection, $local_file, $remote_file);
	}

	public function cleanLocation(){
		if($d = opendir("ssh2.sftp://{$this->sftp}/")){
			while (($file = readdir($d)) != false){
				if ($file != "." && $file != "..") {
					$files[] = $file;
				}
			}
		}
		foreach ($files as $val){
			@unlink("ssh2.sftp://{$this->sftp}/".$val);
		}
	}

	public function renameFile($pPath, $old_name, $new_name){
		return @rename("ssh2.sftp://{$this->sftp}/".$pPath.$old_name, "ssh2.sftp://{$this->sftp}/".$pPath.$new_name);
	}

	function scanFilesystem($remote_file) {
		$sftp = $this->sftp;
		$dir = "ssh2.sftp://$sftp$remote_file";
		$tempArray = array();
		$handle = opendir($dir);
		// List all the files
		while (false !== ($file = readdir($handle))) {
			if (substr("$file", 0, 1) != "."){
				/*
				 $stream = ssh2_exec($this->connection, "file $file");
				stream_set_blocking($stream, true);
				$output = stream_get_contents($stream);
				$output  = trim($output);
				echo "\$output: [$output]\n";
				$exp = explode(":",$output);
				$arrSize = sizeof($exp);
				$ind = $arrSize - 1;
				echo "\$arrSize: [$arrSize]\n";
				echo "\$ind: [$ind]\n";
				print_r($exp);
				if(trim($exp[$ind]) == "directory") {

				//if($this->_is_dir($file)){
				//$tempArray[$file] = $this->scanFilesystem("$dir/$file");
				$tempArray["DIR"][]=$file;
				} else {
				$tempArray["FILE"][]=$file;
				}

				var_dump($this->_is_dir($file));

				$this->_mkdir("teste");
				*/

				$tempArray[]=$file;


			}
		}
		closedir($handle);
		return $tempArray;
	}


	public function listDir($path) {
		$sftp = $this->sftp;
		echo "1->".$sftp."<-\n";
		$dir = "ssh2.sftp://$sftp$path";
		echo "2->".$dir."<-\n";
		$stream = ssh2_exec($this->connection, 'ls -l');
		//stream_set_blocking($stream, true);
		$output = stream_get_contents($stream);
		return $output;
	}


	public function receiveFile($remote_file, $local_file)
	{
		$sftp = $this->sftp;
		$stream = @fopen("ssh2.sftp://$sftp$remote_file", 'r');
		if (! $stream)
			throw new Exception("Could not open file: $remote_file");
		$size = $this->getFileSize($remote_file);
		$contents = '';
		$read = 0;
		$len = $size;
		while ($read < $len && ($buf = fread($stream, $len - $read))) {
			$read += strlen($buf);
			$contents .= $buf;
		}
		file_put_contents ($local_file, $contents);
		@fclose($stream);
	}

	public function deleteFile($remote_file){
		$sftp = $this->sftp;
                unlink("ssh2.sftp://{$sftp}/{$remote_file}");
	}

	public function getFileSize($file){
		$sftp = $this->sftp;
		return filesize("ssh2.sftp://$sftp$file");
	}


	public function _is_dir($path) {
		$path = ltrim($path, '/');
		$callIt = 'ssh2.sftp://' . $this->sftp . '/' . $path;
		echo "\$callIt: [$callIt]\n";
		return is_dir($callIt);
	}

	public function _is_file($file) {
		$file = ltrim($file, '/');
		$callIt = 'ssh2.sftp://' . $this->sftp . '/' . $file;
		//echo "\$callIt: [$callIt]\n";
		return is_file($callIt);
	}

	private function _mkdir($dir) {
		ssh2_sftp_mkdir($this->sftp, $path, "0777", true);
	}
}
?>