<?php

require_once(APPPATH.'libraries/MyException.class.php');

class BillingException extends MyException{
	
	private $account_type;
	
	public function __construct($message, $code = 0, Exception $previous = null,$account_type = null){
		parent::__construct($message, $code, $previous);
		$this->account_type = $account_type;
	}

	public function __toString(){
		return __CLASS__."[{$this->getSpecialCode()}]{$this->getSpecialMessage()}\n";
	}

	public function getSpecialCode(){
		if(preg_match('/\[([a-zA-Z0-9]*)\](.*)/i',$this->message,$matches)){
			return $matches[1];
		}else{
			return $this->code;
		}
	}

	public function getSpecialMessage(){
		if(preg_match('/\[([a-zA-Z0-9]*)\](.*)/i',$this->message,$matches)){
			return $matches[2];
		}else{
			return $this->message;
		}
	}
	
	public function getAccountType(){
		return $this->account_type;
	}
	
	
	

}

?>