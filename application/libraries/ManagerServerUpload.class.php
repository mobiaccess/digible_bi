<?php

/**
 *
 *@author Thiago Silveira <thiago.silveira@acotel.com> and Cesar Ribeiro <cesar.ribeiro@acotel.com>
 *@since 2015-01-30 
 */

require_once(APPPATH . 'libraries/SFTPConnection.class.php');
require_once(APPPATH . 'libraries/FTPConnection.class.php');

class ManagerServerUpload{
    
    private $host;
    private $port;
    private $user;
    private $password;
    private $protocol;
    private $server; 
    
    public function __construct($host, $port, $user, $password, $protocol='ftp') {
        
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        
        $this->protocol = strtolower($protocol);
        
        if ( $this->protocol == 'ftp' ){
            
            $this->server = new FTPConnection();
            $this->server->setPort($this->port);
            $this->server->setConfig($this->host, $this->user, $this->password);
            
        }else{
            
            $this->server = new SFTPConnection($this->host,$this->port);
            $this->server->login($this->user,$this->password);
            
        }
        
        
    }
    
    public function send_to_server($local_file,$remote_file){
        
        $this->server->uploadFile($local_file, $remote_file);
        
    }
    
    public function delete_file($file){
        
        if ( $this->protocol == 'ftp' ){
            
            $this->server->delete($file);
            
        }else{
            
            $this->server->deleteFile($file);
            
        }
        
    }
    
    public function is_file($file){
        
        if ( $this->protocol == 'ftp' ){
            
            $return_is_file = $this->server->isFile($file);
            
        }else{
            
            $return_is_file = $this->server->_is_file($file);
            
        }
        
        return $return_is_file;
        
    }

    
    
    
}
