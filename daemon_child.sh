#!/bin/bash

RELDIR=`dirname $0`;
cd $RELDIR;
. $1;

# PHP="/usr/local/acotel/php-5.5.14/bin/php";
# PHP="/opt/php/bin/php";
PHP="/usr/bin/php";
COMMAND=$PHP" "$RELDIR"/"$DAEMON_CHILD;

GREP="pgrep -fl \"$COMMAND\" | grep -v $0 | grep -v grep | wc -l | cut -f1 -d' '";
X=`pgrep -fl "$COMMAND" | grep -v $0 | grep -v grep | wc -l | cut -f1 -d' '`;
echo $GREP >> $LOGFILE;
echo "Slots usados: "$X >> $LOGFILE;
echo "$X" >> $LOCKFILE$3.$$;
if [ $X -ge 1 ]; then
    # jah estah no maximo, nao vamos fazer nada
    echo $DATE"|estava_rodando|"$COMMAND >> $LOGFILE;
    echo $DATE"|estava_rodando|"$COMMAND;
    #echo "Nao tem slot vago, nao vamos rodar!"
    echo $DATE"|nao_tem_slot_vago:[$X]|"$COMMAND >> $LOGFILE;
else
    # tem slot valido vamos rodar
    #echo "Tem slot vago vamos rodar !"
    #mail -s $DAEMON_CHILD\_vou_rodar $EMAIL < $LOCKFILE.$$
    echo $DATE"|tem_slot_vago:[$X]|"$COMMAND >> $LOGFILE;
    ($COMMAND >> $LOGFILE &);
fi;

mv -f $LOCKFILE$3.$$ $LOCKFILE$3;

exit;

# End.
