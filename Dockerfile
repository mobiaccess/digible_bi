FROM php:7-apache

MAINTAINER Fernando Siggelkow <fernandosiggelkow@gmail.com>

RUN \
    buildDevs="libpq-dev libzip-dev libicu-dev libldap2-dev" && \
    apt-get update -y && \
    apt-get install -y $buildDevs --no-install-recommends && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install \
        ldap \
        pdo \
        pdo_pgsql \
        pgsql \
        intl

RUN \
    mkdir -p /var/www/html/logs/jogos_de_sempre && \
    chmod 777 /var/www/html/logs/jogos_de_sempre && \
    mkdir -p /var/www/html/others/jogos_de_sempre && \
    chmod 777 /var/www/html/others/jogos_de_sempre