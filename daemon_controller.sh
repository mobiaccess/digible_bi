#!/bin/bash

RELDIR=`dirname $0`
cd $RELDIR
. $1

SECOND_INTERVAL=${SECOND_INTERVAL:=60}

echo "SECOND_INTERVAL: "$SECOND_INTERVAL

SECOND_LOOP=`expr 60 / $SECOND_INTERVAL`

echo "SECOND_LOOP: "$SECOND_LOOP

for (( Y=0 ; Y<$SECOND_LOOP ; Y++ ))
do

    for (( I=0; I<$MAX_NUMBER; I++ ))
    do
            ($RELDIR/daemon_child.sh $1 $MAX_NUMBER $I > /dev/null &)
    done

    if [ `expr $Y + 1` -ne $SECOND_LOOP ]; then
        sleep $SECOND_INTERVAL;
    fi;
done

exit;

# End.
