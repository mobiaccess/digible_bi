''' Deploy script

    @author fsiggelkow '''

from time import strftime
from fabric.api import env, local, put, get, run, prompt

env.tag = prompt('What is the new tag name? ')
env.filename = env.tag + '.tar'
env.remote_dir = '/var/www/html/source/jogos_de_sempre'
env.release_dir = env.remote_dir + '/releases/' + env.tag
env.tag_path = env.release_dir + env.filename
env.cache_dir = env.release_dir + '/application/cache'


def qa():
    env.hosts = ['devrio@192.168.222.16', ]
    

def prod():
    env.hosts = ['devrio@10.2.105.57', ]
    

def deploy():
    env.tag_message = prompt('What you have to talk about this tag? ')
    local('git tag -a %(tag)s -m \'%(tag_message)s\'' % env)
    local('git archive %(tag)s > %(filename)s' % env)
    local('git push origin %(tag)s' % env)
    run('rm -rf %(release_dir)s' % env)
    run('mkdir %(release_dir)s' % env)
    put('%(filename)s' % env, '%(release_dir)s' % env)
    run('cd %(release_dir)s && tar -xvf %(filename)s && rm %(filename)s' % env)
    local('rm -rf %(filename)s' % env)
    run('cd %(remote_dir)s && rm -rf current-release && ln -s releases/%(tag)s current-release' % env)
    run('chmod 777 %(cache_dir)s' % env)